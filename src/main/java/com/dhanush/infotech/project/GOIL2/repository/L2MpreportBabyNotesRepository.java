package com.dhanush.infotech.project.GOIL2.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.L2MpreportBabyNotes;


@Repository
public interface L2MpreportBabyNotesRepository extends JpaRepository<L2MpreportBabyNotes, Long>
{

	/// for Asphyxia
	
	@Query(value = "select count(1) as count FROM L2MpreportBabyNotes a where a.state = :state and a.admissiondate like :start% and  a.babyrequireresuscitation in ('true','yes','1')")
	Long findStateAsphyxia(@Param("state") String state,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district=:district and a.admissiondate like :start% and  a.babyrequireresuscitation in ('true','yes','1')")
	Long findDistrictAsphyxia(@Param("state") String state,@Param("district") String district,@Param("start") String start);

	
	@Query(value = "select count(1) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.babyrequireresuscitation in ('true','Yes','1')")
	Long findBlockAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.babyrequireresuscitation in ('true','Yes','1')")
	Long findFacilitytypeAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(1) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.babyrequireresuscitation in ('true','Yes','1')")
	Long findFacilityAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);

	// breast feeding initiation


	@Query(value = "select count(a.breastfeedinginitiated) as count FROM L2MpreportBabyNotes a where a.state = :state and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
	Long findStateBreastFeed(@Param("state")String state, @Param("start") String start);

	@Query(value = "select count(a.breastfeedinginitiated) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
	Long findDistrictBreastFeed(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	@Query(value = "select count(a.breastfeedinginitiated) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
	Long findBlockBreastFeed(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.breastfeedinginitiated) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
	Long findFacilitytypeBreastFeed(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(a.breastfeedinginitiated) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.breastfeedinginitiated in ('Yes','1')")
	Long findFacilityCodeBreastFeed(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
			@Param("start")String start);
// for birth weight




	@Query(value = "select count(a.babyweight) as count FROM L2MpreportBabyNotes a where a.state = :state and a.admissiondate like :start% and a.babyweight >0 ")
	Long findStateBabyWeight(@Param("state")String state, @Param("start") String start);

	@Query(value = "select count(a.babyweight) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.babyweight >0")
	Long findDistrictBabyWeight(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	@Query(value = "select count(a.babyweight) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and  a.babyweight >0 ")
	Long findBlockBabyWeight(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.babyweight) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and  a.babyweight >0")
	Long findFacilitytypeBabyWeight(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(a.babyweight) as count FROM L2MpreportBabyNotes a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.babyweight >0")
	Long findFacilityCodeBabyWeight(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
			@Param("start")String start);
	
	
	
	
	
	
}
