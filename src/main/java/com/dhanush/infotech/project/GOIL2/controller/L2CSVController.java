package com.dhanush.infotech.project.GOIL2.controller;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.dhanush.infotech.project.GOIL2.model.Admission;
import com.dhanush.infotech.project.GOIL2.model.L2CSVModel;
import com.dhanush.infotech.project.GOIL2.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL2.repository.L2CSVRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class L2CSVController 
{
	@Autowired
	L2CSVRepository l2csvRepository;
	
	@Autowired
	AdmissionRepository admissionRepository;
	

	 
	 @CrossOrigin
	 @RequestMapping(value = "l2CSVDownload", method = RequestMethod.GET )
	 private void getCSVDownload(@RequestParam("state_code") String state_code,@RequestParam ("district_code") String district_code
			 ,@RequestParam ("block_code") String block_code,@RequestParam ("facility_code") String facility_code,@RequestParam ("facility_type_id") String facility_type_id,@RequestParam("input_year") String input_year,@RequestParam("input_month") String input_month,HttpServletResponse response) throws Exception
	{
		 
        
			                                                                                                                                 	
			 String state= state_code ;                                                                  	
			String district=district_code.equalsIgnoreCase("ALL") ?"" :district_code;           	
			String block=block_code.equalsIgnoreCase("ALL") ?"" :block_code ;                   	
			String facility=facility_code.equalsIgnoreCase("ALL") ?"" :facility_code ;          	
			String facilityType=facility_type_id.equalsIgnoreCase("ALL") ?"" :facility_type_id ;	 
		    String year=input_year.equalsIgnoreCase("ALL") ?"%" :input_year+"-" ;                          
		    String  month=input_month.equalsIgnoreCase("ALL")?"":input_month.length() == 1 ? "0"+input_month+"-" : input_month+"-";                       
		    String date=year+""+month;                                                                                                       
		    List<L2CSVModel> li=null;
		    
		    
		     if(state.length()>0 && district.length()==0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)             
		     {		 		                                                                                                                  
		      li=l2csvRepository.findStateDetails(state,date);                                                                              
		     }                                                                                                                                 
			if(state.length()>0 && district.length()>0 && block.length()==0 && facility.length()==0 && facilityType.length()==0)                                                                                                                   
		     {		 		                                                                                                                  
	         	 li=l2csvRepository.findDistrictDetails(state,district,date);		                                                      
	         }                                                                                                                                 
	         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()==0 && facility.length()==0)               
	         {		 		                                                                                                                  
            	 li=l2csvRepository.findBlockDetails(state,district,block,date);                                                           
            }                                                                                                                                 
            if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()==0)                
	         {                                                                                                                                 
		     	                                                                                                                              
	         li=l2csvRepository.findFacilityTypeDetails(state,district,block,facilityType,date);                                            
	                                                                                                                                           
	         }                                                                                                                                 
	         if(state.length()>0 && district.length()>0 && block.length()>0 && facilityType.length()>0 && facility.length()>0)                 
	         {                                                                                                                                 
	         	                                                                                                                              
	          li=l2csvRepository.findFacilityDetails(state,district,block,facilityType,facility,date);                                      
		                                                                                                                                       
	         }  
	         
	                                                                                                                            
	            String csvFileName = "csvReport.csv";                                                                           
	                                                                                                                            
	               response.setContentType("text/csv");                                                                         
	                                                                                                                            
	               String headerKey = "Content-Disposition";                                                                    
	               String headerValue = String.format("attachment; filename=\"%s\"",csvFileName);                               
	               response.setHeader(headerKey, headerValue);                                                                  
	                                                                                                                            
	                                                                                                                            
	                ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),CsvPreference.STANDARD_PREFERENCE);    
	                
	                       String[] header = {"statename","districtname","blockname","facilityname","facilitytypename","completeness","aadhar_card_number","abortion_parity","address","admission_category","admission_date","admissionchildid","age","blockname","booked","bpl_jsy_register","contact_number","contraception_history","date_on_set_of_labour",                           
	                      "designation","districtname","edd","family_ho_chronic_illness","final_diagnosis","final_out_come","general_exam_height","general_exam_jaundice","general_exam_pallor","general_exam_pedal_edema",                                   
	                    "general_exam_weight","gestational_ageage_from_usg","gestational_ageantenatal_corticosteroid","gestational_ageedd","gestational_agefinal_estimated_age","gestational_agefunda_mental_heights","gestational_agelmp","gestational_agepre_term",
	                    "gravida","indication_for_assistedlscsothers","ipd_registerd_number","is_active","lab_exam_antidgiven_hb","lab_exam_blood_group_rh","lab_exam_urine_sugar","lab_exam_blood_sugar","lab_examgdm","lab_examhbs_ag","lab_exam_hb","lab_exam_hiv","lab_exam_malaria","lab_exam_others","lab_exam_syphilis","lab_exam_urine_protein",
	                      "lab_exam_vdrlrpr","living_children","lmp","martial_status","mcts_no","medical_surgical_history","name_of_asha","name_of_service_provider",
	                      "others1","others2","paexam_engagement","paexam_lie","paexam_others","parity",
	                      "paexampresentation_cephalic","past_obstetrics_history","patient_contact_number",
	                      "patient_name","pcts_id_no","presenting_complaints","provisional_diagnosis","pv_exam_cervical_dilation","pv_exam_cervical_effacement","pv_exam_colour_of_amniotic_fluid","pv_exam_membranes",
	                      "pv_exam_no_ofpvexaminations","pv_exam_pelvis_adequate","reffered_from","reffered_reason", "status","time","time_of_onset_of_labour",
	                      "unique_no","vitals_bp_diastolic","vitals_bp_systolic","vitals_fhr","vitals_pulse","vitals_respiratory_rate",
	                      "vitals_temparature","wo_do","provider_date", "provider_phone_no","provider_time","condition_of_baby","condition_of_mother",
	                      "counselling_on_danger_signs_done","discharge_details_other_notes","discharge_details_others","family_planning_method_adopted","final_outcome","advice_on_discharge",
	                      "date_of_discharge_referral","other_notes","reason_for_referral","time_of_discharge_referral","treatment_given","treatment_given1",
	                      "augmentation_performed","ifyes_please_specify_the_indi_aug","whether_partograph_was_filled",
	                      "whether_scc1was_filled","whether_ssc2was_filled","assisted","delivery_notes_abortion","delivery_notes_amstl_performed",                        
	                      "delivery_notes_cct","delivery_notes_complications","delivery_notes_date","delivery_notes_episiotomy","delivery_notes_if_maternal_death_cause_of_death","delivery_notes_maternal_death",
	                      "delivery_notes_maternaldeath_time","delivery_notes_others","delivery_notes_ppiucd_inserted","delivery_notes_preterm","delivery_notes_time","delivery_notes_uterine_massage",
	                      "delivery_notes_uterotonic_administered","delivery_outcome1","delivery_outcome2","type_of_delivery","whetherscc4wasfilled","blood_transfusion_or_other_procedure_notes","clinical_diagnosis_if_any_condition_presen",
	                      "condition_at_transfer_to_pnc_ward","date_and_time_of_transfer_topncward","date_and_time_of_transfer_topncward_time","if_referred_reason_for_referral_of_mother_baby",
	                      "notes_for_baby","notes_for_mother","post_delivery_notes","post_delivery_others","whether_scc3was_filled","any_congenital_anomaly",
	                      "any_other_complication","baby_notes_birth_weight","breastfeeding_initiated","breastfeeding_time_of_initiation",
	                      "did_the_baby_cry_immediately_after_birth", "did_the_baby_require_resuscitation","identification_for_baby",
	                      "if_yes_dose","if_yes_was_it_initiated_in_labour_room","sex_of_the_baby","temperature_of_baby","vitaminkadministered",
	                      "unique_id","Baby1","Baby2","Baby3"};
	                   csvWriter.writeHeader(header);                          
	                                                                         
	                 for (L2CSVModel data : li) {                             
	                 	try {                                               
	                 	                                                    
	                 	                                                    
	                     csvWriter.write(data,header);                       
	                 	}catch(Exception e)                                 
	                 	{                                                   
	                 		e.printStackTrace();                            
	                 		System.out.println("exceptionnnnnnnnn----"+e);  
	                 		                                                
	                 	}                                                   
	                 }                                                       
	                                                                         
	                 csvWriter.close();
			
	         
	}
	
	
		
		@CrossOrigin
		@RequestMapping(value = "/getl2beneficiaryreport", method = RequestMethod.POST, produces = { "application/json" })
		public Map<String, Object> getChildIdByUniqueID(@RequestBody String userJsonReq) {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			Map<String, Object> error = new HashMap<>(); 
			Map<String, Object> data = new HashMap<>();
			List<Map<String,Object>> arr = new ArrayList<>();
			try {
				int i = 0;
				i+=  Integer.parseInt(openJson.getString("state_code").length() > 0 ? "1" : "0");
				i+=   Integer.parseInt(!openJson.getString("district_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=  Integer.parseInt(!openJson.getString("block_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=   Integer.parseInt(!openJson.getString("facility_type_id").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				i+=   Integer.parseInt(!openJson.getString("facility_code").toString().equalsIgnoreCase("ALL") ? "1" :"0");
				
		 	String year=openJson.getString("input_year").equalsIgnoreCase("ALL") ?"%" :openJson.getString("input_year")+"-" ;
		 	String month=openJson.getString("input_month").equalsIgnoreCase("ALL")?"%":openJson.getString("input_month").length()==1 ? "0"+openJson.getString("input_month")+"%" :openJson.getString("input_month") +"%";
		 	String date=year+""+month;
			List<Admission> admission = null;
		 	switch(i) {
		 	case 1 :
			 	admission = admissionRepository.findByStateAndAdmissionDateLike(openJson.getString("state_code"),date);
			 	break;
		 	case 2 :
		 		admission=admissionRepository.findByStateAndDistrictAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),date);
		 	break;
		 	case 3 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),date);
		 	break;
		 	case 4 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),date);
		 	break;
		 	case 5 :
		 		admission=admissionRepository.findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndAdmissionDateLike(openJson.getString("state_code"),openJson.getString("district_code"),openJson.getString("block_code"),openJson.getString("facility_type_id"),openJson.getString("facility_code"),date);
		 	break;
		 	default:
		 		break;
		 	}
			for(Admission admission1 : admission)
			{
				Map<String , Object> map  = new HashMap<>();
				map.put("case_no", admission1.getUniqueNo());
				map.put("admDate", admission1.getAdmissionDate());
				map.put("beneficiary_name", admission1.getPatientName());
				map.put("pcts_no", admission1.getPctsIdNo());
				map.put("mobile_no", admission1.getPatientContactNumber());
				map.put("status", admission1.getCompleteness());
				arr.add(map);
			}
			data.put("message", arr.size()+" Records Found");
			data.put("result", arr);
			data.put("total", arr.size());
			data.put("success",true);
			data.put("error", error);
			}
			catch (Exception e) {
				error.put("error", e.getMessage());
				data.put("message", arr.size()+" Records Found");
				data.put("result", arr);
				data.put("total", arr.size());
				data.put("success",true);
				data.put("error", error);
			}
			return data;
		}
	 
	 
	
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
	
	

}