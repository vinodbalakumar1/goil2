package com.dhanush.infotech.project.GOIL2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="l2mpreportbabynotes")
public class L2MpreportBabyNotes implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long id;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="facilitytype")
	public String facilitytype;
	@Column(name="admission_date")
	public String admissiondate;	

	@Column(name="did_the_baby_require_resuscitation")
	public String babyrequireresuscitation ;
	@Column(name="breastfeeding_initiated")
	public String breastfeedinginitiated ;
	@Column(name="baby_notes_birth_weight")
	public String babyweight ;
	
	

	public String getBabyweight() {
		return babyweight;
	}

	public void setBabyweight(String babyweight) {
		this.babyweight = babyweight;
	}

	public String getBreastfeedinginitiated() {
		return breastfeedinginitiated;
	}

	public void setBreastfeedinginitiated(String breastfeedinginitiated) {
		this.breastfeedinginitiated = breastfeedinginitiated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getFacilitytype() {
		return facilitytype;
	}

	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}

	public String getAdmissiondate() {
		return admissiondate;
	}

	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}

	public String getBabyrequireresuscitation() {
		return babyrequireresuscitation;
	}

	public void setBabyrequireresuscitation(String babyrequireresuscitation) {
		this.babyrequireresuscitation = babyrequireresuscitation;
	}
	
	
	
	
}
