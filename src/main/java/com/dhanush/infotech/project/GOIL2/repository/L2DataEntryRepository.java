package com.dhanush.infotech.project.GOIL2.repository;

import java.util.Date;
import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.L2MPRDataEntryModel;


@Repository
public interface L2DataEntryRepository extends JpaRepository<L2MPRDataEntryModel, Long>
{


	@Query(value = "select a FROM L2MPRDataEntryModel a where state = :state and district = :district and block = :block and facilitytype = :facilityType and facility = :facility and year = :year and month = :month")
	List<L2MPRDataEntryModel> findByFacility(@Param("state") int state,@Param("district") int district,@Param("block") int block ,@Param("facilityType") int facilityType,@Param("facility") int facility,@Param("year") int year,@Param("month") int month);

	
	


	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.year = :year and a.month= :month ")
	List<L2MPRDataEntryModel> findByState(@Param("state")int state,@Param("year") int year,@Param("month") int month);
	
	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and year = :year and month = :month")
	List<L2MPRDataEntryModel> findByDistrict(@Param("state")int state,@Param("district")int district,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block  and year = :year and month = :month")
	List<L2MPRDataEntryModel> findByBlock(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and year = :year and month = :month")
	List<L2MPRDataEntryModel> findByFacilitytype(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("facilitytype") int facilitytype,@Param("year") int year,@Param("month") int month);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and year = :year and month = :month")
	List<L2MPRDataEntryModel> findByFacilityCode(@Param("state")int state,@Param("district") int district,@Param("block")  int block,@Param("facilitytype") int facilitytype,@Param("facility") int facility,
			@Param("year") int year,@Param("month") int month);




	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and createdAt=:createdAt")
	List<L2MPRDataEntryModel> findByStateALL(@Param("state")int state,@Param("createdAt") Date createdAt);	
	
	
	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and createdAt=:createdAt")
	List<L2MPRDataEntryModel> findByDistrictALL(@Param("state")int state,@Param("district")int district,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block and createdAt=:createdAt")
	List<L2MPRDataEntryModel> findByBlockALL(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and createdAt=:createdAt")
	List<L2MPRDataEntryModel> findByFacilitytypeALL(@Param("state")int state, @Param("district")int district,@Param("block") int block,@Param("facilitytype") int facilitytype,@Param("createdAt")Date mpReport);

	@Query(value = "select a FROM L2MPRDataEntryModel a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and createdAt=:createdAt")
	List<L2MPRDataEntryModel> findByFacilityCodeALL(@Param("state")int state,@Param("district") int district,@Param("block")  int block,@Param("facilitytype") int facilitytype,@Param("facility") int facility,@Param("createdAt")Date mpReport);


	
	
	//for getting max timestamp records month wise

			@Query(value = "select max(createdAt) FROM L2MPRDataEntryModel group by month")
			List<L2MPRDataEntryModel> findMaxTimerecords();
			
			
			@Query(value = "select max(createdAt) FROM L2MPRDataEntryModel where year=:year group by month")
			List<L2MPRDataEntryModel> findMaxTimerecordsByYear(@Param("year")int year);
			
	

}
