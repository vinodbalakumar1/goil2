package com.dhanush.infotech.project.GOIL2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="l2mpreportdelivery")
public class L2MprView
{
	@Id
	private Long id;
	@Column(name="state")
	public String state;
	@Column(name="district")
	public String district;
	@Column(name="block")
	public String block;
	@Column(name="facility")
	public String facility;
	@Column(name="facilitytype")
	public String facilitytype;
	@Column(name="type_of_delivery")
	public String typeofdelivery;
	@Column(name="admission_date")
	public String admissiondate;	
	@Column(name="delivery_outcome1")
	public String deliveryoutcome1;
	@Column(name="delivery_notes_maternal_death")
	public String maternaldeath;
	@Column(name="delivery_notes_complications")
	public String deliverycomplications;
	@Column(name="delivery_notes_preterm")
	public String preterm;
	@Column(name="whether_partograph_was_filled")
	public String partograph;
	@Column(name="delivery_notes_uterotonic_administered")
	public String uterotonic;
	@Column(name="delivery_notes_ppiucd_inserted")
	public String ppiucd;
	@Column(name="whether_scc1was_filled")
	public String whetherscc1wasfilled;
	@Column(name="whether_ssc2was_filled")
	public String whetherscc2wasfilled;
	@Column(name="whether_scc3was_filled")
	public String whetherscc3wasfilled;
	@Column(name="whetherscc4wasfilled")
	public String whetherscc4wasfilled;
	
	
	
	public String getWhetherscc1wasfilled() {
		return whetherscc1wasfilled;
	}
	public void setWhetherscc1wasfilled(String whetherscc1wasfilled) {
		this.whetherscc1wasfilled = whetherscc1wasfilled;
	}
	public String getWhetherscc2wasfilled() {
		return whetherscc2wasfilled;
	}
	public void setWhetherscc2wasfilled(String whetherscc2wasfilled) {
		this.whetherscc2wasfilled = whetherscc2wasfilled;
	}
	public String getWhetherscc3wasfilled() {
		return whetherscc3wasfilled;
	}
	public void setWhetherscc3wasfilled(String whetherscc3wasfilled) {
		this.whetherscc3wasfilled = whetherscc3wasfilled;
	}
	public String getWhetherscc4wasfilled() {
		return whetherscc4wasfilled;
	}
	public void setWhetherscc4wasfilled(String whetherscc4wasfilled) {
		this.whetherscc4wasfilled = whetherscc4wasfilled;
	}
	public String getPpiucd() {
		return ppiucd;
	}
	public void setPpiucd(String ppiucd) {
		this.ppiucd = ppiucd;
	}
	public String getUterotonic() {
		return uterotonic;
	}
	public void setUterotonic(String uterotonic) {
		this.uterotonic = uterotonic;
	}
	public String getPartograph() {
		return partograph;
	}
	public void setPartograph(String partograph) {
		this.partograph = partograph;
	}
	public String getPreterm() {
		return preterm;
	}
	public void setPreterm(String preterm) {
		this.preterm = preterm;
	}
	public String getDeliverycomplications() {
		return deliverycomplications;
	}
	public void setDeliverycomplications(String deliverycomplications) {
		this.deliverycomplications = deliverycomplications;
	}
	public String getMaternaldeath() {
		return maternaldeath;
	}
	public void setMaternaldeath(String maternaldeath) {
		this.maternaldeath = maternaldeath;
	}
	public String getDeliveryoutcome1() {
		return deliveryoutcome1;
	}
	public void setDeliveryoutcome1(String deliveryoutcome1) {
		this.deliveryoutcome1 = deliveryoutcome1;
	}
	public String getAdmissiondate() {
		return admissiondate;
	}
	public void setAdmissiondate(String admissiondate) {
		this.admissiondate = admissiondate;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getFacilitytype() {
		return facilitytype;
	}
	public void setFacilitytype(String facilitytype) {
		this.facilitytype = facilitytype;
	}
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	
	
	
}
