package com.dhanush.infotech.project.GOIL2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.L2CSVModel;


@Repository
public interface L2CSVRepository extends JpaRepository<L2CSVModel, Long>
{

	@Query(value=" select a from L2CSVModel a where state= :state and admission_date like :date% ")
	List<L2CSVModel> findStateDetails(@Param("state")String state,@Param("date") String date);
	
	
	@Query(value=" select a from L2CSVModel a where state= :state and district = :district and admission_date like :date% ")
	List<L2CSVModel> findDistrictDetails(@Param("state")String state,@Param("district")String district,@Param("date") String date);
	
	@Query(value=" select a from L2CSVModel a where state= :state and district = :district and block= :block and admission_date like :date% ")
	List<L2CSVModel> findBlockDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("date") String date);


	@Query(value=" select a from L2CSVModel a where state= :state and district = :district and block= :block and facility_type= :facilityType and admission_date like :date% ")
	List<L2CSVModel> findFacilityTypeDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("date") String date);


	
	@Query(value=" select a from L2CSVModel a where state= :state and district = :district and block= :block and facility_type= :facilityType and facility= :facility and admission_date like :date% ")
	List<L2CSVModel> findFacilityDetails(@Param("state")String state,@Param("district")String district,@Param("block")String block,@Param("facilityType")String facilityType,@Param("facility")String facility,@Param("date") String date);



	
	
	

}
