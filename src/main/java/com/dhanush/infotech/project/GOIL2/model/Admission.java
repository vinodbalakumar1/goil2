package com.dhanush.infotech.project.GOIL2.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l2_admission")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Admission {

	public AssessmentOfPostPartumCondition getAssessmentOfPostPartumCondition() {
		return assessmentOfPostPartumCondition;
	}

	public void setAssessmentOfPostPartumCondition(AssessmentOfPostPartumCondition assessmentOfPostPartumCondition) {
		this.assessmentOfPostPartumCondition = assessmentOfPostPartumCondition;
	}

	/**
	 * Created by by vinod on 25/07/18.
	 */

	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt = new Date();

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt = new Date();

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private Delivery delivery;
	
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private DischargeDetails dischargeDetails;
	
	public DischargeDetails getDischargeDetails() {
		return dischargeDetails;
	}

	public void setDischargeDetails(DischargeDetails dischargeDetails) {
		this.dischargeDetails = dischargeDetails;
	}

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "admission")
	private AssessmentOfPostPartumCondition assessmentOfPostPartumCondition;
	
	
	@OneToMany(cascade=CascadeType.ALL,fetch =FetchType.LAZY)
    @JoinColumn(name="admissionid")
    private List<ListChild> listChild;
	
	
	public List<BabyNotes> getBabynotes() {
		return babynotes;
	}

	public void setBabynotes(List<BabyNotes> babynotes) {
		this.babynotes = babynotes;
	}

	@OneToMany(cascade=CascadeType.ALL,fetch =FetchType.LAZY)
    @JoinColumn(name="babynotesid")
    private List<BabyNotes> babynotes;
	
	

	public List<ListChild> getListChild() {
		return listChild;
	}

	public void setListChild(List<ListChild> listChild) {
		this.listChild = listChild;
	}

	public Delivery getDelivery() {
		return delivery;
	}

	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Size(max = 60)
	public String uniqueNo;

	public String pctsIdNo;

	public String facilityType;

	private String mctsNo;

	private String booked;

	private String ipdRegisterdNumber;

	private String bplJsyRegister;

	private String admissionchildid;

	private String aadharCardNumber;

	private String refferedFrom;

	private String refferedReason;

	private String facility;

	private String state;

	private String blockname;

	private String statename;

	private String facilitytypename;

	private String districtname;

	private String facilityname;

	private String block;

	private String district;

	private String contactNumber;

	private String nameOfAsha;

	private String isActive;

	private String patientName;

	private String age;

	private String woDo;

	private String address;

	private String patientContactNumber;

	private String martialStatus;

	private String admissionDate;

	private String time;

	private String admissionCategory;

	private String lmp;

	private String edd;

	private String provisionalDiagnosis;

	private String finalDiagnosis;

	private String contraceptionHistory;

	private String others1;

	private String indicationForAssistedLSCSOthers;

	private String finalOutCome;

	private String nameOfServiceProvider;

	private String designation;

	private String presentingComplaints;

	private String pastObstetricsHistory;

	private String others2;

	private String medicalSurgicalHistory;

	private String familyHoChronicIllness;

	private String dateOnSetOfLabour;

	private String timeOfOnsetOfLabour;

	private String gravida;

	private String parity;

	private int status;

	private String abortionParity;

	private String livingChildren;

	public String generalExamHeight;

	public String generalExamWeight;

	public Boolean generalExamPallor;

	public Boolean generalExamJaundice;

	public Boolean generalExamPedalEdema;

	public String vitalsTemparature;


	public String vitalsPulse;

	public String vitalsRespiratoryRate;

	public String vitalsFhr;

	public String vitalsBpSystolic;

	public String vitalsBpDiastolic;

	public Boolean pAExampresentationCephalic;

	public String pAExamOthers;

	public String pAExamEngagement;

	public String pAExamLie;

	public Boolean gestationalAgepreTerm;

	public Boolean gestationalAgeantenatalCorticosteroid;

	public String gestationalAgelmp;

	public String gestationalAgeedd;

	public String gestationalAgefundaMentalHeights;

	public String gestationalAgefinalEstimatedAge;

	public String gestationalAgeageFromUsg;

	public String pvExamNoOfPVExaminations;

	public String pvExamCervicalDilation;

	public String pvExamCervicalEffacement;

	public String pvExamMembranes;

	public String pvExamColourOfAmnioticFluid;

	public Boolean pvExamPelvisAdequate;
	
	public String labExamBloodGroupRh;
	
	public String labExamHb;
	
	public Boolean labExamUrineProtein;
	
	public Boolean labExamHiv;
	
	public Boolean labExamVdrlrpr;
	
	public Boolean labExamMalaria;
	
	public Boolean labExamAntiDGivenHb;
	
	public Boolean labExamBloodSugar;
	
	public Boolean labExamUrineSugar;
	
	public Boolean labExamHBsAg;
	
	public Boolean labExamSyphilis;
	
	public Boolean labExamGDM;
	

	public String labExamOthers;
	
	public String Completeness;
	
	  public String providerPhoneNo;
	  public String providerDate ;
	  public String providerTime ;
	
		private String field1;
		private String field2;
		private String field3;
		private String field4;
		private String field5;
		private String field6;
		private String field7;
		private String field8;
		private String field9;
		private String field10;
	
	public String getField1() {
			return field1;
		}

		public void setField1(String field1) {
			this.field1 = field1;
		}

		public String getField2() {
			return field2;
		}

		public void setField2(String field2) {
			this.field2 = field2;
		}

		public String getField3() {
			return field3;
		}

		public void setField3(String field3) {
			this.field3 = field3;
		}

		public String getField4() {
			return field4;
		}

		public void setField4(String field4) {
			this.field4 = field4;
		}

		public String getField5() {
			return field5;
		}

		public void setField5(String field5) {
			this.field5 = field5;
		}

		public String getField6() {
			return field6;
		}

		public void setField6(String field6) {
			this.field6 = field6;
		}

		public String getField7() {
			return field7;
		}

		public void setField7(String field7) {
			this.field7 = field7;
		}

		public String getField8() {
			return field8;
		}

		public void setField8(String field8) {
			this.field8 = field8;
		}

		public String getField9() {
			return field9;
		}

		public void setField9(String field9) {
			this.field9 = field9;
		}

		public String getField10() {
			return field10;
		}

		public void setField10(String field10) {
			this.field10 = field10;
		}

	public String getProviderPhoneNo() {
		return providerPhoneNo;
	}

	public void setProviderPhoneNo(String providerPhoneNo) {
		this.providerPhoneNo = providerPhoneNo;
	}

	public String getProviderDate() {
		return providerDate;
	}

	public void setProviderDate(String providerDate) {
		this.providerDate = providerDate;
	}

	public String getProviderTime() {
		return providerTime;
	}

	public void setProviderTime(String providerTime) {
		this.providerTime = providerTime;
	}


	public String getCompleteness() {
		return Completeness;
	}

	public void setCompleteness(String completeness) {
		Completeness = completeness;
	}
	

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUniqueNo() {
		return uniqueNo;
	}

	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}

	public String getPctsIdNo() {
		return pctsIdNo;
	}

	public void setPctsIdNo(String pctsIdNo) {
		this.pctsIdNo = pctsIdNo;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public String getMctsNo() {
		return mctsNo;
	}

	public void setMctsNo(String mctsNo) {
		this.mctsNo = mctsNo;
	}

	public String getBooked() {
		return booked;
	}

	public void setBooked(String booked) {
		this.booked = booked;
	}

	public String getIpdRegisterdNumber() {
		return ipdRegisterdNumber;
	}

	public void setIpdRegisterdNumber(String ipdRegisterdNumber) {
		this.ipdRegisterdNumber = ipdRegisterdNumber;
	}

	public String getBplJsyRegister() {
		return bplJsyRegister;
	}

	public void setBplJsyRegister(String bplJsyRegister) {
		this.bplJsyRegister = bplJsyRegister;
	}

	public String getAdmissionchildid() {
		return admissionchildid;
	}

	public void setAdmissionchildid(String admissionchildid) {
		this.admissionchildid = admissionchildid;
	}

	public String getAadharCardNumber() {
		return aadharCardNumber;
	}

	public void setAadharCardNumber(String aadharCardNumber) {
		this.aadharCardNumber = aadharCardNumber;
	}

	public String getRefferedFrom() {
		return refferedFrom;
	}

	public void setRefferedFrom(String refferedFrom) {
		this.refferedFrom = refferedFrom;
	}

	public String getRefferedReason() {
		return refferedReason;
	}

	public void setRefferedReason(String refferedReason) {
		this.refferedReason = refferedReason;
	}

	public String getFacility() {
		return facility;
	}

	public void setFacility(String facility) {
		this.facility = facility;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBlockname() {
		return blockname;
	}

	public void setBlockname(String blockname) {
		this.blockname = blockname;
	}

	public String getStatename() {
		return statename;
	}

	public void setStatename(String statename) {
		this.statename = statename;
	}

	public String getFacilitytypename() {
		return facilitytypename;
	}

	public void setFacilitytypename(String facilitytypename) {
		this.facilitytypename = facilitytypename;
	}

	public String getDistrictname() {
		return districtname;
	}

	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}

	public String getFacilityname() {
		return facilityname;
	}

	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}

	public String getBlock() {
		return block;
	}

	public void setBlock(String block) {
		this.block = block;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getNameOfAsha() {
		return nameOfAsha;
	}

	public void setNameOfAsha(String nameOfAsha) {
		this.nameOfAsha = nameOfAsha;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getWoDo() {
		return woDo;
	}

	public void setWoDo(String woDo) {
		this.woDo = woDo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPatientContactNumber() {
		return patientContactNumber;
	}

	public void setPatientContactNumber(String patientContactNumber) {
		this.patientContactNumber = patientContactNumber;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	public String getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(String admissionDate) {
		this.admissionDate = admissionDate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getAdmissionCategory() {
		return admissionCategory;
	}

	public void setAdmissionCategory(String admissionCategory) {
		this.admissionCategory = admissionCategory;
	}

	public String getLmp() {
		return lmp;
	}

	public void setLmp(String lmp) {
		this.lmp = lmp;
	}

	public String getEdd() {
		return edd;
	}

	public void setEdd(String edd) {
		this.edd = edd;
	}

	public String getProvisionalDiagnosis() {
		return provisionalDiagnosis;
	}

	public void setProvisionalDiagnosis(String provisionalDiagnosis) {
		this.provisionalDiagnosis = provisionalDiagnosis;
	}

	public String getFinalDiagnosis() {
		return finalDiagnosis;
	}

	public void setFinalDiagnosis(String finalDiagnosis) {
		this.finalDiagnosis = finalDiagnosis;
	}

	public String getContraceptionHistory() {
		return contraceptionHistory;
	}

	public void setContraceptionHistory(String contraceptionHistory) {
		this.contraceptionHistory = contraceptionHistory;
	}

	public String getOthers1() {
		return others1;
	}

	public void setOthers1(String others1) {
		this.others1 = others1;
	}

	public String getIndicationForAssistedLSCSOthers() {
		return indicationForAssistedLSCSOthers;
	}

	public void setIndicationForAssistedLSCSOthers(String indicationForAssistedLSCSOthers) {
		this.indicationForAssistedLSCSOthers = indicationForAssistedLSCSOthers;
	}

	public String getFinalOutCome() {
		return finalOutCome;
	}

	public void setFinalOutCome(String finalOutCome) {
		this.finalOutCome = finalOutCome;
	}

	public String getNameOfServiceProvider() {
		return nameOfServiceProvider;
	}

	public void setNameOfServiceProvider(String nameOfServiceProvider) {
		this.nameOfServiceProvider = nameOfServiceProvider;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getPresentingComplaints() {
		return presentingComplaints;
	}

	public void setPresentingComplaints(String presentingComplaints) {
		this.presentingComplaints = presentingComplaints;
	}

	public String getPastObstetricsHistory() {
		return pastObstetricsHistory;
	}

	public void setPastObstetricsHistory(String pastObstetricsHistory) {
		this.pastObstetricsHistory = pastObstetricsHistory;
	}

	public String getOthers2() {
		return others2;
	}

	public void setOthers2(String others2) {
		this.others2 = others2;
	}

	public String getMedicalSurgicalHistory() {
		return medicalSurgicalHistory;
	}

	public void setMedicalSurgicalHistory(String medicalSurgicalHistory) {
		this.medicalSurgicalHistory = medicalSurgicalHistory;
	}

	public String getFamilyHoChronicIllness() {
		return familyHoChronicIllness;
	}

	public void setFamilyHoChronicIllness(String familyHoChronicIllness) {
		this.familyHoChronicIllness = familyHoChronicIllness;
	}

	public String getDateOnSetOfLabour() {
		return dateOnSetOfLabour;
	}

	public void setDateOnSetOfLabour(String dateOnSetOfLabour) {
		this.dateOnSetOfLabour = dateOnSetOfLabour;
	}

	public String getTimeOfOnsetOfLabour() {
		return timeOfOnsetOfLabour;
	}

	public void setTimeOfOnsetOfLabour(String timeOfOnsetOfLabour) {
		this.timeOfOnsetOfLabour = timeOfOnsetOfLabour;
	}

	public String getGravida() {
		return gravida;
	}

	public void setGravida(String gravida) {
		this.gravida = gravida;
	}

	public String getParity() {
		return parity;
	}

	public void setParity(String parity) {
		this.parity = parity;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAbortionParity() {
		return abortionParity;
	}

	public void setAbortionParity(String abortionParity) {
		this.abortionParity = abortionParity;
	}

	public String getLivingChildren() {
		return livingChildren;
	}

	public void setLivingChildren(String livingChildren) {
		this.livingChildren = livingChildren;
	}

	public String getGeneralExamHeight() {
		return generalExamHeight;
	}

	public void setGeneralExamHeight(String generalExamHeight) {
		this.generalExamHeight = generalExamHeight;
	}

	public String getGeneralExamWeight() {
		return generalExamWeight;
	}

	public void setGeneralExamWeight(String generalExamWeight) {
		this.generalExamWeight = generalExamWeight;
	}

	public Boolean getGeneralExamPallor() {
		return generalExamPallor;
	}

	public void setGeneralExamPallor(Boolean generalExamPallor) {
		this.generalExamPallor = generalExamPallor;
	}

	public Boolean getGeneralExamJaundice() {
		return generalExamJaundice;
	}

	public void setGeneralExamJaundice(Boolean generalExamJaundice) {
		this.generalExamJaundice = generalExamJaundice;
	}

	public Boolean getGeneralExamPedalEdema() {
		return generalExamPedalEdema;
	}

	public void setGeneralExamPedalEdema(Boolean generalExamPedalEdema) {
		this.generalExamPedalEdema = generalExamPedalEdema;
	}

	public String getVitalsTemparature() {
		return vitalsTemparature;
	}

	public void setVitalsTemparature(String vitalsTemparature) {
		this.vitalsTemparature = vitalsTemparature;
	}


	public String getVitalsPulse() {
		return vitalsPulse;
	}

	public void setVitalsPulse(String vitalsPulse) {
		this.vitalsPulse = vitalsPulse;
	}

	public String getVitalsRespiratoryRate() {
		return vitalsRespiratoryRate;
	}

	public void setVitalsRespiratoryRate(String vitalsRespiratoryRate) {
		this.vitalsRespiratoryRate = vitalsRespiratoryRate;
	}

	public String getVitalsFhr() {
		return vitalsFhr;
	}

	public void setVitalsFhr(String vitalsFhr) {
		this.vitalsFhr = vitalsFhr;
	}

	public String getVitalsBpSystolic() {
		return vitalsBpSystolic;
	}

	public void setVitalsBpSystolic(String vitalsBpSystolic) {
		this.vitalsBpSystolic = vitalsBpSystolic;
	}

	public String getVitalsBpDiastolic() {
		return vitalsBpDiastolic;
	}

	public void setVitalsBpDiastolic(String vitalsBpDiastolic) {
		this.vitalsBpDiastolic = vitalsBpDiastolic;
	}

	public Boolean getpAExampresentationCephalic() {
		return pAExampresentationCephalic;
	}

	public void setpAExampresentationCephalic(Boolean pAExampresentationCephalic) {
		this.pAExampresentationCephalic = pAExampresentationCephalic;
	}

	public String getpAExamOthers() {
		return pAExamOthers;
	}

	public void setpAExamOthers(String pAExamOthers) {
		this.pAExamOthers = pAExamOthers;
	}

	public String getpAExamEngagement() {
		return pAExamEngagement;
	}

	public void setpAExamEngagement(String pAExamEngagement) {
		this.pAExamEngagement = pAExamEngagement;
	}

	public String getpAExamLie() {
		return pAExamLie;
	}

	public void setpAExamLie(String pAExamLie) {
		this.pAExamLie = pAExamLie;
	}

	public Boolean getGestationalAgepreTerm() {
		return gestationalAgepreTerm;
	}

	public void setGestationalAgepreTerm(Boolean gestationalAgepreTerm) {
		this.gestationalAgepreTerm = gestationalAgepreTerm;
	}

	public Boolean getGestationalAgeantenatalCorticosteroid() {
		return gestationalAgeantenatalCorticosteroid;
	}

	public void setGestationalAgeantenatalCorticosteroid(Boolean gestationalAgeantenatalCorticosteroid) {
		this.gestationalAgeantenatalCorticosteroid = gestationalAgeantenatalCorticosteroid;
	}

	public String getGestationalAgelmp() {
		return gestationalAgelmp;
	}

	public void setGestationalAgelmp(String gestationalAgelmp) {
		this.gestationalAgelmp = gestationalAgelmp;
	}

	public String getGestationalAgeedd() {
		return gestationalAgeedd;
	}

	public void setGestationalAgeedd(String gestationalAgeedd) {
		this.gestationalAgeedd = gestationalAgeedd;
	}

	public String getGestationalAgefundaMentalHeights() {
		return gestationalAgefundaMentalHeights;
	}

	public void setGestationalAgefundaMentalHeights(String gestationalAgefundaMentalHeights) {
		this.gestationalAgefundaMentalHeights = gestationalAgefundaMentalHeights;
	}

	public String getGestationalAgefinalEstimatedAge() {
		return gestationalAgefinalEstimatedAge;
	}

	public void setGestationalAgefinalEstimatedAge(String gestationalAgefinalEstimatedAge) {
		this.gestationalAgefinalEstimatedAge = gestationalAgefinalEstimatedAge;
	}

	public String getGestationalAgeageFromUsg() {
		return gestationalAgeageFromUsg;
	}

	public void setGestationalAgeageFromUsg(String gestationalAgeageFromUsg) {
		this.gestationalAgeageFromUsg = gestationalAgeageFromUsg;
	}

	public String getPvExamNoOfPVExaminations() {
		return pvExamNoOfPVExaminations;
	}

	public void setPvExamNoOfPVExaminations(String pvExamNoOfPVExaminations) {
		this.pvExamNoOfPVExaminations = pvExamNoOfPVExaminations;
	}

	public String getPvExamCervicalDilation() {
		return pvExamCervicalDilation;
	}

	public void setPvExamCervicalDilation(String pvExamCervicalDilation) {
		this.pvExamCervicalDilation = pvExamCervicalDilation;
	}

	public String getPvExamCervicalEffacement() {
		return pvExamCervicalEffacement;
	}

	public void setPvExamCervicalEffacement(String pvExamCervicalEffacement) {
		this.pvExamCervicalEffacement = pvExamCervicalEffacement;
	}

	public String getPvExamMembranes() {
		return pvExamMembranes;
	}

	public void setPvExamMembranes(String pvExamMembranes) {
		this.pvExamMembranes = pvExamMembranes;
	}

	public String getPvExamColourOfAmnioticFluid() {
		return pvExamColourOfAmnioticFluid;
	}

	public void setPvExamColourOfAmnioticFluid(String pvExamColourOfAmnioticFluid) {
		this.pvExamColourOfAmnioticFluid = pvExamColourOfAmnioticFluid;
	}

	public Boolean getPvExamPelvisAdequate() {
		return pvExamPelvisAdequate;
	}

	public void setPvExamPelvisAdequate(Boolean pvExamPelvisAdequate) {
		this.pvExamPelvisAdequate = pvExamPelvisAdequate;
	}

	public String getLabExamBloodGroupRh() {
		return labExamBloodGroupRh;
	}

	public void setLabExamBloodGroupRh(String labExamBloodGroupRh) {
		this.labExamBloodGroupRh = labExamBloodGroupRh;
	}

	public String getLabExamHb() {
		return labExamHb;
	}

	public void setLabExamHb(String labExamHb) {
		this.labExamHb = labExamHb;
	}

	public Boolean getLabExamUrineProtein() {
		return labExamUrineProtein;
	}

	public void setLabExamUrineProtein(Boolean labExamUrineProtein) {
		this.labExamUrineProtein = labExamUrineProtein;
	}

	public Boolean getLabExamHiv() {
		return labExamHiv;
	}

	public void setLabExamHiv(Boolean labExamHiv) {
		this.labExamHiv = labExamHiv;
	}

	public Boolean getLabExamVdrlrpr() {
		return labExamVdrlrpr;
	}

	public void setLabExamVdrlrpr(Boolean labExamVdrlrpr) {
		this.labExamVdrlrpr = labExamVdrlrpr;
	}

	public Boolean getLabExamMalaria() {
		return labExamMalaria;
	}

	public void setLabExamMalaria(Boolean labExamMalaria) {
		this.labExamMalaria = labExamMalaria;
	}

	public Boolean getLabExamAntiDGivenHb() {
		return labExamAntiDGivenHb;
	}

	public void setLabExamAntiDGivenHb(Boolean labExamAntiDGivenHb) {
		this.labExamAntiDGivenHb = labExamAntiDGivenHb;
	}

	public Boolean getLabExamBloodSugar() {
		return labExamBloodSugar;
	}

	public void setLabExamBloodSugar(Boolean labExamBloodSugar) {
		this.labExamBloodSugar = labExamBloodSugar;
	}

	public Boolean getLabExamUrineSugar() {
		return labExamUrineSugar;
	}

	public void setLabExamUrineSugar(Boolean labExamUrineSugar) {
		this.labExamUrineSugar = labExamUrineSugar;
	}

	public Boolean getLabExamHBsAg() {
		return labExamHBsAg;
	}

	public void setLabExamHBsAg(Boolean labExamHBsAg) {
		this.labExamHBsAg = labExamHBsAg;
	}

	public Boolean getLabExamSyphilis() {
		return labExamSyphilis;
	}

	public void setLabExamSyphilis(Boolean labExamSyphilis) {
		this.labExamSyphilis = labExamSyphilis;
	}

	public Boolean getLabExamGDM() {
		return labExamGDM;
	}

	public void setLabExamGDM(Boolean labExamGDM) {
		this.labExamGDM = labExamGDM;
	}

	public String getLabExamOthers() {
		return labExamOthers;
	}

	public void setLabExamOthers(String labExamOthers) {
		this.labExamOthers = labExamOthers;
	}
	
	
	
	
}
