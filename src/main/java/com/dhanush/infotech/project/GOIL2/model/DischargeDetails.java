package com.dhanush.infotech.project.GOIL2.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l2_dischargedetails")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)



public class DischargeDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt = new Date();

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt = new Date();

    @OneToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "admissionId", nullable = false)
    private Admission admission;
	
    
	
	public Admission getAdmission() {
		return admission;
	}
	public void setAdmission(Admission admission) {
		this.admission = admission;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	 private String finalOutcome;
	 private String conditionOfMother;
	 private String conditionOfBaby;
	 private String counsellingOnDangerSignsDone;
	 private String familyPlanningMethodAdopted;
	 private String dischargeDetailsOthers;
	 private String bPOfMother;
	 private String bPOfMother1;
	
	private String dischargeDetailsOtherNotes;
	 private String UniqueId;
	 
	private String date_of_Discharge_Referral;
	private String time_of_Discharge_Referral;
	private String advice_on_discharge       ;
	private String  facility_name            ;
	private String  other_notes              ;
	private String  treatment_given          ;
	private String treatmentGiven1;
	private String reason_for_referral;
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String whetherSCC4wasfilled;
	 
	public String getbPOfMother() {
		return bPOfMother;
	}
	public void setbPOfMother(String bPOfMother) {
		this.bPOfMother = bPOfMother;
	}
	public String getbPOfMother1() {
		return bPOfMother1;
	}
	public void setbPOfMother1(String bPOfMother1) {
		this.bPOfMother1 = bPOfMother1;
	}
	public String getWhetherSCC4wasfilled() {
		return whetherSCC4wasfilled;
	}
	public void setWhetherSCC4wasfilled(String whetherSCC4wasfilled) {
		this.whetherSCC4wasfilled = whetherSCC4wasfilled;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getTreatmentGiven1() {
		return treatmentGiven1;
	}
	public void setTreatmentGiven1(String treatmentGiven1) {
		this.treatmentGiven1 = treatmentGiven1;
	}
	public String getReason_for_referral() {
		return reason_for_referral;
	}
	public void setReason_for_referral(String reason_for_referral) {
		this.reason_for_referral = reason_for_referral;
	}
	public String getUniqueId() {
		return UniqueId;
	}
	public void setUniqueId(String uniqueId) {
		UniqueId = uniqueId;
	}
	public String getFinalOutcome() {
		return finalOutcome;
	}
	public void setFinalOutcome(String finalOutcome) {
		this.finalOutcome = finalOutcome;
	}
	public String getConditionOfMother() {
		return conditionOfMother;
	}
	public void setConditionOfMother(String conditionOfMother) {
		this.conditionOfMother = conditionOfMother;
	}

	public String getConditionOfBaby() {
		return conditionOfBaby;
	}
	public void setConditionOfBaby(String conditionOfBaby) {
		this.conditionOfBaby = conditionOfBaby;
	}
	public String getCounsellingOnDangerSignsDone() {
		return counsellingOnDangerSignsDone;
	}
	public void setCounsellingOnDangerSignsDone(String counsellingOnDangerSignsDone) {
		this.counsellingOnDangerSignsDone = counsellingOnDangerSignsDone;
	}
	public String getFamilyPlanningMethodAdopted() {
		return familyPlanningMethodAdopted;
	}
	public void setFamilyPlanningMethodAdopted(String familyPlanningMethodAdopted) {
		this.familyPlanningMethodAdopted = familyPlanningMethodAdopted;
	}
	public String getDischargeDetailsOthers() {
		return dischargeDetailsOthers;
	}
	public void setDischargeDetailsOthers(String dischargeDetailsOthers) {
		this.dischargeDetailsOthers = dischargeDetailsOthers;
	}
	public String getDischargeDetailsOtherNotes() {
		return dischargeDetailsOtherNotes;
	}
	public void setDischargeDetailsOtherNotes(String dischargeDetailsOtherNotes) {
		this.dischargeDetailsOtherNotes = dischargeDetailsOtherNotes;
	}

	 public String getDate_of_Discharge_Referral() {
			return date_of_Discharge_Referral;
		}
		public void setDate_of_Discharge_Referral(String date_of_Discharge_Referral) {
			this.date_of_Discharge_Referral = date_of_Discharge_Referral;
		}
		public String getTime_of_Discharge_Referral() {
			return time_of_Discharge_Referral;
		}
		public void setTime_of_Discharge_Referral(String time_of_Discharge_Referral) {
			this.time_of_Discharge_Referral = time_of_Discharge_Referral;
		}
		public String getAdvice_on_discharge() {
			return advice_on_discharge;
		}
		public void setAdvice_on_discharge(String advice_on_discharge) {
			this.advice_on_discharge = advice_on_discharge;
		}
		public String getFacility_name() {
			return facility_name;
		}
		public void setFacility_name(String facility_name) {
			this.facility_name = facility_name;
		}
		public String getOther_notes() {
			return other_notes;
		}
		public void setOther_notes(String other_notes) {
			this.other_notes = other_notes;
		}
		public String getTreatment_given() {
			return treatment_given;
		}
		public void setTreatment_given(String treatment_given) {
			this.treatment_given = treatment_given;
		}
	

}
