package com.dhanush.infotech.project.GOIL2.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.repository.LiveCaesareanRepository;
import com.dhanush.infotech.project.GOIL2.repository.NeonatalAsphyxiaViewRepository;
import com.dhanush.infotech.project.GOIL2.repository.RecordingMotherBPRepository;
import com.dhanush.infotech.project.GOIL2.repository.TotalDeliveryRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class DashBoardReports2 {
	
	public static String TOTALADMISSIONS="admissions_total";
	public static String STILLBIRTH="stillbirth_total";
	public static String MATERNALDEATH ="maternal_death_total";
	public static String TOTALECLAMPSIA ="pre_eclampsia_eclamps_pecentage";
	public static String PPH ="pph_pecentage"; 
	public static String CAESAREANDELIVERYPEC ="caesarean_section_pecentage";
	public static String STILLBIRTHPERC ="stillbirth_pecentage";
	public static String NEONATALASPHYXIA ="live_birth_with_neo_natal_asphyxia_pecentage";
	public static String PROLONGEDLABOR="prolonged_labor_pecentage";
	public static Long DELIVERIES = 0L;
	public static Long STILLBIRTHCOUNT = 0L;
	
	@Autowired
	RecordingMotherBPRepository recordingMotherBPRepository;
	
	@Autowired
	TotalDeliveryRepository totalDeliveryRepository ;
	
	@Autowired
	LiveCaesareanRepository liveCaesareanRepository;
	
	
	@Autowired
	NeonatalAsphyxiaViewRepository neonatalAsphyxiaViewRepository;
	

	@CrossOrigin
	@RequestMapping(value = "outcomeindicator/l2", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Object> discharge(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		String  block = obj.getString("block_code").toString();
		String district = obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String facility = obj.getString("facility_code").toString();
		String facilitytype = obj.getString("facility_type_id").toString();
		String endDate = obj.getString("end_date").toString();
		String startDate = obj.getString("start_date").toString();
		JSONObject result = new JSONObject();
		JSONObject kpiResult1 = new JSONObject();
		 Map<String,Object> kpiResult = new HashMap<>();
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		   LocalDateTime now = LocalDateTime.now();
		 if(startDate.equalsIgnoreCase("ALL"))
		 {
			 startDate = "1995-01-01";
			 endDate = ""+dtf.format(now);
		 }
		 kpiResult.put(TOTALADMISSIONS,  totalAdmissions(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(STILLBIRTH,  totalStillBirth(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(MATERNALDEATH,  totalMaternalDeath(state, district, block, facilitytype, facility, startDate, endDate));
		 kpiResult.put(TOTALECLAMPSIA,  totalEclampsia(state, district, block, facilitytype, facility, startDate, endDate));
		 kpiResult.put(PPH, totalPPH(state, district, block, facilitytype, facility, startDate, endDate));	
		 kpiResult.put(CAESAREANDELIVERYPEC, totalCAESAREANDELIVERY(state, district, block, facilitytype, facility, startDate, endDate));
		 kpiResult.put(STILLBIRTHPERC, stillBirth());
		kpiResult.put(NEONATALASPHYXIA, babyrequireresuscitation(state, district, block, facilitytype, facility, startDate, endDate));
		kpiResult.put(PROLONGEDLABOR, totalProLogedLabour(state, district, block, facilitytype, facility, startDate, endDate));
		result.put("result", kpiResult);
		 result.put("total", 8);
		 result.put("message", "8 Records Found");
		 kpiResult1.put("data", result);
		 kpiResult1.put("success", true);
		 return kpiResult1.toMap();
	}
	public String stillBirth() {
		Long total;
		if(STILLBIRTHCOUNT == null || STILLBIRTHCOUNT == 0 && DELIVERIES == 0 || DELIVERIES == null)
		{
			total = (long) 0;
		}
		else {
			total = STILLBIRTHCOUNT *100 /DELIVERIES;
		}
		return total.toString();
		
	}
	 public String totalAdmissions(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		   if (state.length() > 0 && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total =recordingMotherBPRepository.findByState1(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByDistrict1(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByBlock1(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = recordingMotherBPRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
			}
		   if(total == null || total == 0)
			   total = 0L;
		   return total.toString();
		  
	   }
	 
	
	 
	 public String totalStillBirth(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   
		 if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 STILLBIRTHCOUNT = liveCaesareanRepository.findByState3(state,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByDistrict3(state,district,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByBlock3(state,district,block,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacilitytype3(state,district,block,facilitytype,startDate,endDate);
			}
		 if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
			   STILLBIRTHCOUNT = liveCaesareanRepository.findByFacility3(state,district,block,facilitytype,facility,startDate,endDate);
			}
		   if(STILLBIRTHCOUNT == null || STILLBIRTHCOUNT == 0)
			   STILLBIRTHCOUNT = 0L;
		  
		   return STILLBIRTHCOUNT.toString();
	   }
	
	 
	 public String totalMaternalDeath(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		  
		 
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {

				 total = liveCaesareanRepository.findByState4(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByDistrict4(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByBlock4(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacilitytype4(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacility4(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   if(total == null || total == 0)
			   total = 0L;
		  
		   return total.toString();
	   }
	 
	 @SuppressWarnings("unused")
	public String totalEclampsia(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		  
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 total = liveCaesareanRepository.findByState5(state,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByState(state,startDate,endDate);
				 
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByDistrict5(state,district,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByBlock5(state,district,block,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacilitytype5(state,district,block,facilitytype,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
		   }
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacility5(state,district,block,facilitytype,facility,startDate,endDate)*100;
				 DELIVERIES = totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   Long result;
		   if(total == null || total == 0 && DELIVERIES == null || DELIVERIES == 0) {
			   total = 0L;
			   DELIVERIES = 0L;
			   result = (long) 0;
		   }
		   else {
		   result = total/DELIVERIES;
		   }
		   return result.toString();
	   } 
	 
	 
	 @SuppressWarnings("unused")
		public String totalPPH(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
		   {
			   Long total = 0L;
			   
			  
			   
			   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {

					 total = liveCaesareanRepository.findByState6(state,startDate,endDate)*100;
					System.out.println(total+"pph");
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = liveCaesareanRepository.findByDistrict6(state,district,startDate,endDate)*100;
					
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByBlock6(state,district,block,startDate,endDate)*100;
					
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByFacilitytype6(state,district,block,facilitytype,startDate,endDate)*100;
					
			   }
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
					 total = liveCaesareanRepository.findByFacility6(state,district,block,facilitytype,facility,startDate,endDate)*100;
				}
			   Long result ;
			   if(total == null || total == 0 && DELIVERIES == null || DELIVERIES == 0) {
				   total = 0L;
				   DELIVERIES = 0L;
				   result = (long) 0;
			   }
			   else {
				   System.out.println(total+"tot"+DELIVERIES+"del");
			    result = total/DELIVERIES;
			   }
			   return result.toString();
		   } 
	 
	  @SuppressWarnings("unused")
	public String totalCAESAREANDELIVERY(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByState(state,startDate,endDate)*100;
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByDistrict(state,district,startDate,endDate)*100;
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByBlock(state,district,block,startDate,endDate)*100;
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate)*100;
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
				 total = liveCaesareanRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate)*100;
			}
		   Long result;
		   if(total == null || total ==0 && DELIVERIES == null || DELIVERIES ==0) {
			   total = 0L;
			   DELIVERIES = 0L;
			   result =(long) 0;
		   }
		   else
		   {
		    result = total/DELIVERIES;
		   }
		    return result.toString();
	   }
	  
	  @SuppressWarnings("unused")
		public String babyrequireresuscitation(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
		   {
			   Long total = 0L;
			   Long liveBirth =0L;
			   System.out.println();
			   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = neonatalAsphyxiaViewRepository.findByState(state,startDate,endDate)*100;
					 liveBirth = liveCaesareanRepository.findByState1(state,startDate,endDate);
					 System.out.println("nex"+total+"live"+liveBirth);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = neonatalAsphyxiaViewRepository.findByDistrict(state,district,startDate,endDate)*100;
					 liveBirth = liveCaesareanRepository.findByDistrict1(state,district,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = neonatalAsphyxiaViewRepository.findByBlock(state,district,block,startDate,endDate)*100;
					 liveBirth = liveCaesareanRepository.findByBlock1(state,district,block,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = neonatalAsphyxiaViewRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate)*100;
					 liveBirth = liveCaesareanRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
					 total = neonatalAsphyxiaViewRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate)*100;
					
					 liveBirth = liveCaesareanRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
			
			   }
			   Long result;
			   if(total == null || total == 0 && liveBirth == null || liveBirth == 0) {
				   total = 0L;
				   liveBirth = 0L;
				   result =(long) 0;
			   }
			   else {
			    result = total/liveBirth;
			   }
			   return result.toString();
		   }
	 
	
	  
	  
	  
	  
	  @SuppressWarnings("unused")
		public String totalProLogedLabour(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
		   {
			   Long total = 0L;
			   
			   
				   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
							&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {

					 total = liveCaesareanRepository.findByState8(state,startDate,endDate)*100;
					 DELIVERIES = totalDeliveryRepository.findByState(state,startDate,endDate);
				}
				   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
							&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByDistrict8(state,district,startDate,endDate)*100;
					 DELIVERIES = totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
				}
				   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
							&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByBlock8(state,district,block,startDate,endDate)*100;
					 DELIVERIES = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
				}
				   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
							&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByFacilitytype8(state,district,block,facilitytype,startDate,endDate)*100;
					 DELIVERIES = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
			   }
				   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
							&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL"))  {
					 total = liveCaesareanRepository.findByFacility8(state,district,block,facilitytype,facility,startDate,endDate)*100;
					 DELIVERIES = totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
				
				   }
			   Long result;
			   if(total == null || total == 0 && DELIVERIES == null || DELIVERIES == 0) {
				   total = 0L;
				   DELIVERIES = 0L;
				   result = (long) 0;
			   }
			   else {
			   result = total/DELIVERIES;
			   System.out.println(total+"total"+DELIVERIES);
			   }
			   return result.toString();
		   } 
}
