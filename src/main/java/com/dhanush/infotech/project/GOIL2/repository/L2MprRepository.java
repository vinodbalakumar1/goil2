package com.dhanush.infotech.project.GOIL2.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.L2MprView;

@Repository
public interface L2MprRepository extends JpaRepository<L2MprView, Long>
{

	

	//normal deliveries mp report
	
	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start% and a.typeofdelivery = '87' ")
	Long findByState(@Param("state")String state, @Param("start") String start);


	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.typeofdelivery = '87'")
	Long findByDistrict(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.typeofdelivery = '87'")
	Long findByBlock(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.typeofdelivery = '87'")
	Long findByFacilitytype(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.typeofdelivery = '87'")
	Long findFacilityCode(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
			@Param("start")String start);


	//for assisted
	
	
	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start% and a.typeofdelivery = '88' ")
	Long findStateAssistDelivery(@Param("state")String state,@Param("start") String start);


	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.typeofdelivery = '88' ")
	Long findDistrictAssistDelivery(@Param("state")String state,@Param("district") String district,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.typeofdelivery = '88'")
	Long findBlockAssistDelivery(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.typeofdelivery ='88'")
	Long findFacilitytypeAssistDelivery(@Param("state")String state,@Param("district") String district,@Param("block") String block, @Param("facilitytype")String facilitytype,@Param("start")String start);

	@Query(value = "select  count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.typeofdelivery ='88'")
	Long findFacilityCodeAssistDelivery(@Param("state")String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,
			@Param("facility")String facility,@Param("start") String start);


	// for CaesareanDelivery
	

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start% and a.typeofdelivery = '89' ")
	Long findStateCaesareanDelivery(@Param("state")String state,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.typeofdelivery = '89' ")
	Long findDistrictCaesareanDelivery(@Param("state")String state,@Param("district") String district,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.typeofdelivery = '89'")
	Long findBlockCaesareanDelivery(@Param("state")String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.typeofdelivery = '89'")
	Long findFacilitytypeCaesareanDelivery(@Param("state")String state,@Param("district") String district, @Param("block")String block, @Param("facilitytype")String facilitytype,
			@Param("start")String start);

	@Query(value = "select  count(a.typeofdelivery) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.typeofdelivery ='89'")
	Long findFacilityCodeCaesareanDelivery(@Param("state")String state,@Param("district") String district,@Param("block")  String block, @Param("facilitytype") String facilitytype,
			@Param("facility") String facility,@Param("start") String start);

	//for still birth



	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%' ")
	 Long findStateStillBirth(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%'")
	 Long findDistrictStillBirth(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.deliveryoutcome1 like '%93%'")
	 Long findBlockStillBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and  a.deliveryoutcome1 like '%93%'")
	 Long findFacilitytypeStillBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.deliveryoutcome1 like '%93%'")
	 Long findFacilityCodeStillBirth(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);


	
	
//for live birth
	
	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	 Long findStateLiveBirth(@Param("state")String state, @Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	 Long findDistrictLiveBirth(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.deliveryoutcome1 like '%92%'")
	 Long findBlockLiveBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	 @Query(value = "select count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and  a.deliveryoutcome1 like '%92%'")
	 Long findFacilitytypeLiveBirth(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	 @Query(value = "select  count(a.deliveryoutcome1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.deliveryoutcome1 like '%92%'")
	 Long findFacilityCodeLiveBirth(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
	   @Param("start")String start);


	 // for maternal deaths
	 
	 @Query(value = "select count(a.maternaldeath) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.maternaldeath in ('true','Yes')")
		Long findByStateMaternalDeaths(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.maternaldeath) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.maternaldeath in ('true','Yes')")
		Long findByDistrictMaternalDeaths(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.maternaldeath) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.maternaldeath in ('true','Yes')")
		Long findByBlockMaternalDeaths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.maternaldeath) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.maternaldeath in ('true','Yes')")
		Long findByFacilitytypeMaternalDeaths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.maternaldeath) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.maternaldeath in ('true','Yes')")
		Long findByFacilityCodeMaternalDeaths(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);

		//for obstructed labour
		
		

		 @Query(value = "select count(a.deliverycomplications) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.deliverycomplications like '%99%' ")
		 Long findStateObstructedLabor(@Param("state")String state, @Param("start") String start);

		 @Query(value = "select count(a.deliverycomplications) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.deliverycomplications like '%99%' ")
		 Long findDistrictObstructedLabor(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		 @Query(value = "select count(a.deliverycomplications) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.deliverycomplications like '%99%' ")
		 Long findBlockObstructedLabor(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		 @Query(value = "select count(a.deliverycomplications) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.deliverycomplications like '%99%' ")
		 Long findFacilitytypeObstructedLabor(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		 @Query(value = "select  count(a.deliverycomplications) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.deliverycomplications like '%99%' ")
		 Long findFacilityCodeObstructedLabor(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
		   @Param("start")String start);
		 
      // for eclampsia

			
		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and (deliverycomplications like '%96%' or deliverycomplications like '%97%') ")
		 Long findStateEclampsia(@Param("state")String state, @Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and (deliverycomplications like '%96%' or deliverycomplications like '%97%')")
		 Long findDistrictEclampsia(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and (deliverycomplications like '%96%' or deliverycomplications like '%97%')")
		 Long findBlockEclampsia(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and (deliverycomplications like '%96%' or deliverycomplications like '%97%')")
		 Long findFacilitytypeEclampsia(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		 @Query(value = "select  count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and (deliverycomplications like '%96%' or deliverycomplications like '%97%')")
		 Long findFacilityCodeEclampsia(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
		   @Param("start")String start);


		 
		 // for preterm
		 


		@Query(value = "select count(a.preterm) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.preterm in('Yes') ")
		Long findStatePreTermBirths(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.preterm) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.preterm in('Yes')")
		Long findDistrictPreTermBirths(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.preterm) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.preterm in('Yes')")
		Long findBlockPreTermBirths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.preterm) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.preterm in('Yes')")
		Long findFacilitytypePreTermBirths(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.preterm) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.preterm in('Yes')")
		Long findFacilityCodePreTermBirths(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);

// for sepsis
		
	

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.deliverycomplications like '%480%'")
		 Long findStateSepsis(@Param("state")String state, @Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.deliverycomplications like '%480%'")
		 Long findDistrictSepsis(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.deliverycomplications like '%480%'")
		 Long findBlockSepsis(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.deliverycomplications like '%480%'")
		 Long findFacilitytypeSepsis(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		 @Query(value = "select  count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.deliverycomplications like '%480%' ")
		 Long findFacilityCodeSepsis(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
		   @Param("start")String start);

// for pph
	

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start% and a.deliverycomplications like '%100%' ")
		 Long findStatePph(@Param("state")String state, @Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.deliverycomplications like '%100%' ")
		 Long findDistrictPph(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.deliverycomplications like '%100%' ")
		 Long findBlockPph(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		 @Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.deliverycomplications like '%100%' ")
		 Long findFacilitytypePph(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		 @Query(value = "select  count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.deliverycomplications like '%100%' ")
		 Long findFacilityCodePph(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
		   @Param("start")String start);
		 
// for partograph


		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.partograph in ('true','1')")
		Long findStatePartographs(@Param("state") String state,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start% and a.partograph in ('true','1')")
		Long findDistrictPartographs(@Param("state") String state,@Param("district") String district,@Param("start") String start);

		
		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and  a.partograph in ('true','1') ")
		Long findBlockPartographs(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and  a.partograph in ('true','1')")
		Long findFacilitytypePartographs(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.partograph in ('true','1')")
		Long findFacilityPartographs(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);




//for Uterotonic
	

		@Query(value = "select count(a.uterotonic) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start% and a.uterotonic ='Inj Oxytocin' ")
		Long findStateUterotonic(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.uterotonic) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.uterotonic ='Inj Oxytocin'")
		Long findDistrictUterotonic(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.uterotonic) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start%  and a.uterotonic ='Inj Oxytocin'")
		Long findBlockUterotonic(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.uterotonic) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and a.uterotonic ='Inj Oxytocin'")
		Long findFacilitytypeUterotonic(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.uterotonic) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start% and a.uterotonic ='Inj Oxytocin'")
		Long findFacilityCodeUterotonic(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);

// for ppiucd
		
		
		@Query(value = "select count(a.ppiucd) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
		Long findStatePpiucd(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(a.ppiucd) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
		Long findDistrictPpiucd(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(a.ppiucd) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and a.ppiucd in ('1','Yes')")
		Long findBlockPpiucd(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(a.ppiucd) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
		Long findFacilitytypePpiucd(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(a.ppiucd) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and a.ppiucd in ('1','Yes')")
		Long findFacilityCodePpiucd(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);


		// for safe child list
		

		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.admissiondate like :start%  and (a.whetherscc1wasfilled='1' OR a.whetherscc2wasfilled='1' OR a.whetherscc3wasfilled in ('true','Yes','1'))")
		Long findStateSafeChildList(@Param("state")String state, @Param("start") String start);

		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.admissiondate like :start%  and (a.whetherscc1wasfilled='1' OR a.whetherscc2wasfilled='1' OR a.whetherscc3wasfilled in ('true','Yes','1')) ")
		Long findDistrictSafeChildList(@Param("state")String state,@Param("district")String district, @Param("start")String start);

		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.admissiondate like :start% and (a.whetherscc1wasfilled='1' OR a.whetherscc2wasfilled='1' OR a.whetherscc3wasfilled in ('true','Yes','1'))")
		Long findBlockSafeChildList(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

		@Query(value = "select count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.admissiondate like :start% and (a.whetherscc1wasfilled='1' OR a.whetherscc2wasfilled='1' OR a.whetherscc3wasfilled in ('true','Yes'))")
		Long findFacilitytypeSafeChildList(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(1) as count FROM L2MprView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.admissiondate like :start%  and (a.whetherscc1wasfilled='1' OR a.whetherscc2wasfilled='1' OR a.whetherscc3wasfilled in ('true','Yes','1'))")
		Long findFacilityCodeSafeChildList(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
				@Param("start")String start);


	
	

}
