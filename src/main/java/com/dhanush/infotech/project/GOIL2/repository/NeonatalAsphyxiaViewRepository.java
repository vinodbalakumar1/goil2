package com.dhanush.infotech.project.GOIL2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL2.model.NeonatalAsphyxiaView;

public interface NeonatalAsphyxiaViewRepository extends JpaRepository<NeonatalAsphyxiaView, Long>{

	
	@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.createdat > :start and a.createdat < :end and a.babyrequireresuscitation in ('true','yes','1')")
	Long findByState(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.babyrequireresuscitation in ('true','yes','1')")
	Long findByDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.babyrequireresuscitation in ('true','yes','1')")
	Long findByBlock(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.babyrequireresuscitation in ('true','yes','1')")
	Long findByFacilitytype(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.babyrequireresuscitation in ('true','yes','1')")
	Long findByFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
	
	//Asphyxia for mpreports
		@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.createdat like :start% and  a.babyrequireresuscitation in ('true','yes','1')")
		Long findByStateAsphyxia(@Param("state") String state,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district=:district and a.createdat like :start% and  a.babyrequireresuscitation in ('true','yes','1')")
		Long findByDistrictAsphyxia(@Param("state") String state,@Param("district") String district,@Param("start") String start);

		
		@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.createdat like :start%  and a.babyrequireresuscitation in ('true','Yes','1')")
		Long findByBlockAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
		
		
		@Query(value = "select count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat like :start%  and a.babyrequireresuscitation in ('true','Yes','1')")
		Long findByFacilitytypeAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

		@Query(value = "select  count(1) as count FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat like :start% and a.babyrequireresuscitation in ('true','Yes','1')")
		Long findByFacilityAsphyxia(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);
		
	// state dashboard
		
		
		@Query(value = "select count(1) as count,a.district,a.districtname FROM NeonatalAsphyxiaView a where a.state = :state and a.createdat like :date% and a.babyrequireresuscitation in ('true','yes','1') group by a.district,a.districtname")
		List<NeonatalAsphyxiaView> SDByState(@Param("state") String state,@Param("date") String date);
		
		
		@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.createdat like :date% and a.babyrequireresuscitation in ('true','yes','1') group by a.district,a.districtname,a.facility,a.facilityname")
		List<NeonatalAsphyxiaView> SDByDistrict(@Param("state") String state,@Param("district") String district,@Param("date") String date);

		
		
		@Query(value = "select count(1) as count,a.district,a.districtname FROM NeonatalAsphyxiaView a where a.state = :state and a.createdat like :date% and a.babyrequireresuscitation in ('true','yes','1') group by a.district,a.districtname")
		List<Object[]> objState(@Param("state") String state,@Param("date") String date);
		
		
		@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM NeonatalAsphyxiaView a where a.state = :state and a.district = :district and a.createdat like :date% and a.babyrequireresuscitation in ('true','yes','1') group by a.district,a.districtname,a.facility,a.facilityname")
		List<Object[]> objDistrict(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
		
	
}
