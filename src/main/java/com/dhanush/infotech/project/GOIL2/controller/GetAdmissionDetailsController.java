package com.dhanush.infotech.project.GOIL2.controller;

import java.util.List;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIL2.model.Admission;
import com.dhanush.infotech.project.GOIL2.model.ListChild;
import com.dhanush.infotech.project.GOIL2.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL2.repository.ListChildRepository;

@RestController
@RequestMapping("/api")
public class GetAdmissionDetailsController {

	@Autowired
	AdmissionRepository admissionRepository;

	@Autowired
	ListChildRepository listChildRepository;

	@SuppressWarnings("unchecked")
	@CrossOrigin
	@RequestMapping(value = "/getadmissiondetailsbyid/l2/{id}", method = RequestMethod.GET, produces = {
			"application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
		Admission dischargeDetails = admissionRepository.findById(UserId)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		JSONObject normal = new JSONObject();
		JSONObject obj = new JSONObject();
		try {
		obj.put("admissionId", ""+dischargeDetails.getId());
		obj.put("unique_id", dischargeDetails.getUniqueNo());
		obj.put("state", dischargeDetails.getState());
		obj.put("district", dischargeDetails.getDistrict());
		obj.put("block", dischargeDetails.getBlock());
		obj.put("facility", dischargeDetails.getFacility());
		obj.put("facilityType", dischargeDetails.getFacilityType());
		org.json.simple.JSONArray arr = new org.json.simple.JSONArray();
		List<ListChild> childList = listChildRepository.findByAdmissionId(UserId);
		for(ListChild listChild : childList) {
			org.json.JSONObject listObj = new org.json.JSONObject();
			listObj.put("child_id",""+ listChild.getId());
			listObj.put("deliveryOutcome", listChild.getUniqueNo());
			listObj.put("birthWeight", listChild.getBirthWeight());
			listObj.put("unique_id", listChild.getUniqueNo());
			listObj.put("deliveryOutCome1", listChild.getDeliveryOutCome1());
			listObj.put("deliveryOutCome2", listChild.getDeliveryOutCome2());
			listObj.put("abortion", listChild.getAbortion());
			listObj.put("sex", listChild.getSex());
			listObj.put("preterm", listChild.getPreterm());
			listObj.put("birthWeight", listChild.getBirthWeight());
			listObj.put("deliveryDate", listChild.getDeliveryDate());
			listObj.put("deliveryTime", listChild.getDeliveryTime());
			listObj.put("immunization", listChild.getImmunization());
			listObj.put("modeOfDelivery", listChild.getModeOfDelivery());
			listObj.put("deliveryOthers", listChild.getDeliveryOthers());
			listObj.put("abortionParity", listChild.getAbortion());
			listObj.put("vitaminKAdministered", listChild.getVitaminKAdministered());
			arr.add(listObj);
		}

		JSONObject obj1 = new JSONObject();
		obj1.put("providerPhoneNo",""+dischargeDetails.getProviderPhoneNo());
		obj1.put("providerDate",""+ dischargeDetails.getProviderDate());
		obj1.put("providerTime",""+dischargeDetails.getProviderTime());
		obj1.put("mctsNo", dischargeDetails.getMctsNo());
		obj1.put("booked", dischargeDetails.getBooked());
		obj1.put("district_code",dischargeDetails.getDistrictname());
		obj1.put("block_code",dischargeDetails.getBlockname());
		obj1.put("nameOfFacility_code",dischargeDetails.getFacilityname());
		obj1.put("ipdRegisterdNumber", dischargeDetails.getIpdRegisterdNumber());
		obj1.put("bplJsyRegister", dischargeDetails.getBplJsyRegister());
		obj1.put("aadharCardNumber", dischargeDetails.getAadharCardNumber());
		obj1.put("refferedFrom", dischargeDetails.getRefferedFrom());
		obj1.put("refferedReason", dischargeDetails.getRefferedReason());
		obj1.put("contactNumber", dischargeDetails.getContactNumber());
		obj1.put("nameOfAsha", dischargeDetails.getNameOfAsha());
		obj1.put("patientName", dischargeDetails.getPatientName());
		obj1.put("pctsno", dischargeDetails.getPctsIdNo());
		obj1.put("age", dischargeDetails.getAge());
		obj1.put("woDo", dischargeDetails.getWoDo());
		obj1.put("address", dischargeDetails.getAddress());
		obj1.put("patientContactNumber", dischargeDetails.getPatientContactNumber());
		obj1.put("martialStatus", dischargeDetails.getMartialStatus());
		obj1.put("admissionDate", dischargeDetails.getAdmissionDate());
		obj1.put("time", dischargeDetails.getTime());
		obj1.put("admissionCategory", dischargeDetails.getAdmissionCategory());
		obj1.put("lmp", dischargeDetails.getLmp());
		obj1.put("edd", dischargeDetails.getEdd());
		obj1.put("provisionalDiagnosis", dischargeDetails.getProvisionalDiagnosis());
		obj1.put("finalDiagnosis", dischargeDetails.getFinalDiagnosis());
		obj1.put("contraceptionHistory", dischargeDetails.getContraceptionHistory());
		obj1.put("patientOthers", dischargeDetails.getOthers1());
		obj1.put("indicationForAssistedLSCSOthers", dischargeDetails.getIndicationForAssistedLSCSOthers());
		obj1.put("finalOutCome", dischargeDetails.getFinalOutCome());
		obj1.put("abortionParity", dischargeDetails.getAbortionParity());
		obj1.put("nameOfServiceProvider", dischargeDetails.getNameOfServiceProvider());
		obj1.put("designation", dischargeDetails.getDesignation());
		obj1.put("presentingComplaints", dischargeDetails.getPresentingComplaints());
		obj1.put("pastObstetricsHistory", dischargeDetails.getPastObstetricsHistory());
		obj1.put("othersSpecify", dischargeDetails.getOthers2());
		obj1.put("medicalSurgicalHistory", dischargeDetails.getMedicalSurgicalHistory());
		obj1.put("familyHoChronicIllness", dischargeDetails.getFamilyHoChronicIllness());
		obj1.put("dateOnSetOfLabour", dischargeDetails.getDateOnSetOfLabour());
		obj1.put("timeOfOnsetOfLabour", dischargeDetails.getTimeOfOnsetOfLabour());
		obj1.put("gravida", dischargeDetails.getGravida());
		obj1.put("parity", dischargeDetails.getParity());
		obj1.put("livingChildren", dischargeDetails.getLivingChildren());
		obj1.put("district_code", dischargeDetails.getDistrictname());
		obj1.put("block_code", dischargeDetails.getBlockname());
		obj1.put("nameOfFacility_code", dischargeDetails.getFacilityname());
		obj1.put("listChild", arr);
		JSONObject generalExamination = new JSONObject();
		generalExamination.put( "generalExamHeight",dischargeDetails.getGeneralExamHeight());
		generalExamination.put(  "generalExamWeight",dischargeDetails.getGeneralExamWeight());
		generalExamination.put( "generalExamPallor",dischargeDetails.getGeneralExamPallor());
		generalExamination.put("generalExamJaundice",dischargeDetails.getGeneralExamJaundice());
		generalExamination.put("generalExamPedalEdema",dischargeDetails.getGeneralExamPedalEdema());
		
		JSONObject vitals = new JSONObject();
		vitals.put("vitalsBpDiastolic", dischargeDetails.getVitalsBpDiastolic());
		vitals.put("vitalsBpSystolic",dischargeDetails.getVitalsBpSystolic());
		vitals.put("vitalsTemparature", dischargeDetails.getVitalsTemparature());
		vitals.put("vitalsPulse", dischargeDetails.getVitalsPulse());
		vitals.put("vitalsRespiratoryRate", dischargeDetails.getVitalsRespiratoryRate());
		vitals.put("vitalsFhr", dischargeDetails.getVitalsFhr());

		JSONObject pAExamination = new JSONObject();
		pAExamination.put("pAExampresentationCephalic",
				dischargeDetails.getpAExampresentationCephalic());
		pAExamination.put("pAExamOthers", dischargeDetails.getpAExamOthers());
		pAExamination.put("pAExamEngagement", dischargeDetails.getpAExamEngagement());
		pAExamination.put("pAExamLie", dischargeDetails.getpAExamLie());

		JSONObject gestationalAge = new JSONObject();

		gestationalAge.put("gestationalAgepreTerm", dischargeDetails.getGestationalAgepreTerm());
		gestationalAge.put("gestationalAgeantenatalCorticosteroid",
				dischargeDetails.getGestationalAgeantenatalCorticosteroid());
		gestationalAge.put("gestationalAgelmp", dischargeDetails.getGestationalAgelmp());
		gestationalAge.put("gestationalAgeedd", dischargeDetails.getGestationalAgeedd());
		gestationalAge.put("gestationalAgefundaMentalHeights",
				dischargeDetails.getGestationalAgefundaMentalHeights());
		gestationalAge.put("gestationalAgefinalEstimatedAge",
				dischargeDetails.gestationalAgefinalEstimatedAge);
		gestationalAge.put("gestationalAgeageFromUsg",
				dischargeDetails.getGestationalAgeageFromUsg());
		JSONObject others = new JSONObject();
		others.put("field1", ""+dischargeDetails.getField1());
		others.put("field2", ""+dischargeDetails.getField2());
		others.put("field3", ""+dischargeDetails.getField3());
		others.put("field4", ""+dischargeDetails.getField4());
		others.put("field5", ""+dischargeDetails.getField5());
		others.put("field6", ""+dischargeDetails.getField6());
		others.put("field7", ""+dischargeDetails.getField7());
		others.put("field8", ""+dischargeDetails.getField8());
		others.put("field9", ""+dischargeDetails.getField9());
		others.put("field10",""+ dischargeDetails.getField10());

		JSONObject pVExamination = new JSONObject();

		pVExamination.put("noOfPVExaminations", dischargeDetails.getPvExamNoOfPVExaminations());
		pVExamination.put("cervicalDilation", dischargeDetails.getPvExamCervicalDilation());
		pVExamination.put("cervicalEffacement", dischargeDetails.getPvExamCervicalEffacement());
		pVExamination.put("membranes", dischargeDetails.getPvExamMembranes());
		pVExamination.put("colourOfAmnioticFluid", dischargeDetails.getPvExamColourOfAmnioticFluid());
		pVExamination.put("pelvisAdequate", dischargeDetails.getPvExamPelvisAdequate());

		JSONObject labExamination = new JSONObject();

		labExamination.put("bloodGroupRh", dischargeDetails.getLabExamBloodGroupRh());
		labExamination.put("labExamHb", dischargeDetails.getLabExamHb());
		labExamination.put("urineProtein", dischargeDetails.getLabExamUrineProtein());
		labExamination.put("labExamHiv", dischargeDetails.getLabExamHiv());
		labExamination.put("vDRLRPR", dischargeDetails.getLabExamVdrlrpr());
		labExamination.put("malaria", dischargeDetails.getLabExamMalaria());
		labExamination.put("antiDGivenHb", dischargeDetails.getLabExamAntiDGivenHb());
		labExamination.put("labExamBloodSugar", dischargeDetails.getLabExamBloodSugar());
		labExamination.put("labExamUrineSugar", dischargeDetails.getLabExamUrineSugar());
		labExamination.put("labExamHBsAg", dischargeDetails.getLabExamHBsAg());
		labExamination.put("labExamSyphilis", dischargeDetails.getLabExamSyphilis());
		labExamination.put("labExamGDM", dischargeDetails.getLabExamGDM());
		labExamination.put("others", dischargeDetails.getLabExamOthers());
		System.out.println(labExamination);
	
		obj.put("admission", obj1);
		obj.put("generalExamination",generalExamination);
		obj.put("vitals", vitals);
		obj.put("gestationalAge", gestationalAge);
		obj.put("pAExamination", pAExamination);
		obj.put("pVExamination", pVExamination);
		obj.put("labExamination", labExamination);
		obj.put("others", others);
		normal.put("status", "ok");
		normal.put("data", obj);
	}
		catch(Exception e)
		{
			normal.put("status", "No Record Found");
			normal.put("data", obj);
		}
		return normal.toString();
	}
}
