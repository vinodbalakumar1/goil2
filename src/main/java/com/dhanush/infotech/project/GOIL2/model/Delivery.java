package com.dhanush.infotech.project.GOIL2.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l2_delivery")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)

public class Delivery {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt = new Date();

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt = new Date();;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "admissionId", nullable = false)
	private Admission admission;

	private String whetherScc1WasFilled;
	private String whetherSsc2WasFilled;
	private String augmentationPerformed;
	private String ifyesPleaseSpecifyTheIndiAug;
	private String whetherPartographWasFilled;

	private String deliveryNotesDate;
	private String deliveryNotesTime;
	private String typeOfDelivery;
	private String assisted;
	private String deliveryNotesOthers;
	private String deliveryOutcome1;
	private String deliveryOutcome2;
	private String deliveryNotesPreterm;
	private String deliveryNotesAbortion;
	private String deliveryNotesEpisiotomy;
	private String deliveryNotesAmstlPerformed;
	private String deliveryNotesUterotonicAdministered;
	private String deliveryNotesCct;
	private String deliveryNotesUterineMassage;
	private String deliveryNotesComplications;
	private String deliveryNotesMaternalDeath;
	private String deliveryNotesMaternaldeathTime;
	private String deliveryNotesIfMaternalDeathCauseOfDeath;
	private String deliveryNotesPpiucdInserted;
	private String postDeliveryNotes;
	private String bloodTransfusionOrOtherProcedureNotes;
	private String whetherScc3WasFilled;
	private String notesForMother;
	private String notesForBaby;
	private String clinicalDiagnosisIfAnyConditionPresen;
	private String postDeliveryOthers;
	private String dateAndTimeOfTransferToPNCWard;
	private String dateAndTimeOfTransferToPNCWardTime;
	private String conditionAtTransferToPncWard;
	private String ifReferredReasonForReferralOfMotherBaby;
	private String WhetherSCC4wasfilled;

	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	
	
	
	
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Admission getAdmission() {
		return admission;
	}

	public void setAdmission(Admission admission) {
		this.admission = admission;
	}

	public String getWhetherScc1WasFilled() {
		return whetherScc1WasFilled;
	}

	public void setWhetherScc1WasFilled(String whetherScc1WasFilled) {
		this.whetherScc1WasFilled = whetherScc1WasFilled;
	}

	public String getWhetherSsc2WasFilled() {
		return whetherSsc2WasFilled;
	}

	public void setWhetherSsc2WasFilled(String whetherSsc2WasFilled) {
		this.whetherSsc2WasFilled = whetherSsc2WasFilled;
	}

	public String getAugmentationPerformed() {
		return augmentationPerformed;
	}

	public void setAugmentationPerformed(String augmentationPerformed) {
		this.augmentationPerformed = augmentationPerformed;
	}

	public String getIfyesPleaseSpecifyTheIndiAug() {
		return ifyesPleaseSpecifyTheIndiAug;
	}

	public void setIfyesPleaseSpecifyTheIndiAug(String ifyesPleaseSpecifyTheIndiAug) {
		this.ifyesPleaseSpecifyTheIndiAug = ifyesPleaseSpecifyTheIndiAug;
	}

	public String getWhetherPartographWasFilled() {
		return whetherPartographWasFilled;
	}

	public void setWhetherPartographWasFilled(String whetherPartographWasFilled) {
		this.whetherPartographWasFilled = whetherPartographWasFilled;
	}

	public String getDeliveryNotesDate() {
		return deliveryNotesDate;
	}

	public void setDeliveryNotesDate(String deliveryNotesDate) {
		this.deliveryNotesDate = deliveryNotesDate;
	}

	public String getDeliveryNotesTime() {
		return deliveryNotesTime;
	}

	public void setDeliveryNotesTime(String deliveryNotesTime) {
		this.deliveryNotesTime = deliveryNotesTime;
	}

	public String getTypeOfDelivery() {
		return typeOfDelivery;
	}

	public void setTypeOfDelivery(String typeOfDelivery) {
		this.typeOfDelivery = typeOfDelivery;
	}

	public String getAssisted() {
		return assisted;
	}

	public void setAssisted(String assisted) {
		this.assisted = assisted;
	}

	public String getDeliveryNotesOthers() {
		return deliveryNotesOthers;
	}

	public void setDeliveryNotesOthers(String deliveryNotesOthers) {
		this.deliveryNotesOthers = deliveryNotesOthers;
	}

	public String getDeliveryOutcome1() {
		return deliveryOutcome1;
	}

	public void setDeliveryOutcome1(String deliveryOutcome1) {
		this.deliveryOutcome1 = deliveryOutcome1;
	}

	public String getDeliveryOutcome2() {
		return deliveryOutcome2;
	}

	public void setDeliveryOutcome2(String deliveryOutcome2) {
		this.deliveryOutcome2 = deliveryOutcome2;
	}

	public String getDeliveryNotesPreterm() {
		return deliveryNotesPreterm;
	}

	public void setDeliveryNotesPreterm(String deliveryNotesPreterm) {
		this.deliveryNotesPreterm = deliveryNotesPreterm;
	}

	public String getDeliveryNotesAbortion() {
		return deliveryNotesAbortion;
	}

	public void setDeliveryNotesAbortion(String deliveryNotesAbortion) {
		this.deliveryNotesAbortion = deliveryNotesAbortion;
	}

	public String getDeliveryNotesEpisiotomy() {
		return deliveryNotesEpisiotomy;
	}

	public void setDeliveryNotesEpisiotomy(String deliveryNotesEpisiotomy) {
		this.deliveryNotesEpisiotomy = deliveryNotesEpisiotomy;
	}

	public String getDeliveryNotesAmstlPerformed() {
		return deliveryNotesAmstlPerformed;
	}

	public void setDeliveryNotesAmstlPerformed(String deliveryNotesAmstlPerformed) {
		this.deliveryNotesAmstlPerformed = deliveryNotesAmstlPerformed;
	}

	public String getDeliveryNotesUterotonicAdministered() {
		return deliveryNotesUterotonicAdministered;
	}

	public void setDeliveryNotesUterotonicAdministered(String deliveryNotesUterotonicAdministered) {
		this.deliveryNotesUterotonicAdministered = deliveryNotesUterotonicAdministered;
	}

	public String getDeliveryNotesCct() {
		return deliveryNotesCct;
	}

	public void setDeliveryNotesCct(String deliveryNotesCct) {
		this.deliveryNotesCct = deliveryNotesCct;
	}

	public String getDeliveryNotesUterineMassage() {
		return deliveryNotesUterineMassage;
	}

	public void setDeliveryNotesUterineMassage(String deliveryNotesUterineMassage) {
		this.deliveryNotesUterineMassage = deliveryNotesUterineMassage;
	}

	public String getDeliveryNotesComplications() {
		return deliveryNotesComplications;
	}

	public void setDeliveryNotesComplications(String deliveryNotesComplications) {
		this.deliveryNotesComplications = deliveryNotesComplications;
	}

	public String getDeliveryNotesMaternalDeath() {
		return deliveryNotesMaternalDeath;
	}

	public void setDeliveryNotesMaternalDeath(String deliveryNotesMaternalDeath) {
		this.deliveryNotesMaternalDeath = deliveryNotesMaternalDeath;
	}

	public String getDeliveryNotesMaternaldeathTime() {
		return deliveryNotesMaternaldeathTime;
	}

	public void setDeliveryNotesMaternaldeathTime(String deliveryNotesMaternaldeathTime) {
		this.deliveryNotesMaternaldeathTime = deliveryNotesMaternaldeathTime;
	}

	public String getDeliveryNotesIfMaternalDeathCauseOfDeath() {
		return deliveryNotesIfMaternalDeathCauseOfDeath;
	}

	public void setDeliveryNotesIfMaternalDeathCauseOfDeath(String deliveryNotesIfMaternalDeathCauseOfDeath) {
		this.deliveryNotesIfMaternalDeathCauseOfDeath = deliveryNotesIfMaternalDeathCauseOfDeath;
	}

	public String getDeliveryNotesPpiucdInserted() {
		return deliveryNotesPpiucdInserted;
	}

	public void setDeliveryNotesPpiucdInserted(String deliveryNotesPpiucdInserted) {
		this.deliveryNotesPpiucdInserted = deliveryNotesPpiucdInserted;
	}

	public String getPostDeliveryNotes() {
		return postDeliveryNotes;
	}

	public void setPostDeliveryNotes(String postDeliveryNotes) {
		this.postDeliveryNotes = postDeliveryNotes;
	}

	public String getBloodTransfusionOrOtherProcedureNotes() {
		return bloodTransfusionOrOtherProcedureNotes;
	}

	public void setBloodTransfusionOrOtherProcedureNotes(String bloodTransfusionOrOtherProcedureNotes) {
		this.bloodTransfusionOrOtherProcedureNotes = bloodTransfusionOrOtherProcedureNotes;
	}

	public String getWhetherScc3WasFilled() {
		return whetherScc3WasFilled;
	}

	public void setWhetherScc3WasFilled(String whetherScc3WasFilled) {
		this.whetherScc3WasFilled = whetherScc3WasFilled;
	}

	public String getNotesForMother() {
		return notesForMother;
	}

	public void setNotesForMother(String notesForMother) {
		this.notesForMother = notesForMother;
	}

	public String getNotesForBaby() {
		return notesForBaby;
	}

	public void setNotesForBaby(String notesForBaby) {
		this.notesForBaby = notesForBaby;
	}

	public String getClinicalDiagnosisIfAnyConditionPresen() {
		return clinicalDiagnosisIfAnyConditionPresen;
	}

	public void setClinicalDiagnosisIfAnyConditionPresen(String clinicalDiagnosisIfAnyConditionPresen) {
		this.clinicalDiagnosisIfAnyConditionPresen = clinicalDiagnosisIfAnyConditionPresen;
	}

	public String getPostDeliveryOthers() {
		return postDeliveryOthers;
	}

	public void setPostDeliveryOthers(String postDeliveryOthers) {
		this.postDeliveryOthers = postDeliveryOthers;
	}

	public String getDateAndTimeOfTransferToPNCWard() {
		return dateAndTimeOfTransferToPNCWard;
	}

	public void setDateAndTimeOfTransferToPNCWard(String dateAndTimeOfTransferToPNCWard) {
		this.dateAndTimeOfTransferToPNCWard = dateAndTimeOfTransferToPNCWard;
	}

	public String getDateAndTimeOfTransferToPNCWardTime() {
		return dateAndTimeOfTransferToPNCWardTime;
	}

	public void setDateAndTimeOfTransferToPNCWardTime(String dateAndTimeOfTransferToPNCWardTime) {
		this.dateAndTimeOfTransferToPNCWardTime = dateAndTimeOfTransferToPNCWardTime;
	}

	public String getConditionAtTransferToPncWard() {
		return conditionAtTransferToPncWard;
	}

	public void setConditionAtTransferToPncWard(String conditionAtTransferToPncWard) {
		this.conditionAtTransferToPncWard = conditionAtTransferToPncWard;
	}

	public String getIfReferredReasonForReferralOfMotherBaby() {
		return ifReferredReasonForReferralOfMotherBaby;
	}

	public void setIfReferredReasonForReferralOfMotherBaby(String ifReferredReasonForReferralOfMotherBaby) {
		this.ifReferredReasonForReferralOfMotherBaby = ifReferredReasonForReferralOfMotherBaby;
	}

	public String getWhetherSCC4wasfilled() {
		return WhetherSCC4wasfilled;
	}

	public void setWhetherSCC4wasfilled(String whetherSCC4wasfilled) {
		WhetherSCC4wasfilled = whetherSCC4wasfilled;
	}

}
