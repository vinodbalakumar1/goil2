package com.dhanush.infotech.project.GOIL2.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.repository.LiveCaesareanRepository;
import com.dhanush.infotech.project.GOIL2.repository.PartographMonitoringViewRepository;
import com.dhanush.infotech.project.GOIL2.repository.RecordingMotherBPRepository;
import com.dhanush.infotech.project.GOIL2.repository.TotalDeliveryRepository;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class DashBoardReports {
	
	public static String TOTALDELIVERY = "delivery_total";
	public static String CAESAREANDELIVERY = "caesarean_delivery_total";
	public static String LIVEBIRTH = "livebirth_total";
	public static String RECPRDINGMOTHERBP ="mother_bp_at_admission_pecentage";
	public static String MotheTemperatureAtDischarge ="mother_temperature_at_discharge_pecentage";
	public static String TotalRecordingbabyTemp ="new_born_temperature_at_discharge_pecentage";
	public static String AMSTL = "amtsl_percentage";
	public static String  PARTOGRAPH ="partograph_monitoring_pecentage";
	
	@Autowired
	TotalDeliveryRepository totalDeliveryRepository ;
	
	
	@Autowired
	LiveCaesareanRepository liveCaesareanRepository;
	
	@Autowired
	RecordingMotherBPRepository recordingMotherBPRepository;
	
	
	@Autowired
	PartographMonitoringViewRepository partographMonitoringViewRepository;
	
	@CrossOrigin
	@RequestMapping(value = "processindicator/l2", method = RequestMethod.POST, produces = { "application/json" })
	public Map<String, Object> discharge(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		org.json.JSONObject obj = new org.json.JSONObject(userJsonReq);
		String  block = obj.getString("block_code").toString();
		String district = obj.getString("district_code").toString();
		String state = obj.getString("state_code").toString();
		String facility = obj.getString("facility_code").toString();
		String facilitytype = obj.getString("facility_type_id").toString();
		String endDate = obj.getString("end_date").toString();
		String startDate = obj.getString("start_date").toString();
		JSONObject result = new JSONObject();
		JSONObject kpiResult1 = new JSONObject();
		 Map<String,Object> kpiResult = new HashMap<>();
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		   LocalDateTime now = LocalDateTime.now();
		 if(startDate.equalsIgnoreCase("ALL"))
		 {
			 startDate = "1995-01-01";
			 endDate = ""+dtf.format(now);
		 }
		 kpiResult.put(TOTALDELIVERY,  totalDelivery(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(CAESAREANDELIVERY,  totalCAESAREANDELIVERY(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(LIVEBIRTH,  totalLiveBirth(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(RECPRDINGMOTHERBP,  totalRecordingMotherBP(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(MotheTemperatureAtDischarge, 0);
		 kpiResult.put(TotalRecordingbabyTemp,  0);
		 kpiResult.put(AMSTL,  totalAMTSL(state,district,block,facilitytype,facility,startDate,endDate));
		 kpiResult.put(PARTOGRAPH,  Partograph(state,district,block,facilitytype,facility,startDate,endDate));
		 result.put("result", kpiResult);
		 result.put("total", 8);
		 result.put("message", "8 Records Found");
		 kpiResult1.put("data", result);
		 kpiResult1.put("success", true);
		 return kpiResult1.toMap();
					
	}
	
	   public String totalDelivery(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = totalDeliveryRepository.findByState(state,startDate,endDate);
			}
		    if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
			}
		     if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
			}
		     if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
		    		 && !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
			}
		     if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
		    		 && !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
			  
				 total = totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
		
		   }
		     if(total == null || total == 0)
			   total = 0L;
		   return total.toString();
		  
	   }
	
	   public String totalCAESAREANDELIVERY(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByState(state,startDate,endDate);
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByDistrict(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByBlock(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
			}
		   if(total == null || total ==0)
			   total = 0L;
		   
		   return total.toString();
	   }
	   
	   public String totalLiveBirth(String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		  
		 
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {

				 total = liveCaesareanRepository.findByState1(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByDistrict1(state,district,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByBlock1(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
			}
		    if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
			}
		   if(total == null || total == 0)
			   total = 0L;
		  
		   return total.toString();
	   }
	   
	   @SuppressWarnings("unused")
	public String totalRecordingMotherBP (String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   Long admission = 0L;
		 
		 
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			
				 total = recordingMotherBPRepository.findByState(state,startDate,endDate)*100;
			
				 admission = recordingMotherBPRepository.findByState1(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByDistrict(state,district,startDate,endDate)*100;
				 admission = recordingMotherBPRepository.findByDistrict1(state,district,startDate,endDate);
			
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByBlock(state,district,block,startDate,endDate)*100;
				 admission = recordingMotherBPRepository.findByBlock1(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate)*100;
				 admission = recordingMotherBPRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = recordingMotherBPRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate)*100;
				 admission = recordingMotherBPRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   Long result;
		  
		   if( total == 0 || total == null && admission == 0 || admission ==0) {
			 result = (long) 0;
		   }
		   else {
		    result = total/admission;	
		   }
		   return result.toString();
	   }
	   
	   
	   
	   
/*	   @SuppressWarnings("unused")
		public String totalRecordingMotherTemp (String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
		   {
			   Long total = 0L;
			   Long admission = 0L;
			   
			   
			   
			   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				  
					 total = motherTemperatureatDischargeViewRepository.findByState(state,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByState1(state,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = motherTemperatureatDischargeViewRepository.findByDistrict(state,district,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByDistrict1(state,district,startDate,endDate);
					
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = motherTemperatureatDischargeViewRepository.findByBlock(state,district,block,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByBlock1(state,district,block,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = motherTemperatureatDischargeViewRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
					 total = motherTemperatureatDischargeViewRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
			   }
			   Long result ;
			   if(total == 0 || total == null && admission == 0 || admission ==0) {
				   total = 0L;
			   		admission = 0L;
			   		result = (long) 0;
			   }
			   else {
			    result = total/admission;
			   }
			   return result.toString();
		   }
	   */
	   @SuppressWarnings("unused")
		public String Partograph  (String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
		   {
			   Long total = 0L;
			   Long admission = 0L;
			 
			  
			   
			   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				
					 total = partographMonitoringViewRepository.findByState(state,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByState1(state,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = partographMonitoringViewRepository.findByDistrict(state,district,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByDistrict1(state,district,startDate,endDate);
					
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = partographMonitoringViewRepository.findByBlock(state,district,block,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByBlock1(state,district,block,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
					 total = partographMonitoringViewRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
				}
			   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
						&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
					 total = partographMonitoringViewRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate)*100;
					 admission = recordingMotherBPRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
			   }
			   Long result;
			   if(total == 0 || total == null && admission == 0 || admission ==0) {
				   total = 0L;
			   		admission = 0L;
			   		result = (long) 0;
			   }
			   else {
			    result = total/admission;
			   }
			   return result.toString();
		   }
	   
	/*   @SuppressWarnings("unused")
	public String totalRecordingbabyTemp (String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   Long admission = 0L;
		  
		   
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			 
				 total = motherTemperatureatDischargeViewRepository.findByState1(state,startDate,endDate)*100;
				 admission = liveCaesareanRepository.findByState1(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = motherTemperatureatDischargeViewRepository.findByDistrict1(state,district,startDate,endDate)*100;
				 admission = liveCaesareanRepository.findByDistrict1(state,district,startDate,endDate);
				
			}
		   if   (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = motherTemperatureatDischargeViewRepository.findByBlock1(state,district,block,startDate,endDate)*100;
				 admission = liveCaesareanRepository.findByBlock1(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = motherTemperatureatDischargeViewRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate)*100;
				 admission = liveCaesareanRepository.findByFacilitytype1(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = motherTemperatureatDischargeViewRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate)*100;
				 admission = liveCaesareanRepository.findByFacility1(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   Long result;
		   if(total == 0 || total == null && admission == 0 || admission ==0) {
			   total = 0L;
		   		admission = 0L;
		   		result =(long) 0;
		   }
		   else {
		    result = total/admission;	
		   }
		   return result.toString();
	   }*/
		   
	   @SuppressWarnings("unused")
	public String totalAMTSL (String state,String district,String block,String facilitytype,String facility,String startDate,String endDate)
	   {
		   Long total = 0L;
		   Long admission = 0L;
		   if (!state.equalsIgnoreCase("ALL") && district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
			 
				 total = liveCaesareanRepository.findByState2(state,startDate,endDate)*100;
				 admission = totalDeliveryRepository.findByState(state,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByDistrict2(state,district,startDate,endDate)*100;
				 admission =  totalDeliveryRepository.findByDistrict(state,district,startDate,endDate);
				
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByBlock2(state,district,block,startDate,endDate)*100;
				 admission = totalDeliveryRepository.findByBlock(state,district,block,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && facility.equalsIgnoreCase("ALL")){
				 total = liveCaesareanRepository.findByFacilitytype2(state,district,block,facilitytype,startDate,endDate)*100;
				 admission = totalDeliveryRepository.findByFacilitytype(state,district,block,facilitytype,startDate,endDate);
			}
		   if (!state.equalsIgnoreCase("ALL") && !district.equalsIgnoreCase("ALL") && !block.equalsIgnoreCase("ALL")
					&& !facilitytype.equalsIgnoreCase("ALL") && !facility.equalsIgnoreCase("ALL")) {
				 total = liveCaesareanRepository.findByFacility2(state,district,block,facilitytype,facility,startDate,endDate)*100;
				 admission =totalDeliveryRepository.findByFacility(state,district,block,facilitytype,facility,startDate,endDate);
		   }
		   Long result;
		   if(total == 0 || total == null && admission == 0 || admission ==0) {
			   total = 0L;
		   		admission = 0L;
		   		result =(long) 0;
		   }
		   else {
		    result = total/admission;
		   }
		   return result.toString();
	   }
	
}
