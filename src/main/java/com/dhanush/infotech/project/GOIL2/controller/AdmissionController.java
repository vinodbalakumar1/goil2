package com.dhanush.infotech.project.GOIL2.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIL2.model.Admission;
import com.dhanush.infotech.project.GOIL2.model.ListChild;
import com.dhanush.infotech.project.GOIL2.model.LocationMasters;
import com.dhanush.infotech.project.GOIL2.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL2.repository.ListChildRepository;
import com.dhanush.infotech.project.GOIL2.repository.LocationMastersRepository;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class AdmissionController {

	/**
	 * Created by vinod on 23/07/18 13:23 - 22:10. // For insert admission data.
	
	 */

	@Autowired
	AdmissionRepository admissionRepository;

	@Autowired
	ListChildRepository listChildRepository;

	
	
	
	
	@Autowired
	LocationMastersRepository locationmastersRepository;

	public AdmissionController(ListChildRepository listChildRepository) {
		this.listChildRepository = listChildRepository;
	}

	@CrossOrigin
	@RequestMapping(value = "/admission/l2", method = RequestMethod.POST, produces = { "application/json" })
	@ApiOperation(value = "creating admission",
    notes = "Inserting Admission and Updating Admission.")
	@ApiParam(value = "name that need to be updated", required = true) 
	public HashMap<String, Object> createUser(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("admissionId");
		HashMap<String, Object> status = null;
		if(id.equalsIgnoreCase(""))
		{
			status = insertAdmission(userJsonReq);
		String statu = status.get("data") == "Beneficary Added successfull" ? "OK" :  "ERROR";
		userMap.put("status",statu);
		userMap.put("data", status.get("data"));
		userMap.put("uniqueId",status.get("unique_id"));
		userMap.put("admissionId", status.get("admissionId"));
		userMap.put("baby", status.get("baby"));
		userMap.put("error", status.get("error"));
		}
		else 
		{
			status = updateAdmission(userJsonReq, Long.valueOf(id));
			String statu = status.get("data") == "Beneficary Updated Successfull" ? "OK" :  "ERROR";
			userMap.put("status",statu);
			userMap.put("data", status.get("data"));
			userMap.put("uniqueId",status.get("unique_id"));
			userMap.put("admissionId", status.get("admissionId"));
			userMap.put("baby", status.get("baby"));
			userMap.put("error", status.get("error"));
		}
		return userMap;
	}

	public HashMap<String, Object> insertAdmission(String userJsonReq) throws JSONException {
		HashMap<String, Object> userMap = new HashMap<>();
	//	try {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			String state = openJson.getString("state");
			String district = openJson.getString("district");
			String block = openJson.getString("block");
			String facility = openJson.getString("facility");
			String facilityType = openJson.getString("facilityType");

			// Admission Object is Started

			org.json.JSONObject admission = openJson.getJSONObject("admission");
			String mctsNo = admission.getString("mctsNo");
			String booked = admission.getString("booked");
			String ipdRegisterdNumber = admission.getString("ipdRegisterdNumber");
			String bplJsyRegister = admission.getString("bplJsyRegister");
			String aadharCardNumber = admission.getString("aadharCardNumber");
			String refferedFrom = admission.getString("refferedFrom");
			String refferedReason = admission.getString("refferedReason");
			String contactNumber = admission.getString("contactNumber");
			String nameOfAsha = admission.getString("nameOfAsha");
			String patientName = admission.getString("patientName");
			String age = admission.getString("age");
			String woDo = admission.getString("woDo");
			String address = admission.getString("address");
			String patientContactNumber = admission.getString("patientContactNumber");
			String martialStatus = admission.getString("martialStatus");
			String admissionDate = admission.getString("admissionDate");
			String time = admission.getString("time");
			String admissionCategory = admission.getString("admissionCategory");
			String lmp = admission.getString("lmp");
			String edd = admission.getString("edd");
			String provisionalDiagnosis = admission.getString("provisionalDiagnosis");
			String finalDiagnosis = admission.getString("finalDiagnosis");
			String contraceptionHistory = admission.getString("contraceptionHistory");
			String patientOthers = admission.getString("patientOthers");
			String indicationForAssistedLSCSOthers = admission.getString("indicationForAssistedLSCSOthers");
			String finalOutCome = admission.getString("finalOutCome");
			String nameOfServiceProvider = admission.getString("nameOfServiceProvider");
			String designation = admission.getString("designation");
			String presentingComplaints = admission.getString("presentingComplaints");
			String pastObstetricsHistory = admission.getString("pastObstetricsHistory");
			String othersSpecify = admission.getString("othersSpecify");
			String medicalSurgicalHistory = admission.getString("medicalSurgicalHistory");
			String familyHoChronicIllness = admission.getString("familyHoChronicIllness");
			String dateOnSetOfLabour = admission.getString("dateOnSetOfLabour");
			String timeOfOnsetOfLabour = admission.getString("timeOfOnsetOfLabour");
			String gravida = admission.getString("gravida");
			String parity = admission.getString("parity");
			String abortionParity = admission.getString("abortionParity");
			String livingChildren = admission.getString("livingChildren");
			JSONArray arr = (JSONArray) admission.get("listChild");
			String uniqueNo = "" + System.currentTimeMillis();

			// GeneralExamination

			org.json.JSONObject generalExaminationObj = openJson.getJSONObject("generalExamination");

			String generalExamHeight = generalExaminationObj.getString("generalExamHeight");
			String generalExamWeight = generalExaminationObj.getString("generalExamWeight");
			Boolean generalExamPallor = generalExaminationObj.getBoolean("generalExamPallor");
			Boolean generalExamJaundice = generalExaminationObj.getBoolean("generalExamJaundice");
			Boolean generalExamPedalEdema = generalExaminationObj.getBoolean("generalExamPedalEdema");

			
			org.json.JSONObject others1 = openJson.getJSONObject("others");
			 String field1   =  others1.getString("field1");;
			 String field2   =  others1.getString("field2");
			 String field3   =  others1.getString("field3");
			 String field4   =  others1.getString("field4");
			 String field5   =  others1.getString("field5");
			 String field6   =  others1.getString("field6");
			 String field7   =  others1.getString("field7");
			 String field8   =  others1.getString("field8");
			 String field9   =  others1.getString("field9");
			 String field10  =  others1.getString("field10");
			
			// Vitals

			org.json.JSONObject vitals = openJson.getJSONObject("vitals");
			String vitalsBpSystolic = vitals.getString("vitalsBpSystolic");
			String vitalsBpDiastolic = vitals.getString("vitalsBpDiastolic");
			String vitalsTemparature = vitals.getString("vitalsTemparature");
			String vitalsPulse = vitals.getString("vitalsPulse");
			String vitalsRespiratoryRate = vitals.getString("vitalsRespiratoryRate");
			String vitalsFhr = vitals.getString("vitalsFhr");

			// PAExamination

			org.json.JSONObject pAExamination = openJson.getJSONObject("pAExamination");
			Boolean pAExampresentationCephalic = pAExamination.getBoolean("pAExampresentationCephalic");
			String pAExamOthers = pAExamination.getString("pAExamOthers");
			String pAExamEngagement = pAExamination.getString("pAExamEngagement");
			String pAExamLie = pAExamination.getString("pAExamLie");

			// gestationalAge

			org.json.JSONObject gestationalAge = openJson.getJSONObject("gestationalAge");
			Boolean gestationalAgepreTerm = gestationalAge.getBoolean("gestationalAgepreTerm");
			Boolean gestationalAgeantenatalCorticosteroid = gestationalAge
					.getBoolean("gestationalAgeantenatalCorticosteroid");
			String gestationalAgelmp = gestationalAge.getString("gestationalAgelmp");
			String gestationalAgeedd = gestationalAge.getString("gestationalAgeedd");
			String gestationalAgefundaMentalHeights = gestationalAge.getString("gestationalAgefundaMentalHeights");
			String gestationalAgefinalEstimatedAge = gestationalAge.getString("gestationalAgefinalEstimatedAge");
			String gestationalAgeageFromUsg = gestationalAge.getString("gestationalAgeageFromUsg");

			// PVExamination

			org.json.JSONObject setPvExamination = openJson.getJSONObject("pVExamination");
			String noOfPVExaminations = setPvExamination.getString("noOfPVExaminations");
			String cervicalDilation = setPvExamination.getString("cervicalDilation");
			String cervicalEffacement = setPvExamination.getString("cervicalEffacement");
			String membranes = setPvExamination.getString("membranes");
			String colourOfAmnioticFluid = setPvExamination.getString("colourOfAmnioticFluid");
			Boolean pelvisAdequate = setPvExamination.getBoolean("pelvisAdequate");

			// LabExamination

			org.json.JSONObject LabExamination = openJson.getJSONObject("labExamination");
			String bloodGroupRh = LabExamination.getString("bloodGroupRh");
			String labExamHb = LabExamination.getString("labExamHb");
			Boolean urineProtein = LabExamination.getBoolean("urineProtein");
			Boolean labExamHiv = LabExamination.getBoolean("labExamHiv");
			Boolean vDRLRPR = LabExamination.getBoolean("vDRLRPR");
			Boolean malaria = LabExamination.getBoolean("malaria");
			Boolean antiDGivenHb = LabExamination.getBoolean("antiDGivenHb");
			Boolean labExamBloodSugar = LabExamination.getBoolean("labExamBloodSugar");
			Boolean labExamUrineSugar = LabExamination.getBoolean("labExamUrineSugar");
			Boolean labExamHBsAg = LabExamination.getBoolean("labExamHBsAg");
			Boolean labExamSyphilis = LabExamination.getBoolean("labExamSyphilis");
			Boolean labExamGDM = LabExamination.getBoolean("labExamGDM");
			String others = LabExamination.getString("others");
			String providerPhoneNo = admission.getString("providerPhoneNo");
			String providerDate = admission.getString("providerDate");
			String providerTime = admission.getString("providerTime");
			
			
			
			
			String state_name="";
			String district_name="";
			String block_name="";
			String facility_name="";
			String facility_type_name="";
			
			try {
				
			   
			   
					List<LocationMasters> locations=null;
					   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
						for (LocationMasters locationMasters : locations)
						{
							 state_name=locationMasters.getState();
							 district_name=locationMasters.getDistrictCode();
							 block_name=locationMasters.getBlock();
							 facility_name=locationMasters.getFacility();
							 facility_type_name=locationMasters.getFacilityType();
							 
						}
						}catch (Exception e) {
							userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
							userMap.put("data", e.getMessage());
							userMap.put("admissionId","");
							userMap.put("unique_id", "");
							userMap.put("baby", "");
							return userMap;
						}
			
			
			
			
			

			// Admission Setters

			Admission setAdmission = new Admission();
			setAdmission.setUniqueNo(uniqueNo);
			setAdmission.setState(state);
			setAdmission.setDistrict(district);
			setAdmission.setBlock(block);
			setAdmission.setFacility(facility);
			setAdmission.setFacilityType(facilityType);
			setAdmission.setStatename(state_name);
			setAdmission.setDistrictname(district_name);
			setAdmission.setFacilityname(facility_name);
			setAdmission.setBlockname(block_name);
			setAdmission.setFacilitytypename(facility_type_name);
			setAdmission.setMctsNo(mctsNo);
			setAdmission.setBooked(booked);
			setAdmission.setIpdRegisterdNumber(ipdRegisterdNumber);
			setAdmission.setBplJsyRegister(bplJsyRegister);
			setAdmission.setAadharCardNumber(aadharCardNumber);
			setAdmission.setRefferedFrom(refferedFrom);
			setAdmission.setRefferedReason(refferedReason);
			setAdmission.setContactNumber(contactNumber);
			setAdmission.setNameOfAsha(nameOfAsha);
			setAdmission.setPatientName(patientName);
			setAdmission.setAge(age);
			setAdmission.setWoDo(woDo);
			setAdmission.setAddress(address);
			setAdmission.setPatientContactNumber(patientContactNumber);
			setAdmission.setMartialStatus(martialStatus);
			setAdmission.setAdmissionDate(admissionDate);
			setAdmission.setTime(time);
			setAdmission.setAdmissionCategory(admissionCategory);
			setAdmission.setLmp(lmp);
			setAdmission.setEdd(edd);
			setAdmission.setProvisionalDiagnosis(provisionalDiagnosis);
			setAdmission.setFinalDiagnosis(finalDiagnosis);
			setAdmission.setContraceptionHistory(contraceptionHistory);
			setAdmission.setOthers1(patientOthers);
			setAdmission.setProviderDate(providerDate);
			setAdmission.setProviderPhoneNo(providerPhoneNo);
			setAdmission.setProviderTime(providerTime);
			
			
			setAdmission.setField1(field1);
			setAdmission.setField2(field2);
			setAdmission.setField3(field3);
			setAdmission.setField4(field4);
			setAdmission.setField5(field5);
			setAdmission.setField6(field6);
			setAdmission.setField7(field7);
			setAdmission.setField8(field8);
			setAdmission.setField9(field9);
			setAdmission.setField10(field10);
			
			
			setAdmission.setIndicationForAssistedLSCSOthers(indicationForAssistedLSCSOthers);
			setAdmission.setFinalOutCome(finalOutCome);
			setAdmission.setNameOfServiceProvider(nameOfServiceProvider);
			setAdmission.setDesignation(designation);
			setAdmission.setPresentingComplaints(presentingComplaints);
			setAdmission.setPastObstetricsHistory(pastObstetricsHistory);
			setAdmission.setOthers2(othersSpecify);
			setAdmission.setMedicalSurgicalHistory(medicalSurgicalHistory);
			setAdmission.setFamilyHoChronicIllness(familyHoChronicIllness);
			setAdmission.setDateOnSetOfLabour(dateOnSetOfLabour);
			setAdmission.setTimeOfOnsetOfLabour(timeOfOnsetOfLabour);
			setAdmission.setGravida(gravida);
			setAdmission.setParity(parity);
			setAdmission.setAbortionParity(abortionParity);
			setAdmission.setLivingChildren(livingChildren);
			setAdmission.setStatus(108);
			// GeneralExamination setters


			setAdmission.setGeneralExamHeight(generalExamHeight);
			setAdmission.setGeneralExamJaundice(generalExamJaundice);
			setAdmission.setGeneralExamPallor(generalExamPallor);
			setAdmission.setGeneralExamWeight(generalExamWeight);
			setAdmission.setGeneralExamPedalEdema(generalExamPedalEdema);

			// Vitals setters

			setAdmission.setVitalsBpDiastolic(vitalsBpDiastolic);
			setAdmission.setVitalsBpSystolic(vitalsBpSystolic);
			setAdmission.setVitalsFhr(vitalsFhr);
			setAdmission.setVitalsPulse(vitalsPulse);
			setAdmission.setVitalsRespiratoryRate(vitalsRespiratoryRate);
			setAdmission.setVitalsTemparature(vitalsTemparature);

			// PAExamination setters

			setAdmission.setpAExamLie(pAExamLie);
			setAdmission.setpAExamEngagement(pAExamEngagement);
			setAdmission.setpAExampresentationCephalic(pAExampresentationCephalic);
			setAdmission.setpAExamOthers(pAExamOthers);

			// GestationalAge setters
			setAdmission.setGestationalAgeageFromUsg(gestationalAgeageFromUsg);
			setAdmission.setGestationalAgeantenatalCorticosteroid(gestationalAgeantenatalCorticosteroid);
			setAdmission.setGestationalAgeedd(gestationalAgeedd);
			setAdmission.setGestationalAgefinalEstimatedAge(gestationalAgefinalEstimatedAge);
			setAdmission.setGestationalAgefundaMentalHeights(gestationalAgefundaMentalHeights);
			setAdmission.setGestationalAgelmp(gestationalAgelmp);
			setAdmission.setGestationalAgepreTerm(gestationalAgepreTerm);

			// PVExamination setters

			setAdmission.setPvExamCervicalDilation(cervicalDilation);
			setAdmission.setPvExamCervicalEffacement(cervicalEffacement);
			setAdmission.setPvExamColourOfAmnioticFluid(colourOfAmnioticFluid);
			setAdmission.setPvExamMembranes(membranes);
			setAdmission.setPvExamNoOfPVExaminations(noOfPVExaminations);
			setAdmission.setPvExamPelvisAdequate(pelvisAdequate);

			// labExamination setters

			setAdmission.setLabExamAntiDGivenHb(antiDGivenHb);
			setAdmission.setLabExamBloodGroupRh(bloodGroupRh);
			setAdmission.setLabExamBloodSugar(labExamBloodSugar);
			setAdmission.setLabExamGDM(labExamGDM);
			setAdmission.setLabExamHb(labExamHb);
			setAdmission.setLabExamHiv(labExamHiv);
			setAdmission.setLabExamSyphilis(labExamSyphilis);
			setAdmission.setLabExamUrineSugar(labExamUrineSugar);
			setAdmission.setLabExamMalaria(malaria);
			setAdmission.setLabExamOthers(others);
			setAdmission.setLabExamVdrlrpr(vDRLRPR);
			setAdmission.setLabExamUrineProtein(urineProtein);
			setAdmission.setLabExamHBsAg(labExamHBsAg);

			// one to one relation
			
			setAdmission.setCompleteness("30%");
		

			// listchild looping
			ListChild lchild = null;
			List<ListChild> child = null;
			child = new ArrayList<ListChild>();
			String[] baby = new String[arr.length()];
			for (int i = 0; i < arr.length(); i++) {
				String deliveryOutCome1 = arr.getJSONObject(i).getString("deliveryOutCome1");
				String deliveryOutCome2 = arr.getJSONObject(i).getString("deliveryOutCome2");
				String abortion = arr.getJSONObject(i).getString("abortion");
				String sex = arr.getJSONObject(i).getString("sex");
				String preterm = arr.getJSONObject(i).getString("preterm");
				String birthWeight = arr.getJSONObject(i).getString("birthWeight");
				String deliveryDate = arr.getJSONObject(i).getString("deliveryDate");
				String deliveryTime = arr.getJSONObject(i).getString("deliveryTime");
				String immunization = arr.getJSONObject(i).getString("immunization");
				String modeOfDelivery = arr.getJSONObject(i).getString("modeOfDelivery");
				String deliveryOthers = arr.getJSONObject(i).getString("deliveryOthers");
				String vitaminKAdministered = arr.getJSONObject(i).getString("vitaminKAdministered");
				lchild = new ListChild();
				lchild.setDeliveryOutCome1(deliveryOutCome1);
				lchild.setDeliveryOutCome2(deliveryOutCome2);
				lchild.setAbortion(abortion);
				lchild.setSex(sex);
				lchild.setPreterm(preterm);
				lchild.setBirthWeight(birthWeight);
				lchild.setDeliveryDate(deliveryDate);
				lchild.setDeliveryTime(deliveryTime);
				lchild.setImmunization(immunization);
				lchild.setModeOfDelivery(modeOfDelivery);
				lchild.setDeliveryOthers(deliveryOthers);
				lchild.setUniqueNo(uniqueNo);
				lchild.setVitaminKAdministered(vitaminKAdministered);
				child.add(lchild);
			//	listChildRepository.save(lchild);
				
			}
			setAdmission.setListChild(child);
			admissionRepository.save(setAdmission); // saving admission data in admission table and generate uniqu id
			// inserting data
			
			List<ListChild> adm =  listChildRepository.findByAdmissionId(setAdmission.getId());
			for(int i =0; i< adm.size();i++)			{
				baby[i] = ""+adm.get(i).getId();
			}

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Beneficary Added successfull");
			userMap.put("admissionId",setAdmission.getId().toString());
			userMap.put("unique_id",uniqueNo);
			userMap.put("baby",baby);
	
			
/*		} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Add Beneficary failure");
			userMap.put("unique_id", "failed");
			userMap.put("admissionId","");
			userMap.put("baby","fail");
				userMap.put("error",e.getMessage());
		}*/
		return userMap;
	}
	
	
	public  HashMap<String, Object> updateAdmission(String userJsonReq,long id) throws Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		   
	//	try {
			org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
			String state = openJson.getString("state");
			String district = openJson.getString("district");
			String block = openJson.getString("block");
			String facility = openJson.getString("facility");
			String facilityType = openJson.getString("facilityType");
			String uniqueNo = openJson.getString("unique_id");
			// Admission Object is Started

			org.json.JSONObject admission = openJson.getJSONObject("admission");
			String mctsNo = admission.getString("mctsNo");
			String booked = admission.getString("booked");
			String ipdRegisterdNumber = admission.getString("ipdRegisterdNumber");
			String bplJsyRegister = admission.getString("bplJsyRegister");
			String aadharCardNumber = admission.getString("aadharCardNumber");
			String refferedFrom = admission.getString("refferedFrom");
			String refferedReason = admission.getString("refferedReason");
			String contactNumber = admission.getString("contactNumber");
			String nameOfAsha = admission.getString("nameOfAsha");
			String patientName = admission.getString("patientName");
			String age = admission.getString("age");
			String woDo = admission.getString("woDo");
			String address = admission.getString("address");
			String patientContactNumber = admission.getString("patientContactNumber");
			String martialStatus = admission.getString("martialStatus");
			String admissionDate = admission.getString("admissionDate");
			String time = admission.getString("time");
			String admissionCategory = admission.getString("admissionCategory");
			String lmp = admission.getString("lmp");
			String edd = admission.getString("edd");
			String provisionalDiagnosis = admission.getString("provisionalDiagnosis");
			String finalDiagnosis = admission.getString("finalDiagnosis");
			String contraceptionHistory = admission.getString("contraceptionHistory");
			String patientOthers = admission.getString("patientOthers");
			String indicationForAssistedLSCSOthers = admission.getString("indicationForAssistedLSCSOthers");
			String finalOutCome = admission.getString("finalOutCome");
			String nameOfServiceProvider = admission.getString("nameOfServiceProvider");
			String designation = admission.getString("designation");
			String presentingComplaints = admission.getString("presentingComplaints");
			String pastObstetricsHistory = admission.getString("pastObstetricsHistory");
			String othersSpecify = admission.getString("othersSpecify");
			String medicalSurgicalHistory = admission.getString("medicalSurgicalHistory");
			String familyHoChronicIllness = admission.getString("familyHoChronicIllness");
			String dateOnSetOfLabour = admission.getString("dateOnSetOfLabour");
			String timeOfOnsetOfLabour = admission.getString("timeOfOnsetOfLabour");
			String gravida = admission.getString("gravida");
			String parity = admission.getString("parity");
			String abortionParity = admission.getString("abortionParity");
			String livingChildren = admission.getString("livingChildren");
			String providerPhoneNo = admission.getString("providerPhoneNo");
			String providerDate = admission.getString("providerDate");
			String providerTime = admission.getString("providerTime");
			JSONArray arr = (JSONArray) admission.get("listChild");
			
			

			// GeneralExamination

			org.json.JSONObject generalExaminationObj = openJson.getJSONObject("generalExamination");

			String generalExamHeight = generalExaminationObj.getString("generalExamHeight");
			String generalExamWeight = generalExaminationObj.getString("generalExamWeight");
			Boolean generalExamPallor = generalExaminationObj.getBoolean("generalExamPallor");
			Boolean generalExamJaundice = generalExaminationObj.getBoolean("generalExamJaundice");
			Boolean generalExamPedalEdema = generalExaminationObj.getBoolean("generalExamPedalEdema");

			// Vitals
			
			org.json.JSONObject others1 = openJson.getJSONObject("others");
			 String field1   =  others1.getString("field1");;
			 String field2   =  others1.getString("field2");
			 String field3   =  others1.getString("field3");
			 String field4   =  others1.getString("field4");
			 String field5   =  others1.getString("field5");
			 String field6   =  others1.getString("field6");
			 String field7   =  others1.getString("field7");
			 String field8   =  others1.getString("field8");
			 String field9   =  others1.getString("field9");
			 String field10  =  others1.getString("field10");

			org.json.JSONObject vitals = openJson.getJSONObject("vitals");
			String vitalsBpDiastolic = vitals.getString("vitalsBpDiastolic");
			String vitalsBpSystolic = vitals.getString("vitalsBpSystolic");
			String vitalsTemparature = vitals.getString("vitalsTemparature");
			String vitalsPulse = vitals.getString("vitalsPulse");
			String vitalsRespiratoryRate = vitals.getString("vitalsRespiratoryRate");
			String vitalsFhr = vitals.getString("vitalsFhr");

			// PAExamination

			org.json.JSONObject pAExamination = openJson.getJSONObject("pAExamination");
			Boolean pAExampresentationCephalic = pAExamination.getBoolean("pAExampresentationCephalic");
			String pAExamOthers = pAExamination.getString("pAExamOthers");
			String pAExamEngagement = pAExamination.getString("pAExamEngagement");
			String pAExamLie = pAExamination.getString("pAExamLie");

			// gestationalAge
		
			

			org.json.JSONObject gestationalAge = openJson.getJSONObject("gestationalAge");
			Boolean gestationalAgepreTerm = gestationalAge.getBoolean("gestationalAgepreTerm");
			Boolean gestationalAgeantenatalCorticosteroid = gestationalAge
					.getBoolean("gestationalAgeantenatalCorticosteroid");
			String gestationalAgelmp = gestationalAge.getString("gestationalAgelmp");
			String gestationalAgeedd = gestationalAge.getString("gestationalAgeedd");
			String gestationalAgefundaMentalHeights = gestationalAge.getString("gestationalAgefundaMentalHeights");
			String gestationalAgefinalEstimatedAge = gestationalAge.getString("gestationalAgefinalEstimatedAge");
			String gestationalAgeageFromUsg = gestationalAge.getString("gestationalAgeageFromUsg");

			// PVExamination

			org.json.JSONObject setPvExamination = openJson.getJSONObject("pVExamination");
			String noOfPVExaminations = setPvExamination.getString("noOfPVExaminations");
			String cervicalDilation = setPvExamination.getString("cervicalDilation");
			String cervicalEffacement = setPvExamination.getString("cervicalEffacement");
			String membranes = setPvExamination.getString("membranes");
			String colourOfAmnioticFluid = setPvExamination.getString("colourOfAmnioticFluid");
			Boolean pelvisAdequate = setPvExamination.getBoolean("pelvisAdequate");

			
			// LabExamination

			org.json.JSONObject LabExamination = openJson.getJSONObject("labExamination");
			String bloodGroupRh = LabExamination.getString("bloodGroupRh");
			String labExamHb = LabExamination.getString("labExamHb");
			Boolean urineProtein = LabExamination.getBoolean("urineProtein");
			Boolean labExamHiv = LabExamination.getBoolean("labExamHiv");
			Boolean vDRLRPR = LabExamination.getBoolean("vDRLRPR");
			Boolean malaria = LabExamination.getBoolean("malaria");
			Boolean antiDGivenHb = LabExamination.getBoolean("antiDGivenHb");
			Boolean labExamBloodSugar = LabExamination.getBoolean("labExamBloodSugar");
			Boolean labExamUrineSugar = LabExamination.getBoolean("labExamUrineSugar");
			Boolean labExamHBsAg = LabExamination.getBoolean("labExamHBsAg");
			Boolean labExamSyphilis = LabExamination.getBoolean("labExamSyphilis");
			Boolean labExamGDM = LabExamination.getBoolean("labExamGDM");
			String others = LabExamination.getString("others");
			
			
			
			@SuppressWarnings("unused")
			String state_name="";
			@SuppressWarnings("unused")
			String district_name="";
			@SuppressWarnings("unused")
			String block_name="";
			@SuppressWarnings("unused")
			String facility_name="";
			@SuppressWarnings("unused")
			String facility_type_name="";
			
			try {
				
			   
			   
					List<LocationMasters> locations=null;
					   locations=locationmastersRepository.findByFacility(Integer.parseInt(state),Integer.parseInt(district),Integer.parseInt(block),Integer.parseInt(facility),facilityType);
						for (Iterator<LocationMasters> iterator = locations.iterator(); iterator.hasNext();)
						{
							LocationMasters locationMasters = (LocationMasters) iterator.next();
							 state_name=locationMasters.getState();
							 district_name=locationMasters.getDistrictCode();
							 block_name=locationMasters.getBlock();
							 facility_name=locationMasters.getFacility();
							 facility_type_name=locationMasters.getFacilityType();
							 
							
							
						}
						}catch (Exception e) {
							userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
							userMap.put("data", e.getMessage());
							userMap.put("admissionId","");
							userMap.put("unique_id", "");
							userMap.put("baby", "");
							return userMap;
						}
			
			

			// Admission Setters
			
			Admission setAdmission =  admissionRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", id));
			setAdmission.setUniqueNo(uniqueNo);
			setAdmission.setState(state);
			setAdmission.setDistrict(district);
			setAdmission.setBlock(block);
			setAdmission.setFacility(facility);
			setAdmission.setFacilityType(facilityType);
			setAdmission.setMctsNo(mctsNo);
			setAdmission.setBooked(booked);
			setAdmission.setStatename(state_name);
			setAdmission.setDistrictname(district_name);
			setAdmission.setFacilityname(facility_name);
			setAdmission.setBlockname(block_name);
			setAdmission.setFacilitytypename(facility_type_name);
			
			setAdmission.setIpdRegisterdNumber(ipdRegisterdNumber);
			setAdmission.setBplJsyRegister(bplJsyRegister);
			setAdmission.setAadharCardNumber(aadharCardNumber);
			setAdmission.setRefferedFrom(refferedFrom);
			setAdmission.setRefferedReason(refferedReason);
			setAdmission.setContactNumber(contactNumber);
			setAdmission.setNameOfAsha(nameOfAsha);
			setAdmission.setPatientName(patientName);
			setAdmission.setAge(age);
			setAdmission.setWoDo(woDo);
			setAdmission.setAddress(address);
			setAdmission.setPatientContactNumber(patientContactNumber);
			setAdmission.setMartialStatus(martialStatus);
			setAdmission.setAdmissionDate(admissionDate);
			setAdmission.setTime(time);
			setAdmission.setAdmissionCategory(admissionCategory);
			setAdmission.setLmp(lmp);
			setAdmission.setEdd(edd);
			setAdmission.setProvisionalDiagnosis(provisionalDiagnosis);
			setAdmission.setFinalDiagnosis(finalDiagnosis);
			setAdmission.setContraceptionHistory(contraceptionHistory);
			setAdmission.setOthers1(patientOthers);
			setAdmission.setIndicationForAssistedLSCSOthers(indicationForAssistedLSCSOthers);
			setAdmission.setFinalOutCome(finalOutCome);
			setAdmission.setNameOfServiceProvider(nameOfServiceProvider);
			setAdmission.setDesignation(designation);
			setAdmission.setPresentingComplaints(presentingComplaints);
			setAdmission.setPastObstetricsHistory(pastObstetricsHistory);
			setAdmission.setOthers2(othersSpecify);
			setAdmission.setMedicalSurgicalHistory(medicalSurgicalHistory);
			setAdmission.setFamilyHoChronicIllness(familyHoChronicIllness);
			setAdmission.setDateOnSetOfLabour(dateOnSetOfLabour);
			setAdmission.setTimeOfOnsetOfLabour(timeOfOnsetOfLabour);
			setAdmission.setGravida(gravida);
			setAdmission.setParity(parity);
			setAdmission.setAbortionParity(abortionParity);
			setAdmission.setLivingChildren(livingChildren);
			setAdmission.setStatus(108);
			setAdmission.setProviderDate(providerDate);
			setAdmission.setProviderPhoneNo(providerPhoneNo);
			setAdmission.setProviderTime(providerTime);

			// GeneralExamination setters

			setAdmission.setField1(field1);
			setAdmission.setField2(field2);
			setAdmission.setField3(field3);
			setAdmission.setField4(field4);
			setAdmission.setField5(field5);
			setAdmission.setField6(field6);
			setAdmission.setField7(field7);
			setAdmission.setField8(field8);
			setAdmission.setField9(field9);
			setAdmission.setField10(field10);
			

			setAdmission.setGeneralExamHeight(generalExamHeight);
			setAdmission.setGeneralExamJaundice(generalExamJaundice);
			setAdmission.setGeneralExamPallor(generalExamPallor);
			setAdmission.setGeneralExamWeight(generalExamWeight);
			setAdmission.setGeneralExamPedalEdema(generalExamPedalEdema);

			// Vitals setters

		
			setAdmission.setVitalsBpDiastolic(vitalsBpDiastolic);
			setAdmission.setVitalsBpSystolic(vitalsBpSystolic);
			setAdmission.setVitalsFhr(vitalsFhr);
			setAdmission.setVitalsPulse(vitalsPulse);
			setAdmission.setVitalsRespiratoryRate(vitalsRespiratoryRate);
			setAdmission.setVitalsTemparature(vitalsTemparature);

			// PAExamination setters
			

			setAdmission.setpAExamLie(pAExamLie);
			setAdmission.setpAExamEngagement(pAExamEngagement);
			setAdmission.setpAExampresentationCephalic(pAExampresentationCephalic);
			setAdmission.setpAExamOthers(pAExamOthers);

			// GestationalAge setters

			
			setAdmission.setGestationalAgeageFromUsg(gestationalAgeageFromUsg);
			setAdmission.setGestationalAgeantenatalCorticosteroid(gestationalAgeantenatalCorticosteroid);
			setAdmission.setGestationalAgeedd(gestationalAgeedd);
			setAdmission.setGestationalAgefinalEstimatedAge(gestationalAgefinalEstimatedAge);
			setAdmission.setGestationalAgefundaMentalHeights(gestationalAgefundaMentalHeights);
			setAdmission.setGestationalAgelmp(gestationalAgelmp);
			setAdmission.setGestationalAgepreTerm(gestationalAgepreTerm);

			// PVExamination setters

			
			setAdmission.setPvExamCervicalDilation(cervicalDilation);
			setAdmission.setPvExamCervicalEffacement(cervicalEffacement);
			setAdmission.setPvExamColourOfAmnioticFluid(colourOfAmnioticFluid);
			setAdmission.setPvExamMembranes(membranes);
			setAdmission.setPvExamNoOfPVExaminations(noOfPVExaminations);
			setAdmission.setPvExamPelvisAdequate(pelvisAdequate);

			// labExamination setters

		
			setAdmission.setLabExamAntiDGivenHb(antiDGivenHb);
			setAdmission.setLabExamBloodGroupRh(bloodGroupRh);
			setAdmission.setLabExamBloodSugar(labExamBloodSugar);
			setAdmission.setLabExamGDM(labExamGDM);
			setAdmission.setLabExamHb(labExamHb);
			setAdmission.setLabExamHiv(labExamHiv);
			setAdmission.setLabExamSyphilis(labExamSyphilis);
			setAdmission.setLabExamUrineSugar(labExamUrineSugar);
			setAdmission.setLabExamMalaria(malaria);
			setAdmission.setLabExamOthers(others);
			setAdmission.setLabExamVdrlrpr(vDRLRPR);
			setAdmission.setLabExamUrineProtein(urineProtein);
			setAdmission.setLabExamHBsAg(labExamHBsAg);

			/// one to one relation
			ListChild lchild = null;
			List<ListChild> child = null;
			child = new ArrayList<ListChild>();
			String[] baby = new String[arr.length()];
			for (int i = 0; i < arr.length(); i++) {
				String id2 = arr.getJSONObject(i).getString("child_id").equals("") ? "0" : arr.getJSONObject(i).getString("child_id");
				String deliveryOutCome1 = arr.getJSONObject(i).getString("deliveryOutCome1");
				String deliveryOutCome2 = arr.getJSONObject(i).getString("deliveryOutCome2");
				String abortion = arr.getJSONObject(i).getString("abortion");
				String sex = arr.getJSONObject(i).getString("sex");
				String preterm = arr.getJSONObject(i).getString("preterm");
				String birthWeight = arr.getJSONObject(i).getString("birthWeight");
				String deliveryDate = arr.getJSONObject(i).getString("deliveryDate");
				String deliveryTime = arr.getJSONObject(i).getString("deliveryTime");
				String immunization = arr.getJSONObject(i).getString("immunization");
				String modeOfDelivery = arr.getJSONObject(i).getString("modeOfDelivery");
				String deliveryOthers = arr.getJSONObject(i).getString("deliveryOthers");
				String vitaminKAdministered = arr.getJSONObject(i).getString("vitaminKAdministered");
				lchild = new ListChild();
				if(!id2.equalsIgnoreCase("0"))
					lchild.setId(Long.valueOf(id2));
				lchild.setDeliveryOutCome1(deliveryOutCome1);
				lchild.setDeliveryOutCome2(deliveryOutCome2);
				lchild.setAbortion(abortion);
				lchild.setSex(sex);
				lchild.setPreterm(preterm);
				lchild.setBirthWeight(birthWeight);
				lchild.setDeliveryDate(deliveryDate);
				lchild.setDeliveryTime(deliveryTime);
				lchild.setImmunization(immunization);
				lchild.setModeOfDelivery(modeOfDelivery);
				lchild.setDeliveryOthers(deliveryOthers);
				lchild.setUniqueNo(uniqueNo);
				lchild.setVitaminKAdministered(vitaminKAdministered);
			//	listChildRepository.save(lchild);
			//	baby[i]= Long.toString(lchild.getId());
				child.add(lchild);
				
			}
			setAdmission.setListChild(child);
			
			admissionRepository.save(setAdmission); // saving admission data in admission table and generate uniqu id

			List<ListChild> adm =  listChildRepository.findByAdmissionId(setAdmission.getId());
			for(int i =0; i <= adm.size()-1;i++)			{
				baby[i] = ""+adm.get(i).getId();
			}
			// list child 

			/// one to one relation

			// inserting data
			

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Beneficary Updated Successfull");
			userMap.put("admissionId",setAdmission.getId().toString());
			userMap.put("unique_id", setAdmission.getUniqueNo());
			userMap.put("baby", baby);
	/*	}
		catch(Exception e)
		{
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Add Beneficary failure");
			userMap.put("unique_id", "failed");
			userMap.put("baby","fail");
			userMap.put("error",e.getMessage());
		}*/
		return userMap;
	}

}
