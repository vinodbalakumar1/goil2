package com.dhanush.infotech.project.GOIL2.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIL2.model.Admission;
import com.dhanush.infotech.project.GOIL2.model.DischargeDetails;
import com.dhanush.infotech.project.GOIL2.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL2.repository.DischargeDetailsRepository;

@RestController
@RequestMapping("/api")
public class GetDischargeDetailsController {

	@Autowired
	AdmissionRepository admissionRepository;
	
	@Autowired
	DischargeDetailsRepository dischargeDetailsRepository;
	
	@CrossOrigin
	@RequestMapping(value = "/getdischargedetailsbyid/l2/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public String getUserById(@PathVariable(value = "id") Long UserId) {
		JSONObject normal = new JSONObject();
		JSONObject obj = new JSONObject();
		JSONObject obj1 = new JSONObject();
		JSONObject obj2 = new JSONObject();
		try {
			Admission admission = admissionRepository.findById(UserId)
					.orElseThrow(() -> new ResourceNotFoundException("User", "id", UserId));
		List<DischargeDetails> obstetric1 = dischargeDetailsRepository.findByCaseId(admission.getId());
		if(obstetric1.size() == 0) {
			obj.put("status","Record Not Found");
			return obj.toString();
		}
		else
		obj.put("id", admission.getDischargeDetails().getId());
		obj1.put("finalOutcome", admission.getDischargeDetails().getFinalOutcome());
		obj1.put("conditionOfMother", admission.getDischargeDetails().getConditionOfMother());
		obj1.put("conditionOfBaby", admission.getDischargeDetails().getConditionOfBaby());
		obj1.put("counsellingOnDangerSignsDone", admission.getDischargeDetails().getCounsellingOnDangerSignsDone());
		obj1.put("familyPlanningMethodAdopted", admission.getDischargeDetails().getFamilyPlanningMethodAdopted());
		obj1.put("dischargeDetailsOthers", admission.getDischargeDetails().getDischargeDetailsOthers());
		obj1.put("dischargeDetailsOtherNotes", admission.getDischargeDetails().getDischargeDetailsOtherNotes());
		obj2.put("date_of_Discharge_Referral", admission.getDischargeDetails().getDate_of_Discharge_Referral());
		obj2.put("time_of_Discharge_Referral", admission.getDischargeDetails().getTime_of_Discharge_Referral());
		obj2.put("advice_on_discharge", admission.getDischargeDetails().getAdvice_on_discharge());
		obj2.put("facility_name", admission.getDischargeDetails().getFacility_name());
		obj2.put("other_notes", admission.getDischargeDetails().getOther_notes());
		obj2.put("treatment_given", admission.getDischargeDetails().getTreatment_given());
		obj2.put("reason_for_referral", admission.getDischargeDetails().getReason_for_referral());
		obj2.put("treatment_given1", admission.getDischargeDetails().getTreatmentGiven1());
		obj2.put("bPOfMother", admission.getDischargeDetails().getbPOfMother());
		obj2.put("bPOfMother1", admission.getDischargeDetails().getbPOfMother1());
		JSONObject others = new JSONObject();
		others.put("field1", ""+admission.getField1());
		others.put("field2", ""+admission.getField2());
		others.put("field3", ""+admission.getField3());
		others.put("field4", ""+admission.getField4());
		others.put("field5", ""+admission.getField5());
		others.put("field6", ""+admission.getField6());
		others.put("field7", ""+admission.getField7());
		others.put("field8", ""+admission.getField8());
		others.put("field9",""+ admission.getField9());
		others.put("field10", ""+admission.getField10());
		obj.put("others", others);
		obj.put("dischargeNotes", obj1);
		obj.put("discharge_Referral_LAMA_Death", obj2);
		obj.put("admissionId", admission.getDischargeDetails().getAdmission().getId());
		normal.put("status", "ok");
		normal.put("data", obj);
		}
		catch(Exception e) {
			normal.put("status", "No Record Found");
			normal.put("data", obj);
		}
		return normal.toString();
	}

}
