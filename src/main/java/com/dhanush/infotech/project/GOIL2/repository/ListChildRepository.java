package com.dhanush.infotech.project.GOIL2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.ListChild;

/**
 * Created by vinod on 11/06/18.
 */
@Repository
public interface ListChildRepository extends JpaRepository<ListChild, Long> {

	@Query(value = "FROM ListChild  where admissionid = :uniqueId")
	List<ListChild> findByAdmissionId(@Param("uniqueId") Long uniqueId);
}
