package com.dhanush.infotech.project.GOIL2.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dhanush.infotech.project.GOIL2.exception.ResourceNotFoundException;
import com.dhanush.infotech.project.GOIL2.model.Admission;
import com.dhanush.infotech.project.GOIL2.model.DischargeDetails;
import com.dhanush.infotech.project.GOIL2.repository.AdmissionRepository;
import com.dhanush.infotech.project.GOIL2.repository.DischargeDetailsRepository;

@RestController
@RequestMapping("/api")
public class DischargeController {

	/**
	 * Created by vinod on 13/06/18 12:23 // For insert discharge data.
	 * 
	 */

	@Autowired
	private DischargeDetailsRepository dischargeDetailsRepository;

	public DischargeDetailsRepository getDischargeDetailsRepository() {
		return dischargeDetailsRepository;
	}

	public void setDischargeDetailsRepository(DischargeDetailsRepository dischargeDetailsRepository) {
		this.dischargeDetailsRepository = dischargeDetailsRepository;
	}

	@Autowired
	AdmissionRepository admissionRepository;

	@CrossOrigin
	@RequestMapping(value = "/discharge/l2", method = RequestMethod.POST, produces = { "application/json" })
	public HashMap<String, Object> discharge(@RequestBody String userJsonReq) throws NumberFormatException, Exception {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson = new org.json.JSONObject(userJsonReq);
		String id = openJson.getString("id");
		HashMap<String, Object> status = null;
		if (id.equalsIgnoreCase("")) {
			status = insertDischarge(userJsonReq);
			String statu = status.get("data").equals("Discharge Added successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
		} else {
			status = updateDischarge(userJsonReq, id);
			String statu = status.get("data").equals("Discharge Updated successfull") ? "OK"
					: "ERROR";
			userMap.put("status", statu);
			userMap.put("data", status.get("data"));
			userMap.put("id", status.get("id"));
		}

		return userMap;
	}

	public HashMap<String, Object> insertDischarge(String userJsonReq) {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson1 = new org.json.JSONObject(userJsonReq);
		
		try {
				// getting values from request
		
		
		String admissionId = openJson1.getString("admissionId");
		org.json.JSONObject openJson = openJson1.getJSONObject("dischargeNotes");
			String finalOutcome = openJson.getString("finalOutcome");
			String conditionOfMother = openJson.getString("conditionOfMother");
			String conditionOfBaby = openJson.getString("conditionOfBaby");
			String counsellingOnDangerSignsDone = openJson.getString("counsellingOnDangerSignsDone");
			String familyPlanningMethodAdopted = openJson.getString("familyPlanningMethodAdopted");
			String dischargeDetailsOthers = openJson.getString("dischargeDetailsOthers");
			String dischargeDetailsOtherNotes = openJson.getString("dischargeDetailsOtherNotes");
			String bPOfMother = openJson.optString("bPOfMother");
			String bPOfMother1 = openJson.optString("bPOfMother1");
			org.json.JSONObject openJson2 = openJson1.getJSONObject("discharge_Referral_LAMA_Death");
			String date_of_Discharge_Referral = openJson2.getString("date_of_Discharge_Referral");
			String time_of_Discharge_Referral = openJson2.getString("time_of_Discharge_Referral");
			String advice_on_discharge        = openJson2.getString("advice_on_discharge");
			String  facility_name             = openJson2.getString("facility_name");
			String  other_notes               = openJson2.getString("other_notes");
			String  treatment_given           = openJson2.getString("treatment_given");
			String  reason_for_referral           = openJson2.getString("reason_for_referral");
			String  treatment_given1           = openJson2.getString("treatment_given1");
			org.json.JSONObject others2 = openJson1.getJSONObject("others");
			 String field1   =  others2.getString("field1");
			 String field2   =  others2.getString("field2");
			 String field3   =  others2.getString("field3");
			 String field4   =  others2.getString("field4");
			 String field5   =  others2.getString("field5");
			 String field6   =  others2.getString("field6");
			 String field7   =  others2.getString("field7");
			 String field8   =  others2.getString("field8");
			 String field9   =  others2.getString("field9");
			 String field10  =  others2.getString("field10");
			
			// setters
			Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
					.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
			
			DischargeDetails dischargeDetails = new DischargeDetails();
			dischargeDetails.setFinalOutcome(finalOutcome);
			dischargeDetails.setUniqueId(setAdmission1.getUniqueNo());
			dischargeDetails.setConditionOfMother(conditionOfMother);
			dischargeDetails.setConditionOfBaby(conditionOfBaby);
			dischargeDetails.setCounsellingOnDangerSignsDone(counsellingOnDangerSignsDone);
			dischargeDetails.setFamilyPlanningMethodAdopted(familyPlanningMethodAdopted);
			dischargeDetails.setDischargeDetailsOthers(dischargeDetailsOthers);
			dischargeDetails.setDischargeDetailsOtherNotes(dischargeDetailsOtherNotes);
			dischargeDetails.setDate_of_Discharge_Referral(date_of_Discharge_Referral);
			dischargeDetails.setTime_of_Discharge_Referral(time_of_Discharge_Referral);
			dischargeDetails.setAdvice_on_discharge(advice_on_discharge);
			dischargeDetails.setFacility_name(facility_name);
			dischargeDetails.setOther_notes(other_notes);
			dischargeDetails.setReason_for_referral(reason_for_referral);
			dischargeDetails.setTreatmentGiven1(treatment_given1);
			dischargeDetails.setTreatment_given(treatment_given);
			dischargeDetails.setbPOfMother(bPOfMother);
			dischargeDetails.setbPOfMother1(bPOfMother1);
			dischargeDetails.setField1(field1);
			dischargeDetails.setField2(field2);
			dischargeDetails.setField3(field3);
			dischargeDetails.setField4(field4);
			dischargeDetails.setField5(field5);
			dischargeDetails.setField6(field6);
			dischargeDetails.setField7(field7);
			dischargeDetails.setField8(field8);
			dischargeDetails.setField9(field9);
			dischargeDetails.setField10(field10);
			dischargeDetails.setAdmission(setAdmission1);
			setAdmission1.setId(Long.valueOf(admissionId));
			setAdmission1.setCompleteness("100%");
			List<DischargeDetails> admission = dischargeDetailsRepository.findByCaseId(setAdmission1.getId());
			if(admission.size() == 0) {
				dischargeDetailsRepository.save(dischargeDetails);
				userMap.put("status", HttpStatus.OK);
				userMap.put("data", "Discharge Added successfull");
				userMap.put("id", dischargeDetails.getId());
				return userMap;
			}
			
			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Discharge Already Exist");
			userMap.put("id", dischargeDetails.getId());
		
			} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Discharge failure");
			userMap.put("id", "failed");
		}
		return userMap;
	}

	public HashMap<String, Object> updateDischarge(String userJsonReq, String id) {
		HashMap<String, Object> userMap = new HashMap<>();
		org.json.JSONObject openJson1 = new org.json.JSONObject(userJsonReq);
		try {
			String admissionId = openJson1.getString("admissionId");                                           
			org.json.JSONObject openJson = openJson1.getJSONObject("dischargeNotes");                          
				String finalOutcome = openJson.getString("finalOutcome");                                      
				String conditionOfMother = openJson.getString("conditionOfMother");                            
				String conditionOfBaby = openJson.getString("conditionOfBaby");                                
				String counsellingOnDangerSignsDone = openJson.getString("counsellingOnDangerSignsDone");      
				String familyPlanningMethodAdopted = openJson.getString("familyPlanningMethodAdopted");        
				String dischargeDetailsOthers = openJson.getString("dischargeDetailsOthers");                  
				String dischargeDetailsOtherNotes = openJson.getString("dischargeDetailsOtherNotes");   
				String bPOfMother = openJson.optString("bPOfMother");
				String bPOfMother1 = openJson.optString("bPOfMother1");
            	org.json.JSONObject openJson2 = openJson1.getJSONObject("discharge_Referral_LAMA_Death");      
				String date_of_Discharge_Referral = openJson2.getString("date_of_Discharge_Referral");         
				String time_of_Discharge_Referral = openJson2.getString("time_of_Discharge_Referral");         
				String advice_on_discharge        = openJson2.getString("advice_on_discharge");                
				String  facility_name             = openJson2.getString("facility_name");                      
				String  other_notes               = openJson2.getString("other_notes");                        
				String  treatment_given           = openJson2.getString("treatment_given");                    
				String  reason_for_referral           = openJson2.getString("reason_for_referral");
				String  treatment_given1           = openJson2.getString("treatment_given1");
				org.json.JSONObject others2 = openJson1.getJSONObject("others");       
				 String field1   =  others2.getString("field1");                       
				 String field2   =  others2.getString("field2");                       
				 String field3   =  others2.getString("field3");                       
				 String field4   =  others2.getString("field4");                       
				 String field5   =  others2.getString("field5");                       
				 String field6   =  others2.getString("field6");                       
				 String field7   =  others2.getString("field7");                       
				 String field8   =  others2.getString("field8");                       
				 String field9   =  others2.getString("field9");                       
				 String field10  =  others2.getString("field10");                      
			// setters
			
			Admission setAdmission1 = admissionRepository.findById(Long.valueOf(admissionId))
					.orElseThrow(() -> new ResourceNotFoundException("Admission", "NOT FOUND", admissionId));
			DischargeDetails dischargeDetails = dischargeDetailsRepository.findById(Long.valueOf(setAdmission1.getDischargeDetails().getId()))
					.orElseThrow(() -> new ResourceNotFoundException("Discharge", "NOT FOUND", id));
			dischargeDetails.setFinalOutcome(finalOutcome);
			dischargeDetails.setConditionOfMother(conditionOfMother);
			dischargeDetails.setConditionOfBaby(conditionOfBaby);
			dischargeDetails.setCounsellingOnDangerSignsDone(counsellingOnDangerSignsDone);
			dischargeDetails.setFamilyPlanningMethodAdopted(familyPlanningMethodAdopted);
			dischargeDetails.setDischargeDetailsOthers(dischargeDetailsOthers);
			dischargeDetails.setDischargeDetailsOtherNotes(dischargeDetailsOtherNotes);
			dischargeDetails.setDate_of_Discharge_Referral(date_of_Discharge_Referral);
			dischargeDetails.setTime_of_Discharge_Referral(time_of_Discharge_Referral);
			dischargeDetails.setAdvice_on_discharge(advice_on_discharge);
			dischargeDetails.setFacility_name(facility_name);
			dischargeDetails.setOther_notes(other_notes);
			dischargeDetails.setTreatment_given(treatment_given);
			dischargeDetails.setReason_for_referral(reason_for_referral);
			dischargeDetails.setTreatmentGiven1(treatment_given1);
			dischargeDetails.setbPOfMother(bPOfMother);
			dischargeDetails.setbPOfMother1(bPOfMother1);
			dischargeDetails.setField1(field1);
			dischargeDetails.setField2(field2);
			dischargeDetails.setField3(field3);
			dischargeDetails.setField4(field4);
			dischargeDetails.setField5(field5);
			dischargeDetails.setField6(field6);
			dischargeDetails.setField7(field7);
			dischargeDetails.setField8(field8);
			dischargeDetails.setField9(field9);
			dischargeDetails.setField10(field10);
			dischargeDetails.setAdmission(setAdmission1);
			dischargeDetails.setUniqueId(setAdmission1.getUniqueNo());
			dischargeDetailsRepository.save(dischargeDetails);

			userMap.put("status", HttpStatus.OK);
			userMap.put("data", "Discharge Updated successfull");
			userMap.put("id", dischargeDetails.getId());
			} catch (Exception e) {
			userMap.put("status", HttpStatus.INTERNAL_SERVER_ERROR);
			userMap.put("data", "Discharge Updated failure");
			userMap.put("id", "failed");
		}
		return userMap;
	}
}
