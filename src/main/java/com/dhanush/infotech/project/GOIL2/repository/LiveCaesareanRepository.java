package com.dhanush.infotech.project.GOIL2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL2.model.LiveCaesareanCount;

public interface LiveCaesareanRepository extends JpaRepository<LiveCaesareanCount, Long>{

	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.typeofdelivery = '89'")
	Long findByState(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.typeofdelivery = '89'")
	Long findByDistrict(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.typeofdelivery = '89'")
	Long findByBlock(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.typeofdelivery = '89'")
	Long findByFacilitytype(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.typeofdelivery = '89'")
	Long findByFacility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%92%'")
	Long findByState1(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%92%'")
	Long findByDistrict1(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%92%'")
	Long findByBlock1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%92%'")
	Long findByFacilitytype1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.typeofdelivery) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%92%'")
	Long findByFacility1(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	
	
	@Query(value = "select count(a.amstl) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and amstl in ('yes','1')")
	Long findByState2(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.amstl) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and amstl in ('yes','1')")
	Long findByDistrict2(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.amstl) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and amstl in ('yes','1')")
	Long findByBlock2(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.amstl) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and amstl in ('yes','1')")
	Long findByFacilitytype2(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.amstl) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and amstl in ('yes','1')")
	Long findByFacility2(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%'")
	Long findByState3(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByDistrict3(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByBlock3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByFacilitytype3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(a.deliveryoutcome1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliveryoutcome1 like '%93%' ")
	Long findByFacility3(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.maternaldeath in ('true','yes')")
	Long findByState4(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.maternaldeath in ('true','yes')")
	Long findByDistrict4(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.maternaldeath in ('true','yes')")
	Long findByBlock4(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.maternaldeath in ('true','yes')")
	Long findByFacilitytype4(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.maternaldeath in ('true','yes')")
	Long findByFacility4(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	 
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%'  or a.deliverycomplications like '%97%') ")
	Long findByState5(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or  a.deliverycomplications like '%97%') ")
	Long findByDistrict5(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByBlock5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByFacilitytype5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long findByFacility5(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
	
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByState6(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByDistrict6(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByBlock6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%' ")
	Long findByFacilitytype6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%100%'")
	Long findByFacility6(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat like %:date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long FacilityDashBoardState(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat  like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') ")
	Long FacilityDashBoardFility(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("date") String start);
	
	
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%479%' ")
	Long findByState8(@Param("state") String state,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%479%' ")
	Long findByDistrict8(@Param("state") String state,@Param("district") String district,@Param("start") String start,@Param("end") String end);

	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%479%' ")
	Long findByBlock8(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start,@Param("end") String end);
	
	
	@Query(value = "select count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and  a.createdat > :start and a.createdat < :end and a.deliverycomplications  like '%479%' ")
	Long findByFacilitytype8(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start,@Param("end") String end);

	@Query(value = "select  count(1) as count FROM LiveCaesareanCount a where a.state = :state and a.district = :district and a.block = :block and a.facilitytype = :facilitytype and facility = :facility and  a.createdat > :start and a.createdat < :end and a.deliverycomplications like '%479%' ")
	Long findByFacility8(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start,@Param("end") String end);

	
}
