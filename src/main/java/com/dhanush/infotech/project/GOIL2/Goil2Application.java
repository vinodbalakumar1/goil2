package com.dhanush.infotech.project.GOIL2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class Goil2Application {
	public static void main(String[] args) {
		SpringApplication.run(Goil2Application.class, args);
	}
}
