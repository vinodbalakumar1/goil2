package com.dhanush.infotech.project.GOIL2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dhanush.infotech.project.GOIL2.model.stateddeliveryoutcome;

public interface StateDashBoardDeliveryView extends JpaRepository<stateddeliveryoutcome ,String>{
	
	@Query(value = "select count(1) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.districtname,a.facilityname,a.facility FROM stateddeliveryoutcome a where  a.createdat like :date% and a.state = :state and a.district = :district  group by  a.facility,a.facilityname,a.districtname,a.district")
	List<stateddeliveryoutcome> findByDistrict(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	
	
	@Query(value = "select count(1) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% group by a.district,a.districtname")
	List<Object[]> objByState(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.facilityname,a.districtname,a.facility FROM stateddeliveryoutcome a where  a.createdat like :date% and a.state = :state and a.district = :district  group by  a.facility,a.facilityname,a.districtname,a.district")
	List<Object[]> objByDistrict(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	
	//live births
	
	@Query(value = "select count(1) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and  a.createdat like :date% and a.deliveryoutcome1 in ('92','92_','92_93','93_92') group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState0(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.districtname,a.facilityname,a.facility,a.district FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliveryoutcome1 in ('92','92_','92_93','93_92') group by a.facility,a.district,a.facilityname,a.districtname")
	List<stateddeliveryoutcome> findByDistrict0(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and  a.createdat like :date% and a.deliveryoutcome1 like '%92%' group by a.district,a.districtname")
	List<Object[]> objByState0(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.facilityname,a.districtname,a.facility,a.district FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliveryoutcome1 like '%92%' group by a.facility,a.district,a.facilityname,a.districtname")
	List<Object[]> objByDistrict0(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	// amstl
	
	@Query(value = "select count(a.amstl) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and amstl in ('yes','1') group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState1(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.amstl) as count,a.districtname,a.facilityname,a.facility,a.district FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and amstl in ('yes','1') group by a.facility,a.district,a.facilityname,a.districtname")
	List<stateddeliveryoutcome> findByDistrict1(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	// sd
	
	@Query(value = "select count(a.amstl) as count,a.districtname,a.district FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and amstl in ('yes','1') group by a.district,a.districtname")
	List<Object[]> objByState1(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.amstl) as count,a.districtname,a.facilityname,a.facility,a.district FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and amstl in ('yes','1') group by a.facility,a.district,a.facilityname,a.districtname")
	List<Object[]> objByDistrict1(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	
	
	
	
	// state dashboard
	
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliverycomplications in ('96','97','96_97','97_96') group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState5(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications in ('96','97','96_97','97_96') group by a.district,a.districtname,a.facility,a.facilityname")
	List<stateddeliveryoutcome> findByDistrict5(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%') group by a.district,a.districtname")
	List<Object[]> objByState5(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and (a.deliverycomplications like '%96%' or a.deliverycomplications like '%97%')  group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict5(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	
	
	// pph
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliverycomplications in ('100') group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState6(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications in ('100') group by a.district,a.districtname,a.facility,a.facilityname")
	List<stateddeliveryoutcome> findByDistrict6(@Param("state") String state,@Param("district") String district,@Param("date") String date);


	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliverycomplications like '%100%' group by a.district,a.districtname")
	List<Object[]> objByState6(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications like '%100%' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict6(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	
	
	
	
	// prolonged
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliverycomplications like '%479%' group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState8(@Param("state") String state,@Param("date") String date);
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications like '%479%' group by a.district,a.districtname,a.facility,a.facilityname")
	List<stateddeliveryoutcome> findByDistrict8(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	
	
	
	@Query(value = "select count(1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliverycomplications like '%479%' group by a.district,a.districtname")
	List<Object[]> objByState8(@Param("state") String state,@Param("date") String date);
	
	@Query(value = "select count(1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.deliverycomplications like '%479%' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict8(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	
	
	
	
	// ceasearn
	
	
	@Query(value = "select count(a.typeofdelivery) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.typeofdelivery = '89' group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState9(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.typeofdelivery) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.typeofdelivery = '89' group by a.district,a.districtname,a.facility,a.facilityname")
	List<stateddeliveryoutcome> findByDistrict9(@Param("state") String state,@Param("district") String district,@Param("date") String date);

	
	
	@Query(value = "select count(a.typeofdelivery) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.typeofdelivery = '89' group by a.district,a.districtname")
	List<Object[]> objByState9(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.typeofdelivery) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date% and a.typeofdelivery = '89' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict9(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	
	// still
	
	@Query(value = "select count(a.deliveryoutcome1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliveryoutcome1 in ('93','93_','92_93','93_92') group by a.district,a.districtname")
	List<stateddeliveryoutcome> findByState3(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date and a.deliveryoutcome1 in ('93','93_','92_93','93_92') group by a.district,a.districtname,a.facility,a.facilityname")
	List<stateddeliveryoutcome> findByDistrict3(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count,a.district,a.districtname FROM stateddeliveryoutcome a where a.state = :state and a.createdat like :date% and a.deliveryoutcome1 like '%93%' group by a.district,a.districtname")
	List<Object[]> objByState3(@Param("state") String state,@Param("date") String date);
	
	
	@Query(value = "select count(a.deliveryoutcome1) as count,a.district,a.districtname,a.facility,a.facilityname FROM stateddeliveryoutcome a where a.state = :state and a.district = :district and a.createdat like :date and a.deliveryoutcome1 like '%93%' group by a.district,a.districtname,a.facility,a.facilityname")
	List<Object[]> objByDistrict3(@Param("state") String state,@Param("district") String district,@Param("date") String date);
	
}
