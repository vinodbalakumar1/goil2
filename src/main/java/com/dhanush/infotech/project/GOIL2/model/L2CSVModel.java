package com.dhanush.infotech.project.GOIL2.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "csvl2")

public class L2CSVModel
{
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;	
    private String state;
    private String district;
    private String block;
    private String facility_type;
    private String facility;
    private String completeness;
    private String aadhar_card_number;
    private String abortion_parity ;
    private String address; 
    private String admission_category;
    private String admission_date ;
    private String admissionchildid ;
    private String age;
    private String blockname;
    private String booked ;
    private String bpl_jsy_register;
    private String  contact_number;
    private String contraception_history ;
    private String date_on_set_of_labour;
    private String designation;
    private String districtname;
    private String edd;
    private String facilityname;
    private String facilitytypename;
    private String family_ho_chronic_illness;
    private String final_diagnosis;
    private String final_out_come;
    private String general_exam_height;
    private String  general_exam_jaundice ;
    private String general_exam_pallor ;
    private String general_exam_pedal_edema ;
    private String general_exam_weight;
    private String gestational_ageage_from_usg;
    private String gestational_ageantenatal_corticosteroid ;
    private String gestational_ageedd;
    private String gestational_agefinal_estimated_age;
    private String gestational_agefunda_mental_heights;
    private String gestational_agelmp;
    private String gestational_agepre_term ;
    private String gravida;
    private String indication_for_assistedlscsothers;
    private String ipd_registerd_number;
    private String is_active;
    private String lab_exam_antidgiven_hb ;
    private String lab_exam_blood_group_rh ;
    private String lab_exam_urine_sugar ;
    private String lab_exam_blood_sugar ;
    private String lab_examgdm ;
    private String lab_examhbs_ag ;
    private String lab_exam_hb;
    private String lab_exam_hiv ;
    private String lab_exam_malaria ;
    private String lab_exam_others;
    private String lab_exam_syphilis ;
    private String lab_exam_urine_protein ;
    private String lab_exam_vdrlrpr ;
    private String living_children;
    private String lmp;
    private String martial_status;
    private String mcts_no;
    private String medical_surgical_history;
    private String name_of_asha;
    private String name_of_service_provider  ;
    private String others1;
    private String others2;
    private String paexam_engagement;
    private String paexam_lie;
    private String paexam_others;
    private String parity;
    private String paexampresentation_cephalic ;
    private String past_obstetrics_history; 
    private String patient_contact_number;
    private String patient_name;
    private String pcts_id_no;
    private String presenting_complaints;
    private String provisional_diagnosis;
    private String pv_exam_cervical_dilation;
    private String pv_exam_cervical_effacement;
    private String pv_exam_colour_of_amniotic_fluid ;
    private String pv_exam_membranes ; 
    private String pv_exam_no_ofpvexaminations;
    private String pv_exam_pelvis_adequate ;
    private String reffered_from;
    private String reffered_reason;
    private String statename;
    private String status;
    private String time;
    private String time_of_onset_of_labour;
    private String unique_no ;
    private String vitals_bp_diastolic;
    private String vitals_bp_systolic;
    private String vitals_fhr;
    private String vitals_pulse;
    private String vitals_respiratory_rate;
    private String vitals_temparature;
    private String  wo_do;
    private String provider_date;
    private String provider_phone_no;
    private String provider_time;
    private String condition_of_baby;
    private String condition_of_mother;
    private String counselling_on_danger_signs_done;
    private String discharge_details_other_notes;
    private String discharge_details_others;
    private String family_planning_method_adopted;
    private String final_outcome;
    private String advice_on_discharge;
    private String date_of_discharge_referral;
     private String facility_name;
     private String other_notes;
     private String reason_for_referral;
     private String time_of_discharge_referral;
     private String treatment_given;
     private String treatment_given1;
     private String augmentation_performed;
     private String ifyes_please_specify_the_indi_aug;
     private String whether_partograph_was_filled;
     private String whether_scc1was_filled;
     private String whether_ssc2was_filled;
     private String assisted;
     private String delivery_notes_abortion;
     private String delivery_notes_amstl_performed;
     private String delivery_notes_cct;
     private String delivery_notes_complications;
     private String delivery_notes_date;
     private String delivery_notes_episiotomy;
     private String delivery_notes_if_maternal_death_cause_of_death;
     private String delivery_notes_maternal_death;
     private String delivery_notes_maternaldeath_time;
     private String delivery_notes_others;
     private String delivery_notes_ppiucd_inserted;
     private String delivery_notes_preterm;
     private String delivery_notes_time;
     private String delivery_notes_uterine_massage;
     private String delivery_notes_uterotonic_administered;
     private String delivery_outcome1;
     private String delivery_outcome2;
     private String type_of_delivery ;
     private String whetherscc4wasfilled;
     private String blood_transfusion_or_other_procedure_notes;
     private String clinical_diagnosis_if_any_condition_presen;
     private String condition_at_transfer_to_pnc_ward;
     private String date_and_time_of_transfer_topncward;
     private String date_and_time_of_transfer_topncward_time;
     private String if_referred_reason_for_referral_of_mother_baby;
     private String notes_for_baby;
     private String notes_for_mother;
     private String post_delivery_notes;
     private String post_delivery_others;
     private String whether_scc3was_filled;
     private String any_congenital_anomaly;
     private String any_other_complication;
     private String baby_notes_birth_weight;
     private String breastfeeding_initiated;
     private String breastfeeding_time_of_initiation;
     private String did_the_baby_cry_immediately_after_birth;
     private String did_the_baby_require_resuscitation;
     private String identification_for_baby;
     private String if_yes_dose;
     private String if_yes_was_it_initiated_in_labour_room;
     private String sex_of_the_baby; 
     private String temperature_of_baby;
     private String vitaminkadministered;
     private String unique_id;
     private String Baby1;
     private String Baby2;
     private String  Baby3 ;
     
     
     
     
     
     
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}

	public String getFacility_type() {
		return facility_type;
	}
	public void setFacility_type(String facility_type) {
		this.facility_type = facility_type;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCompleteness() {
		return completeness;
	}
	public void setCompleteness(String completeness) {
		this.completeness = completeness;
	}
	public String getAadhar_card_number() {
		return aadhar_card_number;
	}
	public void setAadhar_card_number(String aadhar_card_number) {
		this.aadhar_card_number = aadhar_card_number;
	}
	public String getAbortion_parity() {
		return abortion_parity;
	}
	public void setAbortion_parity(String abortion_parity) {
		this.abortion_parity = abortion_parity;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAdmission_category() {
		return admission_category;
	}
	public void setAdmission_category(String admission_category) {
		this.admission_category = admission_category;
	}
	public String getAdmission_date() {
		return admission_date;
	}
	public void setAdmission_date(String admission_date) {
		this.admission_date = admission_date;
	}
	public String getAdmissionchildid() {
		return admissionchildid;
	}
	public void setAdmissionchildid(String admissionchildid) {
		this.admissionchildid = admissionchildid;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBlockname() {
		return blockname;
	}
	public void setBlockname(String blockname) {
		this.blockname = blockname;
	}
	public String getBooked() {
		return booked;
	}
	public void setBooked(String booked) {
		this.booked = booked;
	}
	public String getBpl_jsy_register() {
		return bpl_jsy_register;
	}
	public void setBpl_jsy_register(String bpl_jsy_register) {
		this.bpl_jsy_register = bpl_jsy_register;
	}
	public String getContact_number() {
		return contact_number;
	}
	public void setContact_number(String contact_number) {
		this.contact_number = contact_number;
	}
	public String getContraception_history() {
		return contraception_history;
	}
	public void setContraception_history(String contraception_history) {
		this.contraception_history = contraception_history;
	}
	public String getDate_on_set_of_labour() {
		return date_on_set_of_labour;
	}
	public void setDate_on_set_of_labour(String date_on_set_of_labour) {
		this.date_on_set_of_labour = date_on_set_of_labour;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDistrictname() {
		return districtname;
	}
	public void setDistrictname(String districtname) {
		this.districtname = districtname;
	}
	public String getEdd() {
		return edd;
	}
	public void setEdd(String edd) {
		this.edd = edd;
	}
	public String getFacilityname() {
		return facilityname;
	}
	public void setFacilityname(String facilityname) {
		this.facilityname = facilityname;
	}
	public String getFacilitytypename() {
		return facilitytypename;
	}
	public void setFacilitytypename(String facilitytypename) {
		this.facilitytypename = facilitytypename;
	}
	public String getFamily_ho_chronic_illness() {
		return family_ho_chronic_illness;
	}
	public void setFamily_ho_chronic_illness(String family_ho_chronic_illness) {
		this.family_ho_chronic_illness = family_ho_chronic_illness;
	}
	public String getFinal_diagnosis() {
		return final_diagnosis;
	}
	public void setFinal_diagnosis(String final_diagnosis) {
		this.final_diagnosis = final_diagnosis;
	}
	public String getFinal_out_come() {
		return final_out_come;
	}
	public void setFinal_out_come(String final_out_come) {
		this.final_out_come = final_out_come;
	}
	public String getGeneral_exam_height() {
		return general_exam_height;
	}
	public void setGeneral_exam_height(String general_exam_height) {
		this.general_exam_height = general_exam_height;
	}
	public String getGeneral_exam_jaundice() {
		return general_exam_jaundice;
	}
	public void setGeneral_exam_jaundice(String general_exam_jaundice) {
		this.general_exam_jaundice = general_exam_jaundice;
	}
	public String getGeneral_exam_pallor() {
		return general_exam_pallor;
	}
	public void setGeneral_exam_pallor(String general_exam_pallor) {
		this.general_exam_pallor = general_exam_pallor;
	}
	public String getGeneral_exam_pedal_edema() {
		return general_exam_pedal_edema;
	}
	public void setGeneral_exam_pedal_edema(String general_exam_pedal_edema) {
		this.general_exam_pedal_edema = general_exam_pedal_edema;
	}
	public String getGeneral_exam_weight() {
		return general_exam_weight;
	}
	public void setGeneral_exam_weight(String general_exam_weight) {
		this.general_exam_weight = general_exam_weight;
	}
	public String getGestational_ageage_from_usg() {
		return gestational_ageage_from_usg;
	}
	public void setGestational_ageage_from_usg(String gestational_ageage_from_usg) {
		this.gestational_ageage_from_usg = gestational_ageage_from_usg;
	}
	public String getGestational_ageantenatal_corticosteroid() {
		return gestational_ageantenatal_corticosteroid;
	}
	public void setGestational_ageantenatal_corticosteroid(String gestational_ageantenatal_corticosteroid) {
		this.gestational_ageantenatal_corticosteroid = gestational_ageantenatal_corticosteroid;
	}
	public String getGestational_ageedd() {
		return gestational_ageedd;
	}
	public void setGestational_ageedd(String gestational_ageedd) {
		this.gestational_ageedd = gestational_ageedd;
	}
	public String getGestational_agefinal_estimated_age() {
		return gestational_agefinal_estimated_age;
	}
	public void setGestational_agefinal_estimated_age(String gestational_agefinal_estimated_age) {
		this.gestational_agefinal_estimated_age = gestational_agefinal_estimated_age;
	}
	public String getGestational_agefunda_mental_heights() {
		return gestational_agefunda_mental_heights;
	}
	public void setGestational_agefunda_mental_heights(String gestational_agefunda_mental_heights) {
		this.gestational_agefunda_mental_heights = gestational_agefunda_mental_heights;
	}
	public String getGestational_agelmp() {
		return gestational_agelmp;
	}
	public void setGestational_agelmp(String gestational_agelmp) {
		this.gestational_agelmp = gestational_agelmp;
	}
	public String getGestational_agepre_term() {
		return gestational_agepre_term;
	}
	public void setGestational_agepre_term(String gestational_agepre_term) {
		this.gestational_agepre_term = gestational_agepre_term;
	}
	public String getGravida() {
		return gravida;
	}
	public void setGravida(String gravida) {
		this.gravida = gravida;
	}
	public String getIndication_for_assistedlscsothers() {
		return indication_for_assistedlscsothers;
	}
	public void setIndication_for_assistedlscsothers(String indication_for_assistedlscsothers) {
		this.indication_for_assistedlscsothers = indication_for_assistedlscsothers;
	}
	public String getIpd_registerd_number() {
		return ipd_registerd_number;
	}
	public void setIpd_registerd_number(String ipd_registerd_number) {
		this.ipd_registerd_number = ipd_registerd_number;
	}
	public String getIs_active() {
		return is_active;
	}
	public void setIs_active(String is_active) {
		this.is_active = is_active;
	}
	public String getLab_exam_antidgiven_hb() {
		return lab_exam_antidgiven_hb;
	}
	public void setLab_exam_antidgiven_hb(String lab_exam_antidgiven_hb) {
		this.lab_exam_antidgiven_hb = lab_exam_antidgiven_hb;
	}
	public String getLab_exam_blood_group_rh() {
		return lab_exam_blood_group_rh;
	}
	public void setLab_exam_blood_group_rh(String lab_exam_blood_group_rh) {
		this.lab_exam_blood_group_rh = lab_exam_blood_group_rh;
	}
	public String getLab_exam_urine_sugar() {
		return lab_exam_urine_sugar;
	}
	public void setLab_exam_urine_sugar(String lab_exam_urine_sugar) {
		this.lab_exam_urine_sugar = lab_exam_urine_sugar;
	}
	public String getLab_exam_blood_sugar() {
		return lab_exam_blood_sugar;
	}
	public void setLab_exam_blood_sugar(String lab_exam_blood_sugar) {
		this.lab_exam_blood_sugar = lab_exam_blood_sugar;
	}
	public String getLab_examgdm() {
		return lab_examgdm;
	}
	public void setLab_examgdm(String lab_examgdm) {
		this.lab_examgdm = lab_examgdm;
	}
	public String getLab_examhbs_ag() {
		return lab_examhbs_ag;
	}
	public void setLab_examhbs_ag(String lab_examhbs_ag) {
		this.lab_examhbs_ag = lab_examhbs_ag;
	}
	public String getLab_exam_hb() {
		return lab_exam_hb;
	}
	public void setLab_exam_hb(String lab_exam_hb) {
		this.lab_exam_hb = lab_exam_hb;
	}
	public String getLab_exam_hiv() {
		return lab_exam_hiv;
	}
	public void setLab_exam_hiv(String lab_exam_hiv) {
		this.lab_exam_hiv = lab_exam_hiv;
	}
	public String getLab_exam_malaria() {
		return lab_exam_malaria;
	}
	public void setLab_exam_malaria(String lab_exam_malaria) {
		this.lab_exam_malaria = lab_exam_malaria;
	}
	public String getLab_exam_others() {
		return lab_exam_others;
	}
	public void setLab_exam_others(String lab_exam_others) {
		this.lab_exam_others = lab_exam_others;
	}
	public String getLab_exam_syphilis() {
		return lab_exam_syphilis;
	}
	public void setLab_exam_syphilis(String lab_exam_syphilis) {
		this.lab_exam_syphilis = lab_exam_syphilis;
	}
	public String getLab_exam_urine_protein() {
		return lab_exam_urine_protein;
	}
	public void setLab_exam_urine_protein(String lab_exam_urine_protein) {
		this.lab_exam_urine_protein = lab_exam_urine_protein;
	}
	public String getLab_exam_vdrlrpr() {
		return lab_exam_vdrlrpr;
	}
	public void setLab_exam_vdrlrpr(String lab_exam_vdrlrpr) {
		this.lab_exam_vdrlrpr = lab_exam_vdrlrpr;
	}
	public String getLiving_children() {
		return living_children;
	}
	public void setLiving_children(String living_children) {
		this.living_children = living_children;
	}
	public String getLmp() {
		return lmp;
	}
	public void setLmp(String lmp) {
		this.lmp = lmp;
	}
	public String getMartial_status() {
		return martial_status;
	}
	public void setMartial_status(String martial_status) {
		this.martial_status = martial_status;
	}
	public String getMcts_no() {
		return mcts_no;
	}
	public void setMcts_no(String mcts_no) {
		this.mcts_no = mcts_no;
	}
	public String getMedical_surgical_history() {
		return medical_surgical_history;
	}
	public void setMedical_surgical_history(String medical_surgical_history) {
		this.medical_surgical_history = medical_surgical_history;
	}
	public String getName_of_asha() {
		return name_of_asha;
	}
	public void setName_of_asha(String name_of_asha) {
		this.name_of_asha = name_of_asha;
	}
	public String getName_of_service_provider() {
		return name_of_service_provider;
	}
	public void setName_of_service_provider(String name_of_service_provider) {
		this.name_of_service_provider = name_of_service_provider;
	}
	public String getOthers1() {
		return others1;
	}
	public void setOthers1(String others1) {
		this.others1 = others1;
	}
	public String getOthers2() {
		return others2;
	}
	public void setOthers2(String others2) {
		this.others2 = others2;
	}
	public String getPaexam_engagement() {
		return paexam_engagement;
	}
	public void setPaexam_engagement(String paexam_engagement) {
		this.paexam_engagement = paexam_engagement;
	}
	public String getPaexam_lie() {
		return paexam_lie;
	}
	public void setPaexam_lie(String paexam_lie) {
		this.paexam_lie = paexam_lie;
	}
	public String getPaexam_others() {
		return paexam_others;
	}
	public void setPaexam_others(String paexam_others) {
		this.paexam_others = paexam_others;
	}
	public String getParity() {
		return parity;
	}
	public void setParity(String parity) {
		this.parity = parity;
	}
	public String getPaexampresentation_cephalic() {
		return paexampresentation_cephalic;
	}
	public void setPaexampresentation_cephalic(String paexampresentation_cephalic) {
		this.paexampresentation_cephalic = paexampresentation_cephalic;
	}
	public String getPast_obstetrics_history() {
		return past_obstetrics_history;
	}
	public void setPast_obstetrics_history(String past_obstetrics_history) {
		this.past_obstetrics_history = past_obstetrics_history;
	}
	public String getPatient_contact_number() {
		return patient_contact_number;
	}
	public void setPatient_contact_number(String patient_contact_number) {
		this.patient_contact_number = patient_contact_number;
	}
	public String getPatient_name() {
		return patient_name;
	}
	public void setPatient_name(String patient_name) {
		this.patient_name = patient_name;
	}
	public String getPcts_id_no() {
		return pcts_id_no;
	}
	public void setPcts_id_no(String pcts_id_no) {
		this.pcts_id_no = pcts_id_no;
	}
	public String getPresenting_complaints() {
		return presenting_complaints;
	}
	public void setPresenting_complaints(String presenting_complaints) {
		this.presenting_complaints = presenting_complaints;
	}
	public String getProvisional_diagnosis() {
		return provisional_diagnosis;
	}
	public void setProvisional_diagnosis(String provisional_diagnosis) {
		this.provisional_diagnosis = provisional_diagnosis;
	}
	public String getPv_exam_cervical_dilation() {
		return pv_exam_cervical_dilation;
	}
	public void setPv_exam_cervical_dilation(String pv_exam_cervical_dilation) {
		this.pv_exam_cervical_dilation = pv_exam_cervical_dilation;
	}
	public String getPv_exam_cervical_effacement() {
		return pv_exam_cervical_effacement;
	}
	public void setPv_exam_cervical_effacement(String pv_exam_cervical_effacement) {
		this.pv_exam_cervical_effacement = pv_exam_cervical_effacement;
	}
	public String getPv_exam_colour_of_amniotic_fluid() {
		return pv_exam_colour_of_amniotic_fluid;
	}
	public void setPv_exam_colour_of_amniotic_fluid(String pv_exam_colour_of_amniotic_fluid) {
		this.pv_exam_colour_of_amniotic_fluid = pv_exam_colour_of_amniotic_fluid;
	}
	public String getPv_exam_membranes() {
		return pv_exam_membranes;
	}
	public void setPv_exam_membranes(String pv_exam_membranes) {
		this.pv_exam_membranes = pv_exam_membranes;
	}
	public String getPv_exam_no_ofpvexaminations() {
		return pv_exam_no_ofpvexaminations;
	}
	public void setPv_exam_no_ofpvexaminations(String pv_exam_no_ofpvexaminations) {
		this.pv_exam_no_ofpvexaminations = pv_exam_no_ofpvexaminations;
	}
	public String getPv_exam_pelvis_adequate() {
		return pv_exam_pelvis_adequate;
	}
	public void setPv_exam_pelvis_adequate(String pv_exam_pelvis_adequate) {
		this.pv_exam_pelvis_adequate = pv_exam_pelvis_adequate;
	}
	public String getReffered_from() {
		return reffered_from;
	}
	public void setReffered_from(String reffered_from) {
		this.reffered_from = reffered_from;
	}
	public String getReffered_reason() {
		return reffered_reason;
	}
	public void setReffered_reason(String reffered_reason) {
		this.reffered_reason = reffered_reason;
	}
	public String getStatename() {
		return statename;
	}
	public void setStatename(String statename) {
		this.statename = statename;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTime_of_onset_of_labour() {
		return time_of_onset_of_labour;
	}
	public void setTime_of_onset_of_labour(String time_of_onset_of_labour) {
		this.time_of_onset_of_labour = time_of_onset_of_labour;
	}
	public String getUnique_no() {
		return unique_no;
	}
	public void setUnique_no(String unique_no) {
		this.unique_no = unique_no;
	}
	public String getVitals_bp_diastolic() {
		return vitals_bp_diastolic;
	}
	public void setVitals_bp_diastolic(String vitals_bp_diastolic) {
		this.vitals_bp_diastolic = vitals_bp_diastolic;
	}
	public String getVitals_bp_systolic() {
		return vitals_bp_systolic;
	}
	public void setVitals_bp_systolic(String vitals_bp_systolic) {
		this.vitals_bp_systolic = vitals_bp_systolic;
	}
	public String getVitals_fhr() {
		return vitals_fhr;
	}
	public void setVitals_fhr(String vitals_fhr) {
		this.vitals_fhr = vitals_fhr;
	}
	public String getVitals_pulse() {
		return vitals_pulse;
	}
	public void setVitals_pulse(String vitals_pulse) {
		this.vitals_pulse = vitals_pulse;
	}
	public String getVitals_respiratory_rate() {
		return vitals_respiratory_rate;
	}
	public void setVitals_respiratory_rate(String vitals_respiratory_rate) {
		this.vitals_respiratory_rate = vitals_respiratory_rate;
	}
	public String getVitals_temparature() {
		return vitals_temparature;
	}
	public void setVitals_temparature(String vitals_temparature) {
		this.vitals_temparature = vitals_temparature;
	}
	public String getWo_do() {
		return wo_do;
	}
	public void setWo_do(String wo_do) {
		this.wo_do = wo_do;
	}
	public String getProvider_date() {
		return provider_date;
	}
	public void setProvider_date(String provider_date) {
		this.provider_date = provider_date;
	}
	public String getProvider_phone_no() {
		return provider_phone_no;
	}
	public void setProvider_phone_no(String provider_phone_no) {
		this.provider_phone_no = provider_phone_no;
	}
	public String getProvider_time() {
		return provider_time;
	}
	public void setProvider_time(String provider_time) {
		this.provider_time = provider_time;
	}
	public String getCondition_of_baby() {
		return condition_of_baby;
	}
	public void setCondition_of_baby(String condition_of_baby) {
		this.condition_of_baby = condition_of_baby;
	}
	public String getCondition_of_mother() {
		return condition_of_mother;
	}
	public void setCondition_of_mother(String condition_of_mother) {
		this.condition_of_mother = condition_of_mother;
	}
	public String getCounselling_on_danger_signs_done() {
		return counselling_on_danger_signs_done;
	}
	public void setCounselling_on_danger_signs_done(String counselling_on_danger_signs_done) {
		this.counselling_on_danger_signs_done = counselling_on_danger_signs_done;
	}
	public String getDischarge_details_other_notes() {
		return discharge_details_other_notes;
	}
	public void setDischarge_details_other_notes(String discharge_details_other_notes) {
		this.discharge_details_other_notes = discharge_details_other_notes;
	}
	public String getDischarge_details_others() {
		return discharge_details_others;
	}
	public void setDischarge_details_others(String discharge_details_others) {
		this.discharge_details_others = discharge_details_others;
	}
	public String getFamily_planning_method_adopted() {
		return family_planning_method_adopted;
	}
	public void setFamily_planning_method_adopted(String family_planning_method_adopted) {
		this.family_planning_method_adopted = family_planning_method_adopted;
	}
	public String getFinal_outcome() {
		return final_outcome;
	}
	public void setFinal_outcome(String final_outcome) {
		this.final_outcome = final_outcome;
	}
	public String getAdvice_on_discharge() {
		return advice_on_discharge;
	}
	public void setAdvice_on_discharge(String advice_on_discharge) {
		this.advice_on_discharge = advice_on_discharge;
	}
	public String getDate_of_discharge_referral() {
		return date_of_discharge_referral;
	}
	public void setDate_of_discharge_referral(String date_of_discharge_referral) {
		this.date_of_discharge_referral = date_of_discharge_referral;
	}
	public String getFacility_name() {
		return facility_name;
	}
	public void setFacility_name(String facility_name) {
		this.facility_name = facility_name;
	}
	public String getOther_notes() {
		return other_notes;
	}
	public void setOther_notes(String other_notes) {
		this.other_notes = other_notes;
	}
	public String getReason_for_referral() {
		return reason_for_referral;
	}
	public void setReason_for_referral(String reason_for_referral) {
		this.reason_for_referral = reason_for_referral;
	}
	public String getTime_of_discharge_referral() {
		return time_of_discharge_referral;
	}
	public void setTime_of_discharge_referral(String time_of_discharge_referral) {
		this.time_of_discharge_referral = time_of_discharge_referral;
	}
	public String getTreatment_given() {
		return treatment_given;
	}
	public void setTreatment_given(String treatment_given) {
		this.treatment_given = treatment_given;
	}
	public String getTreatment_given1() {
		return treatment_given1;
	}
	public void setTreatment_given1(String treatment_given1) {
		this.treatment_given1 = treatment_given1;
	}
	public String getAugmentation_performed() {
		return augmentation_performed;
	}
	public void setAugmentation_performed(String augmentation_performed) {
		this.augmentation_performed = augmentation_performed;
	}
	
	public String getIfyes_please_specify_the_indi_aug() {
		return ifyes_please_specify_the_indi_aug;
	}
	public void setIfyes_please_specify_the_indi_aug(String ifyes_please_specify_the_indi_aug) {
		this.ifyes_please_specify_the_indi_aug = ifyes_please_specify_the_indi_aug;
	}

	public String getWhether_partograph_was_filled() {
		return whether_partograph_was_filled;
	}
	public void setWhether_partograph_was_filled(String whether_partograph_was_filled) {
		this.whether_partograph_was_filled = whether_partograph_was_filled;
	}
	public String getWhether_scc1was_filled() {
		return whether_scc1was_filled;
	}
	public void setWhether_scc1was_filled(String whether_scc1was_filled) {
		this.whether_scc1was_filled = whether_scc1was_filled;
	}
	public String getWhether_ssc2was_filled() {
		return whether_ssc2was_filled;
	}
	public void setWhether_ssc2was_filled(String whether_ssc2was_filled) {
		this.whether_ssc2was_filled = whether_ssc2was_filled;
	}
	public String getAssisted() {
		return assisted;
	}
	public void setAssisted(String assisted) {
		this.assisted = assisted;
	}
	public String getDelivery_notes_abortion() {
		return delivery_notes_abortion;
	}
	public void setDelivery_notes_abortion(String delivery_notes_abortion) {
		this.delivery_notes_abortion = delivery_notes_abortion;
	}
	public String getDelivery_notes_amstl_performed() {
		return delivery_notes_amstl_performed;
	}
	public void setDelivery_notes_amstl_performed(String delivery_notes_amstl_performed) {
		this.delivery_notes_amstl_performed = delivery_notes_amstl_performed;
	}
	public String getDelivery_notes_cct() {
		return delivery_notes_cct;
	}
	public void setDelivery_notes_cct(String delivery_notes_cct) {
		this.delivery_notes_cct = delivery_notes_cct;
	}
	public String getDelivery_notes_complications() {
		return delivery_notes_complications;
	}
	public void setDelivery_notes_complications(String delivery_notes_complications) {
		this.delivery_notes_complications = delivery_notes_complications;
	}
	public String getDelivery_notes_date() {
		return delivery_notes_date;
	}
	public void setDelivery_notes_date(String delivery_notes_date) {
		this.delivery_notes_date = delivery_notes_date;
	}
	public String getDelivery_notes_episiotomy() {
		return delivery_notes_episiotomy;
	}
	public void setDelivery_notes_episiotomy(String delivery_notes_episiotomy) {
		this.delivery_notes_episiotomy = delivery_notes_episiotomy;
	}
	public String getDelivery_notes_if_maternal_death_cause_of_death() {
		return delivery_notes_if_maternal_death_cause_of_death;
	}
	public void setDelivery_notes_if_maternal_death_cause_of_death(String delivery_notes_if_maternal_death_cause_of_death) {
		this.delivery_notes_if_maternal_death_cause_of_death = delivery_notes_if_maternal_death_cause_of_death;
	}
	public String getDelivery_notes_maternal_death() {
		return delivery_notes_maternal_death;
	}
	public void setDelivery_notes_maternal_death(String delivery_notes_maternal_death) {
		this.delivery_notes_maternal_death = delivery_notes_maternal_death;
	}
	public String getDelivery_notes_maternaldeath_time() {
		return delivery_notes_maternaldeath_time;
	}
	public void setDelivery_notes_maternaldeath_time(String delivery_notes_maternaldeath_time) {
		this.delivery_notes_maternaldeath_time = delivery_notes_maternaldeath_time;
	}
	public String getDelivery_notes_others() {
		return delivery_notes_others;
	}
	public void setDelivery_notes_others(String delivery_notes_others) {
		this.delivery_notes_others = delivery_notes_others;
	}
	public String getDelivery_notes_ppiucd_inserted() {
		return delivery_notes_ppiucd_inserted;
	}
	public void setDelivery_notes_ppiucd_inserted(String delivery_notes_ppiucd_inserted) {
		this.delivery_notes_ppiucd_inserted = delivery_notes_ppiucd_inserted;
	}
	public String getDelivery_notes_preterm() {
		return delivery_notes_preterm;
	}
	public void setDelivery_notes_preterm(String delivery_notes_preterm) {
		this.delivery_notes_preterm = delivery_notes_preterm;
	}
	public String getDelivery_notes_time() {
		return delivery_notes_time;
	}
	public void setDelivery_notes_time(String delivery_notes_time) {
		this.delivery_notes_time = delivery_notes_time;
	}
	public String getDelivery_notes_uterine_massage() {
		return delivery_notes_uterine_massage;
	}
	public void setDelivery_notes_uterine_massage(String delivery_notes_uterine_massage) {
		this.delivery_notes_uterine_massage = delivery_notes_uterine_massage;
	}
	public String getDelivery_notes_uterotonic_administered() {
		return delivery_notes_uterotonic_administered;
	}
	public void setDelivery_notes_uterotonic_administered(String delivery_notes_uterotonic_administered) {
		this.delivery_notes_uterotonic_administered = delivery_notes_uterotonic_administered;
	}
	public String getDelivery_outcome1() {
		return delivery_outcome1;
	}
	public void setDelivery_outcome1(String delivery_outcome1) {
		this.delivery_outcome1 = delivery_outcome1;
	}
	public String getDelivery_outcome2() {
		return delivery_outcome2;
	}
	public void setDelivery_outcome2(String delivery_outcome2) {
		this.delivery_outcome2 = delivery_outcome2;
	}
	public String getType_of_delivery() {
		return type_of_delivery;
	}
	public void setType_of_delivery(String type_of_delivery) {
		this.type_of_delivery = type_of_delivery;
	}
	public String getWhetherscc4wasfilled() {
		return whetherscc4wasfilled;
	}
	public void setWhetherscc4wasfilled(String whetherscc4wasfilled) {
		this.whetherscc4wasfilled = whetherscc4wasfilled;
	}
	public String getBlood_transfusion_or_other_procedure_notes() {
		return blood_transfusion_or_other_procedure_notes;
	}
	public void setBlood_transfusion_or_other_procedure_notes(String blood_transfusion_or_other_procedure_notes) {
		this.blood_transfusion_or_other_procedure_notes = blood_transfusion_or_other_procedure_notes;
	}
	public String getClinical_diagnosis_if_any_condition_presen() {
		return clinical_diagnosis_if_any_condition_presen;
	}
	public void setClinical_diagnosis_if_any_condition_presen(String clinical_diagnosis_if_any_condition_presen) {
		this.clinical_diagnosis_if_any_condition_presen = clinical_diagnosis_if_any_condition_presen;
	}
	public String getCondition_at_transfer_to_pnc_ward() {
		return condition_at_transfer_to_pnc_ward;
	}
	public void setCondition_at_transfer_to_pnc_ward(String condition_at_transfer_to_pnc_ward) {
		this.condition_at_transfer_to_pnc_ward = condition_at_transfer_to_pnc_ward;
	}
	public String getDate_and_time_of_transfer_topncward() {
		return date_and_time_of_transfer_topncward;
	}
	public void setDate_and_time_of_transfer_topncward(String date_and_time_of_transfer_topncward) {
		this.date_and_time_of_transfer_topncward = date_and_time_of_transfer_topncward;
	}
	public String getDate_and_time_of_transfer_topncward_time() {
		return date_and_time_of_transfer_topncward_time;
	}
	public void setDate_and_time_of_transfer_topncward_time(String date_and_time_of_transfer_topncward_time) {
		this.date_and_time_of_transfer_topncward_time = date_and_time_of_transfer_topncward_time;
	}
	public String getIf_referred_reason_for_referral_of_mother_baby() {
		return if_referred_reason_for_referral_of_mother_baby;
	}
	public void setIf_referred_reason_for_referral_of_mother_baby(String if_referred_reason_for_referral_of_mother_baby) {
		this.if_referred_reason_for_referral_of_mother_baby = if_referred_reason_for_referral_of_mother_baby;
	}
	public String getNotes_for_baby() {
		return notes_for_baby;
	}
	public void setNotes_for_baby(String notes_for_baby) {
		this.notes_for_baby = notes_for_baby;
	}
	public String getNotes_for_mother() {
		return notes_for_mother;
	}
	public void setNotes_for_mother(String notes_for_mother) {
		this.notes_for_mother = notes_for_mother;
	}
	public String getPost_delivery_notes() {
		return post_delivery_notes;
	}
	public void setPost_delivery_notes(String post_delivery_notes) {
		this.post_delivery_notes = post_delivery_notes;
	}
	public String getPost_delivery_others() {
		return post_delivery_others;
	}
	public void setPost_delivery_others(String post_delivery_others) {
		this.post_delivery_others = post_delivery_others;
	}
	public String getWhether_scc3was_filled() {
		return whether_scc3was_filled;
	}
	public void setWhether_scc3was_filled(String whether_scc3was_filled) {
		this.whether_scc3was_filled = whether_scc3was_filled;
	}
	public String getAny_congenital_anomaly() {
		return any_congenital_anomaly;
	}
	public void setAny_congenital_anomaly(String any_congenital_anomaly) {
		this.any_congenital_anomaly = any_congenital_anomaly;
	}
	public String getAny_other_complication() {
		return any_other_complication;
	}
	public void setAny_other_complication(String any_other_complication) {
		this.any_other_complication = any_other_complication;
	}
	public String getBaby_notes_birth_weight() {
		return baby_notes_birth_weight;
	}
	public void setBaby_notes_birth_weight(String baby_notes_birth_weight) {
		this.baby_notes_birth_weight = baby_notes_birth_weight;
	}
	public String getBreastfeeding_initiated() {
		return breastfeeding_initiated;
	}
	public void setBreastfeeding_initiated(String breastfeeding_initiated) {
		this.breastfeeding_initiated = breastfeeding_initiated;
	}
	public String getBreastfeeding_time_of_initiation() {
		return breastfeeding_time_of_initiation;
	}
	public void setBreastfeeding_time_of_initiation(String breastfeeding_time_of_initiation) {
		this.breastfeeding_time_of_initiation = breastfeeding_time_of_initiation;
	}
	public String getDid_the_baby_cry_immediately_after_birth() {
		return did_the_baby_cry_immediately_after_birth;
	}
	public void setDid_the_baby_cry_immediately_after_birth(String did_the_baby_cry_immediately_after_birth) {
		this.did_the_baby_cry_immediately_after_birth = did_the_baby_cry_immediately_after_birth;
	}
	public String getDid_the_baby_require_resuscitation() {
		return did_the_baby_require_resuscitation;
	}
	public void setDid_the_baby_require_resuscitation(String did_the_baby_require_resuscitation) {
		this.did_the_baby_require_resuscitation = did_the_baby_require_resuscitation;
	}
	public String getIdentification_for_baby() {
		return identification_for_baby;
	}
	public void setIdentification_for_baby(String identification_for_baby) {
		this.identification_for_baby = identification_for_baby;
	}
	public String getIf_yes_dose() {
		return if_yes_dose;
	}
	public void setIf_yes_dose(String if_yes_dose) {
		this.if_yes_dose = if_yes_dose;
	}
	public String getIf_yes_was_it_initiated_in_labour_room() {
		return if_yes_was_it_initiated_in_labour_room;
	}
	public void setIf_yes_was_it_initiated_in_labour_room(String if_yes_was_it_initiated_in_labour_room) {
		this.if_yes_was_it_initiated_in_labour_room = if_yes_was_it_initiated_in_labour_room;
	}
	public String getSex_of_the_baby() {
		return sex_of_the_baby;
	}
	public void setSex_of_the_baby(String sex_of_the_baby) {
		this.sex_of_the_baby = sex_of_the_baby;
	}
	public String getTemperature_of_baby() {
		return temperature_of_baby;
	}
	public void setTemperature_of_baby(String temperature_of_baby) {
		this.temperature_of_baby = temperature_of_baby;
	}
	public String getVitaminkadministered() {
		return vitaminkadministered;
	}
	public void setVitaminkadministered(String vitaminkadministered) {
		this.vitaminkadministered = vitaminkadministered;
	}
	public String getUnique_id() {
		return unique_id;
	}
	public void setUnique_id(String unique_id) {
		this.unique_id = unique_id;
	}
	public String getBaby1() {
		return Baby1;
	}
	public void setBaby1(String baby1) {
		Baby1 = baby1;
	}
	public String getBaby2() {
		return Baby2;
	}
	public void setBaby2(String baby2) {
		Baby2 = baby2;
	}
	public String getBaby3() {
		return Baby3;
	}
	public void setBaby3(String baby3) {
		Baby3 = baby3;
	}
     
     
     
     
     
     
	
}
