package com.dhanush.infotech.project.GOIL2.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.dhanush.infotech.project.GOIL2.model.AssessmentOfPostPartumCondition;

public interface AssessmentOfPostPartumConditionRepository extends JpaRepository<AssessmentOfPostPartumCondition, Long> {
	
	
	
	
}
