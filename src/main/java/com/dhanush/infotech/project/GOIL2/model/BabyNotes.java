package com.dhanush.infotech.project.GOIL2.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "l2_babynotes")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)

public class BabyNotes {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt  = new Date();;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt  = new Date();;

    public Admission getAdmission() {
		return admission;
	}
	public void setAdmission(Admission admission) {
		this.admission = admission;
	}
	@ManyToOne
    @JoinColumn(name = "babynotesid")
    private Admission admission;

	
	 private String sexOfTheBaby;
	 private String babyNotesBirthWeight;
	 private String didTheBabyCryImmediatelyAfterBirth;
	 private String didTheBabyRequireResuscitation;
	 private String ifYesWasItInitiatedInLabourRoom;
	 private String breastfeedingInitiated;
	 private String breastfeedingTimeOfInitiation;
	 private String anyCongenitalAnomaly;
	 private String anyOtherComplication;
	 private String childId;
	 private String vitaminKAdministered;
	 private String ifYesDose;
	 private String vaccinationDone;
	 private String temperatureOfBaby;
	 private String identificationForBaby;
	 private String uniqueId;
	 public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	
	 public String getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	} public String getSexOfTheBaby() {
		return sexOfTheBaby;
	}
	public void setSexOfTheBaby(String sexOfTheBaby) {
		this.sexOfTheBaby = sexOfTheBaby;
	}
	public String getBabyNotesBirthWeight() {
		return babyNotesBirthWeight;
	}
	public void setBabyNotesBirthWeight(String babyNotesBirthWeight) {
		this.babyNotesBirthWeight = babyNotesBirthWeight;
	}
	public String getDidTheBabyCryImmediatelyAfterBirth() {
		return didTheBabyCryImmediatelyAfterBirth;
	}
	public void setDidTheBabyCryImmediatelyAfterBirth(String didTheBabyCryImmediatelyAfterBirth) {
		this.didTheBabyCryImmediatelyAfterBirth = didTheBabyCryImmediatelyAfterBirth;
	}
	public String getDidTheBabyRequireResuscitation() {
		return didTheBabyRequireResuscitation;
	}
	public void setDidTheBabyRequireResuscitation(String didTheBabyRequireResuscitation) {
		this.didTheBabyRequireResuscitation = didTheBabyRequireResuscitation;
	}
	public String getIfYesWasItInitiatedInLabourRoom() {
		return ifYesWasItInitiatedInLabourRoom;
	}
	public void setIfYesWasItInitiatedInLabourRoom(String ifYesWasItInitiatedInLabourRoom) {
		this.ifYesWasItInitiatedInLabourRoom = ifYesWasItInitiatedInLabourRoom;
	}
	public String getBreastfeedingInitiated() {
		return breastfeedingInitiated;
	}
	public void setBreastfeedingInitiated(String breastfeedingInitiated) {
		this.breastfeedingInitiated = breastfeedingInitiated;
	}
	public String getBreastfeedingTimeOfInitiation() {
		return breastfeedingTimeOfInitiation;
	}
	public void setBreastfeedingTimeOfInitiation(String breastfeedingTimeOfInitiation) {
		this.breastfeedingTimeOfInitiation = breastfeedingTimeOfInitiation;
	}
	public String getAnyCongenitalAnomaly() {
		return anyCongenitalAnomaly;
	}
	public void setAnyCongenitalAnomaly(String anyCongenitalAnomaly) {
		this.anyCongenitalAnomaly = anyCongenitalAnomaly;
	}
	public String getAnyOtherComplication() {
		return anyOtherComplication;
	}
	public void setAnyOtherComplication(String anyOtherComplication) {
		this.anyOtherComplication = anyOtherComplication;
	}
	public String getVitaminKAdministered() {
		return vitaminKAdministered;
	}
	public void setVitaminKAdministered(String vitaminKAdministered) {
		this.vitaminKAdministered = vitaminKAdministered;
	}
	public String getIfYesDose() {
		return ifYesDose;
	}
	public void setIfYesDose(String ifYesDose) {
		this.ifYesDose = ifYesDose;
	}
	public String getVaccinationDone() {
		return vaccinationDone;
	}
	public void setVaccinationDone(String vaccinationDone) {
		this.vaccinationDone = vaccinationDone;
	}
	public String getTemperatureOfBaby() {
		return temperatureOfBaby;
	}
	public void setTemperatureOfBaby(String temperatureOfBaby) {
		this.temperatureOfBaby = temperatureOfBaby;
	}
	public String getIdentificationForBaby() {
		return identificationForBaby;
	}
	public void setIdentificationForBaby(String identificationForBaby) {
		this.identificationForBaby = identificationForBaby;
	}
	

}
