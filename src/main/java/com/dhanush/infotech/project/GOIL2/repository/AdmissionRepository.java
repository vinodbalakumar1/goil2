package com.dhanush.infotech.project.GOIL2.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dhanush.infotech.project.GOIL2.model.Admission;

/**
 * Created by vinod on 11/06/18.
 */
@Repository
public interface AdmissionRepository extends JpaRepository<Admission, Long> {
	
	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and uniqueNo like :caseId")
	Optional<Admission> uniqueId(@Param("caseId") String uniquNo);
	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and uniqueNo like :caseId")
	List<Admission> findByCaseId(@Param("caseId") String uniqueNo);
	
	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and mctsNo like :mcts")
	List<Admission> findByMCTSNO(@Param("mcts") String mcts);
	
	//search on beneficiary name
	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientName like :beneficary% and a.state= :state")
	List<Admission> findByCaseNameState(@Param("beneficary") String beneficary,@Param("state") String state);


	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientName like :beneficary% and a.state= :state and a.district= :district")
	List<Admission> findByCaseNameDistrict(@Param("beneficary") String beneficary,@Param("state") String state,@Param("district") String district);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientName like :beneficary% and a.state= :state and a.district= :district and a.block= :block")
	List<Admission> findByCaseNameBlock(@Param("beneficary") String beneficary,@Param("state") String state,@Param("district") String district,@Param("block") String block);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientName like :beneficary% and a.state= :state and a.district= :district and a.block= :block and facilityType= :facilityType")
	List<Admission> findByCaseNameFacilityType(@Param("beneficary") String beneficary,@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilityType") String facilityType);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientName like :beneficary% and a.state= :state and a.district= :district and a.block= :block and facilityType= :facilityType and facility= :facility")
	List<Admission> findByCaseNameFacility(@Param("beneficary") String beneficary,@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilityType") String facilityType,@Param("facility") String facility);


	
	//search on mobile no
	
	
	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientContactNumber = :mobileno and a.state= :state")
	List<Admission> findByMobileState(@Param("mobileno") String mobileno,@Param("state") String state);


	
	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientContactNumber = :mobileno and a.state= :state and a.district= :district")
	List<Admission> findByMobileDistrict(@Param("mobileno") String mobileno,@Param("state") String state,@Param("district") String district);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientContactNumber = :mobileno and a.state= :state and a.district= :district and a.block= :block")
	List<Admission> findByMobileBlock(@Param("mobileno") String mobileno,@Param("state") String state,@Param("district") String district,@Param("block") String block);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientContactNumber = :mobileno and a.state= :state and a.district= :district and a.block= :block and facilityType= :facilityType")
	List<Admission> findByMobileFacilityType(@Param("mobileno") String mobileno,@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilityType") String facilityType);


	@Query(value = "SELECT a FROM Admission a WHERE status <> 104 and a.patientContactNumber = :mobileno and a.state= :state and a.district= :district and a.block= :block and facilityType= :facilityType and facility= :facility")
	List<Admission> findByMobileFacility(@Param("mobileno") String mobileno,@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilityType") String facilityType,@Param("facility") String facility);


	// for l2 csv download
	
	List<Admission> findByStateAndAdmissionDateLike(String string, String date);


	List<Admission> findByStateAndDistrictAndAdmissionDateLike(String string, String string2, String date);


	List<Admission> findByStateAndDistrictAndBlockAndAdmissionDateLike(String string, String string2, String string3,
			String date);


	List<Admission> findByStateAndDistrictAndBlockAndFacilityTypeAndAdmissionDateLike(String string, String string2,
			String string3, String string4, String date);


	List<Admission> findByStateAndDistrictAndBlockAndFacilityTypeAndFacilityAndAdmissionDateLike(String string,
			String string2, String string3, String string4, String string5, String date);

	
	// for mother bd in mpr


	@Query(value = "select count(1) as count FROM Admission a where a.state = :state and a.admissionDate like :start%  and a.vitalsBpDiastolic > 0")
	Long findStateMotherBp(@Param("state") String state,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count FROM Admission a where a.state = :state and a.district = :district and a.admissionDate like :start% and a.vitalsBpDiastolic > 0 ")
	Long findDistrictMotherBp(@Param("state") String state,@Param("district") String district,@Param("start") String start);

	
	@Query(value = "select count(1) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.admissionDate like :start% and a.vitalsBpDiastolic > 0 ")
	Long findBlockMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("start") String start);
	
	
	@Query(value = "select count(1) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.facilityType = :facilitytype and  a.admissionDate like :start% and a.vitalsBpDiastolic > 0 ")
	Long findFacilitytypeMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(1) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.facilityType = :facilitytype and facility = :facility and  a.admissionDate like :start% and a.vitalsBpDiastolic > 0")
	Long findFacilityCodeMotherBp(@Param("state") String state,@Param("district") String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,@Param("start") String start);

// for mother temp at admission
	

	@Query(value = "select count(a.vitalsTemparature) as count FROM Admission a where a.state = :state and a.admissionDate like :start%  and a.vitalsTemparature >0")
	Long findStateTempAdmission(@Param("state")String state, @Param("start") String start);

	@Query(value = "select count(a.vitalsTemparature) as count FROM Admission a where a.state = :state and a.district = :district and a.admissionDate like :start%  and a.vitalsTemparature >0")
	Long findDistrictTempAdmission(@Param("state")String state,@Param("district")String district, @Param("start")String start);

	@Query(value = "select count(a.vitalsTemparature) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.admissionDate like :start% and a.vitalsTemparature >0")
	Long findBlockTempAdmission(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("start") String start);

	@Query(value = "select count(a.vitalsTemparature) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.facilityType = :facilitytype and  a.admissionDate like :start% and a.vitalsTemparature >0")
	Long findFacilitytypeTempAdmission(@Param("state")String state, @Param("district")String district,@Param("block") String block,@Param("facilitytype") String facilitytype,@Param("start") String start);

	@Query(value = "select  count(a.vitalsTemparature) as count FROM Admission a where a.state = :state and a.district = :district and a.block = :block and a.facilityType = :facilitytype and facility = :facility and  a.admissionDate like :start% and a.vitalsTemparature >0")
	Long findFacilityCodeTempAdmission(@Param("state")String state,@Param("district") String district,@Param("block")  String block,@Param("facilitytype") String facilitytype,@Param("facility") String facility,
			@Param("start")String start);

	





	
	
}
