package com.dhanush.infotech.project.GOIL2.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "l2_list_child")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public class ListChild {
	

    @ManyToOne
    @JoinColumn(name = "admissionid")
    private Admission admission;
    

	public Admission getAdmission() {
		return admission;
	}

	public void setAdmission(Admission admission) {
		this.admission = admission;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeliveryOutCome1() {
		return deliveryOutCome1;
	}

	public void setDeliveryOutCome1(String deliveryOutCome1) {
		this.deliveryOutCome1 = deliveryOutCome1;
	}

	public String getDeliveryOutCome2() {
		return deliveryOutCome2;
	}

	public void setDeliveryOutCome2(String deliveryOutCome2) {
		this.deliveryOutCome2 = deliveryOutCome2;
	}

	public String getAbortion() {
		return abortion;
	}

	public void setAbortion(String abortion) {
		this.abortion = abortion;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPreterm() {
		return preterm;
	}

	public void setPreterm(String preterm) {
		this.preterm = preterm;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public String getImmunization() {
		return immunization;
	}

	public void setImmunization(String immunization) {
		this.immunization = immunization;
	}

	public String getModeOfDelivery() {
		return modeOfDelivery;
	}

	public void setModeOfDelivery(String modeOfDelivery) {
		this.modeOfDelivery = modeOfDelivery;
	}

	public String getDeliveryOthers() {
		return deliveryOthers;
	}

	public void setDeliveryOthers(String deliveryOthers) {
		this.deliveryOthers = deliveryOthers;
	}

	public void setBirthWeight(String birthWeight) {
		this.birthWeight = birthWeight;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String uniqueNo;

	private String deliveryOutCome1;

	private String deliveryOutCome2;

	private String abortion;

	private String sex;

	private String preterm;

	private String birthWeight;

	private String deliveryDate;

	private String deliveryTime;

	private String immunization;

	private String modeOfDelivery;

	private String deliveryOthers;
	
	private String vitaminKAdministered;

	public String getVitaminKAdministered() {
		return vitaminKAdministered;
	}

	public void setVitaminKAdministered(String vitaminKAdministered) {
		this.vitaminKAdministered = vitaminKAdministered;
	}

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createdAt = new Date();

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updatedAt = new Date();

	public String getUniqueNo() {
		return uniqueNo;
	}

	public void setUniqueNo(String uniqueNo) {
		this.uniqueNo = uniqueNo;
	}



	public String getBirthWeight() {
		return birthWeight;
	}

	

}